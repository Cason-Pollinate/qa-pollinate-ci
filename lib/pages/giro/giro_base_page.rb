class GiroBasePage < BasePage

ul		:side_finishes, 	css: 		"#main-wrapper > main > section > section.spectrum-controls > section > div:nth-child(3) > div > ul.spectrum-finish-type-controls"
ul		:side_colors,		css: 		"#main-wrapper > main > section > section.spectrum-controls > section > div:nth-child(3) > div > ul.selection-list"
button 	:side_button,		css: 		"#main-wrapper > main > section > section.spectrum-feature-navigation-controls > ul > li.active > button"

ul		:middle_finishes, 	css: 		"#main-wrapper > main > section > section.spectrum-controls > section > div:nth-child(2) > div > ul.spectrum-finish-type-controls"
ul		:middle_colors,		css: 		"#main-wrapper > main > section > section.spectrum-controls > section > div:nth-child(2) > div > ul.selection-list"
button 	:middle_button,		css: 		"#main-wrapper > main > section > section.spectrum-feature-navigation-controls > ul > li:nth-child(2) > button"


ul		:top_finishes, 		css: 		"#main-wrapper > main > section > section.spectrum-controls > section > div:nth-child(1) > div > ul.spectrum-finish-type-controls"
ul		:top_colors,		css: 		"#main-wrapper > main > section > section.spectrum-controls > section > div:nth-child(1) > div > ul.selection-list"
button 	:top_button,		css: 		"#main-wrapper > main > section > section.spectrum-feature-navigation-controls > ul > li:nth-child(3) > button"

ul		:bottom_finishes, 	css: 		"#main-wrapper > main > section > section.spectrum-controls > section > div:nth-child(4) > div > ul.spectrum-finish-type-controls"
ul		:bottom_colors,		css: 		"#main-wrapper > main > section > section.spectrum-controls > section > div:nth-child(4) > div > ul.selection-list"
button 	:bottom_button,		css: 		"#main-wrapper > main > section > section.spectrum-feature-navigation-controls > ul > li:nth-child(4) > button"

button 	:logo_button,		css: 		"#main-wrapper > main > section > section.spectrum-feature-navigation-controls > ul > li:nth-child(5) > button"
ul 		:logo_colors,		css: 		"#main-wrapper > main > section > section.spectrum-controls > section > div:nth-child(9) > div > ul"

button 	:fit_button,		css: 		"#main-wrapper > main > section > section.spectrum-feature-navigation-controls > ul > li:nth-child(6) > button"
ul 		:fit_colors,		css: 		"#main-wrapper > main > section > section.spectrum-controls > section > div:nth-child(8) > div > ul"

button 	:webbing_button,	css: 		"#main-wrapper > main > section > section.spectrum-feature-navigation-controls > ul > li:nth-child(7) > button"
ul 		:webbing_colors,	css: 		"#main-wrapper > main > section > section.spectrum-controls > section > div:nth-child(6) > div > ul"

button 	:et_al_button,		css: 		"#main-wrapper > main > section > section.spectrum-feature-navigation-controls > ul > li:nth-child(8) > button"
ul 		:et_al_colors,		css: 		"#main-wrapper > main > section > section.spectrum-controls > section > div:nth-child(7) > div > ul"

ul 		:areas, 			css: 		"#main-wrapper > main > section > section.spectrum-feature-navigation-controls > ul"

p 		:price,				css: 		"#main-wrapper > main > section > section:nth-child(1) > p"
button	:restart_button,	text: 		"Restart"
button 	:save_button, 		text: 		"Save"
button 	:share_button,		text: 		"Share"
button 	:size_dropdown,		text: 		"Choose Size"
button 	:size_small,		text: 		"Small"
button 	:size_medium,		text: 		"Medium"
button 	:size_large,		text: 		"Large"
button 	:add_to_cart, 		class: 		["spectrum-add-to-cart"]

element :three_d,			class: 		["hero"]

div 	:save_ber,			class: 		["graphic"]
image	:share_ber,			class: 		["share__image share__image-outline"]

button 	:close_button, 		class: 		["close"]

div 	:page_loader, 		class: 		["loader__bar"]



	# def iterate_areas
	# 	self.wait_until {self.areas_element.exists?}
	# 	self.areas_element.lis.each do |li|
	# 		li.children.each do |c|
	# 			c.button.click
	# 		end
	# 	end
	# end


	def spec_response(recipe_id)
		@hash = Hash.new{ |hsh,key| hsh[key] = [] }
		@response = Nokogiri::XML.parse(RestClient.get("http://test.spectrumcustomizer.com/giro/specification/#{recipe_id}/html"){|response, request, result| response })
		@response.css('tr').each do |row|
			if row.css('td[class=label]').text != ""
				if row.css('td[class=sku]').text == ""
					@hash[row.css('td[class=label]').text] << row.css('td[class=description]').text
				else
					@hash[row.css('td[class=label]').text] << [row.css('td[class=description]').text, row.css('td[class=sku]').text]
				end
			end
		end
		@hash
	end



	def packlist_response(recipe_id)
		@hash = Hash.new{ |hsh,key| hsh[key] = [] }
		@response = Nokogiri::XML.parse(RestClient.get("http://test.spectrumcustomizer.com/giro/packlist/#{recipe_id}/html"){|response, request, result| response })
		
	end

	def get_recipe_id
		self.url.scan(/[A-Z0-9]+/)
	end

	def verify_sku
		case self.size_dropdown
		when "Small"
			sku = "7099579"
		when "Medium"
			sku = "7099580"
		when "Large"
			sku = "7099581"
		end
	end

	def we_are_all_here
		self.wait_until(60)	{self.page_loader?}
		self.wait_while 	{self.page_loader_element.visible?}
		self.wait_until(60) {self.side_button?}
		self.wait_until(60) {self.middle_button?}
		self.wait_until(60) {self.top_button?}
		self.wait_until(60) {self.bottom_button?}
		self.wait_until(60) {self.logo_button?}
		self.wait_until(60) {self.fit_button?}
		self.wait_until(60) {self.webbing_button?}
		self.wait_until(60) {self.et_al_button?}
	end


	def iterate_side
		self.side_finishes_element.lis.each do |f|
			f.button.click
			sleep 1
			self.side_colors_element.lis.each do |c|
				c.button.click
			end
		end
	end

	def iterate_middle
		self.middle_button
		self.wait_until {self.middle_finishes_element.visible?}
		self.middle_finishes_element.lis.each do |f|
			f.button.click
			sleep 1
			self.middle_colors_element.lis.each do |c|
				c.button.click
			end
		end
	end

	def iterate_top
		self.top_button
		self.wait_until {self.top_finishes_element.visible?}
		self.top_finishes_element.lis.each do |f|
			f.button.click
			sleep 1
			self.top_colors_element.lis.each do |c|
				c.button.click
			end
		end
	end

	def iterate_bottom
		self.bottom_button
		self.wait_until {self.bottom_finishes_element.visible?}
		self.bottom_finishes_element.lis.each do |f|
			if f != bottom_finishes_element.lis.last
				f.button.click
				sleep 1
				self.bottom_colors_element.lis.each do |c|
					c.button.click
				end
			end
		end
	end

	def iterate_logo
		self.logo_button
		self.logo_colors_element.lis.each do |c|
			c.button.click
		end
	end

	def iterate_fit
		self.fit_button
		self.fit_colors_element.lis.each do |c|
			c.button.click
		end
	end

	def iterate_webbing
		self.webbing_button
		self.webbing_colors_element.lis.each do |c|
			c.button.click
		end
	end

	def iterate_et_al
		self.et_al_button
		self.et_al_colors_element.lis.each do |c|
			c.button.click
		end
	end

end