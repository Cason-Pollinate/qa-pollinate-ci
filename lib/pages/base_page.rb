class BasePage
	include PageObject

	def initialize
		super($driver)
	end

	def self.get_text_message_handles(scene)
		@pset = Hash.new{ |hsh,key| hsh[key] = [] }
		case ENV['ENVIRONMENT'].to_sym
		when :test then @response = JSON.parse(RestClient.get("test.spectrumcustomizer.com/api/productsets/#{scene}"){|response, request, result| response })
		when :staging then @response = JSON.parse(RestClient.get("staging.spectrumcustomizer.com/api/productsets/#{scene}"){|response, request, result| response })
		when :prod then @response = JSON.parse(RestClient.get("api.spectrumcustomizer.com/api/productsets/#{scene}"){|response, request, result| response })
		end
		@response['contents']['productGroups'].each do |productGroups|
			if productGroups.empty? == false
				productGroups['products'].each do |products|
					if products.empty? == false
						products['rootFeature']['childFeatures'].each do |childFeatures|
							if childFeatures.empty? == false
								childFeatures['selectionGroup']['selections'].each do |sg|
									if sg.empty? == false && sg['available'] == 'true'
										@pset.store(sg['handle'],sg['textMessageHandle'])
										if sg['features'].nil? == false
											sg['features'].each do |features|
												features['selectionGroup']['selections'].empty? == false
												features['selectionGroup']['selections'].each do |sel|
													@pset.store(sel['handle'],sel['textMessageHandle'])
													if sel['selectionGroup'].nil? == false
														if sel['selectionGroup']['selections'].empty? == false
															sel['selectionGroup']['selections'].each do |s|
																@pset.store(s['handle'],s['textMessageHandle'])
															end
														end
													end
												end
											end
										end
									end
								end
							end
						end
					end
				end
			end
		end
		return @pset
	end



	def self.localize_text_message_handles(scene)
		@msg = parse_product_set(scene)
		@loc = Hash.new{ |hsh,key| hsh[key] = [] }
		case ENV['ENVIRONMENT'].to_sym
		when :test
			@response = JSON.parse(RestClient.get("test.spectrumcustomizer.com/api/localized-messages/productset/#{scene}/"){|response, request, result| response })
		when :staging
			@response = JSON.parse(RestClient.get("staging.spectrumcustomizer.com/api/localized-messages/productset/#{scene}/"){|response, request, result| response })
		when :prod
			@response = JSON.parse(RestClient.get("api.spectrumcustomizer.com/api/localized-messages/productset/#{scene}/"){|response, request, result| response })
		end
		@msg.each do |k, v|
			v.each do |value|
				@response['contents'].each do |c|
					if c['handle'] == "#{value}"
						@loc[k] << c['fragment']
					end
				end
			end
		end
		return @loc
	end




	def self.convert_handle_to_text(scene, recipe_id)
		@rset = parse_recipeset(recipe_id)
		@msg = localize_text_message_handles(scene)
		@rset.each do |k,v|
			@msg.each do |key,value|
				if v == key
					@rset[k] = "#{value.gsub(/ \D\d+./,"")}"
				end
			end
		end
		return @rset
	end



	def self.parse_recipeset(recipe_id)
		@rset = Hash.new{ |hsh,key| hsh[key] = [] }
		case ENV['ENVIRONMENT'].to_sym
		when :test then @response = JSON.parse(RestClient.get("test.spectrumcustomizer.com/api/recipesets/readable/#{recipe_id}"){|response, request, result| response })
		when :staging then @response = JSON.parse(RestClient.get("staging.spectrumcustomizer.com/api/recipesets/readable/#{recipe_id}"){|response, request, result| response })
		when :prod then @response = JSON.parse(RestClient.get("api.spectrumcustomizer.com/api/recipesets/readable/#{recipe_id}"){|response, request, result| response })
		end
		@response['contents']['recipes'].each do |recipes|
			recipes['recipe']['recipeData'].each do |recipeData|
				recipeData['childFeatures'].each do |childFeatures|
					if (childFeatures.empty? == false || childFeatures.nil? == false)
						if (childFeatures['featureHandle'].nil? == false && childFeatures['selectionHandle'].nil? == false)
							if (childFeatures['featureHandle'].include?("-color") == true && childFeatures['featureHandle'].include?("roughness") == false && childFeatures['featureHandle'].include?("toggle") == false && childFeatures['featureHandle'].include?("-upper-color") == false)
								@rset.store(childFeatures['featureHandle'], childFeatures['selectionHandle'])
							end
						end
						if childFeatures['selectionGroup'].nil? == false
							childFeatures['selectionGroup']['selections'].each do |selections|
								if (selections.empty? == false || selections.nil? == false)
									if (selections['childFeatures'].nil? == false || selections['childFeatures'].empty? == false)
										selections['childFeatures'].each do |features|
											if (features.empty? == false || features.nil? == false)
												if (features['featureHandle'].nil? == false && features['selectionHandle'].nil? == false)
													if (features['featureHandle'].include?("-color") || features['featureHandle'].include?("roughness") == false && features['featureHandle'].include?("toggle") == false && features['featureHandle'].include?("-upper-color") == false)
														@rset.store(features['featureHandle'], features['selectionHandle'])
													end
												end
											end
										end
									end
								end
							end
						end
					end
				end
			end
		end
		return @rset
	end




	def self.parse_product_set(scene)
		@pset = Hash.new{ |hsh,key| hsh[key] = [] }
		case ENV['ENVIRONMENT'].to_sym
		when :test
			@response = JSON.parse(RestClient.get("test.spectrumcustomizer.com/api/productsets/#{scene}"){|response, request, result| response })
		when :staging
			@response = JSON.parse(RestClient.get("staging.spectrumcustomizer.com/api/productsets/#{scene}"){|response, request, result| response })
		when :prod
			@response = JSON.parse(RestClient.get("api.spectrumcustomizer.com/api/productsets/#{scene}"){|response, request, result| response })
		end
		@response['contents']['productGroups'].each do |cpg|
			cpg['products'].each do |p|
				if p['rootFeature']['childFeatures'].nil? == false
					p['rootFeature']['childFeatures'].each do |cf|

						if cf['handle'].include?("size") || cf['handle'].include?("metalness") || cf['handle'].nil?
						else
							if cf['childFeatures'].nil? == false
								cf['childFeatures'].each do |child|
									handle = child['handle']
									child['selectionGroup']['selections'].each do |sgs|
										if @pset[handle].include?(sgs['textMessageHandle']) == false
											@pset[handle] << sgs['textMessageHandle']
										end
									end
								end
							end
							handle = cf['handle']

							if cf['selectionGroup']['selections'].nil? == false
								cf['selectionGroup']['selections'].each do |s|
									if @pset[handle].include?(s['textMessageHandle']) == false
										@pset[handle] << s['textMessageHandle']
									end

									if s['features'].nil? == false
										s['features'].each do |f|

											if f['handle'].include?("size") || f['handle'].include?("metalness") || f['handle'].nil?
											else

												if f['childFeatures'].nil? == false
													f['childFeatures'].each do |ccf|

														handle = ccf['handle']

														ccf['selectionGroup']['selections'].each do |ssg|
															if @pset[handle].include?(ssg['textMessageHandle']) == false
																@pset[handle] << ssg['textMessageHandle']
															end
														end
													end
												end

												if f['handle'].nil? == false && f['selectionGroup']['selections'].nil? == false

													handle = f['handle']

													f['selectionGroup']['selections'].each do |ss|
														if @pset[handle].include?(ss['textMessageHandle']) == false
															@pset[handle] << ss['textMessageHandle']
														end
													end
												end
											end
										end
									end
								end
							end
						end
					end
				end
				@pset.values.uniq
			end
		end
		return @pset									
	end

	def self.parse_html(productset)
		Nokogiri::XML.parse(RestClient.get("http://test.spectrumcustomizer.com/api/assets/productset/#{productset}"){|response, request, result| response })
	end

	def self.image_diff(recipe_id)
		self.adjust_fe_images(recipe_id)
		n = 0
		Dir["#{$screenshotfolder}/#{recipe_id}/FE/*.png"].zip(Dir["#{$screenshotfolder}/#{recipe_id}/BER/*.png"]).each do |fe, ber|
			n += 1
			images = [
				ChunkyPNG::Image.from_file("#{fe}"),
				ChunkyPNG::Image.from_file("#{ber}")
			]
			output = ChunkyPNG::Image.new(images.first.width, images.last.width, WHITE)

			diff = []

			images.first.height.times do |y|
				images.first.row(y).each_with_index do |pixel, x|
					unless pixel == images.last[x,y]
						score = Math.sqrt(
							(r(images.last[x,y]) - r(pixel)) ** 2 +
							(g(images.last[x,y]) - g(pixel)) ** 2 +
							(b(images.last[x,y]) - b(pixel)) ** 2
							) / Math.sqrt(MAX ** 2 * 3)

						output[x,y] = grayscale(MAX - (score * 255).round)
						diff << score
					end
				end
			end

			puts "pixels (total):     #{images.first.pixels.length}"
			puts "pixels changed:     #{diff.length}"
			puts "image changed (%): #{(diff.inject {|sum, value| sum + value} / images.first.pixels.length) * 100}%"

			output.save("#{$screenshotfolder}/#{recipe_id}/diff-#{n}.png")
		end
	end

	def self.adjust_fe_images(recipe_id)
		Dir["#{$screenshotfolder}/#{recipe_id}/FE/*.png"].each do |img|
			new_image = Magick::Image.read(img)[0]
			new_image.resize_to_fit!(512,512)
			new_image.sharpen(radius=0.0, sigma=1.0)
			new_image.write img
		end
	end

	def self.validate_svg(zip_dir, schema)
		schema = Nokogiri::XML::Schema(File.read(schema))
		# doc = Nokogiri::XML(File.open("#{zip_dir, schema}"))
		Zip::File.open(zip_dir, schema) do |zip_file|
			zip_file.each do |entry|
				if entry.get_input_stream.is_a?(Zip::InputStream) && entry.name.include?(".svg")
					doc = Nokogiri::XML.parse(entry.get_input_stream)
					puts "#{entry.name}"
					schema.validate(doc).each do |error|
						puts "#{error.message}"
						puts ""
					end
					puts ""
					puts ""
				end
			end
		end
	end

	def self.validate_svg_url(url, schema)
		File.write 'image.svg', open("#{url}").read
		schema = Nokogiri::XML::Schema(File.read(schema))
		doc = Nokogiri::XML(File.open("#{Dir.pwd}/image.svg"))
		schema.validate(doc).each do |error|
			puts "#{url} | #{error.message}"
			puts ""
		end
	end

	def self.parse_json(url)
		return JSON.parse(RestClient.get(url){|response, request, result| response })
	end

	def self.set_device_to(device)
		case device.to_sym
		when :ipad then user = Webdriver::UserAgent.driver(browser: :chrome, agent: :ipad, orientation: :landscape)
		when :iphone then user = Webdriver::UserAgent.driver(browser: :chrome, agent: :iphone, orientation: :landscape)
		when :phone then user = Webdriver::UserAgent.driver(browser: :chrome, agent: :android_phone, orientation: :landscape)
		when :tablet then user = Webdriver::UserAgent.driver(browser: :chrome, agent: :android_tablet, orientation: :landscape)
		end
		args = ['--flag-switches-begin','--use-mobile-user-agent','--window-position=0,0','--disable-infobars','--flag-switches-end']
		$driver = Watir::Browser.new user, options: { args: args }
	end

	def self.performance_check
		puts "Response time: #{$driver.performance.summary[:response_time]}"
	end

	def self.extract_zip(file)
		Zip::File.open('./reports/*.zip') do |zip_file|
			zip_file.each do |f|
				fpath = File.join($screenshotfolder, f.name)
				zip_file.extract(f, fpath) unless File.exist?(fpath)
			end
		end
	end

	def self.download_image(src,df)
		file_name = File.basename(src)
		file_name = file_name.scan(/^[^\?]*/)
		File.open("#{df}/#{file_name[0]}", 'wb') do |f|
			f.write open(src).read
		end
	end

	def wait_or_rescue(arg)
		begin
			self.wait_until { arg }
		rescue Watir::Wait::TimeoutError, Watir::Exception::UnknownObjectException, Timeout::Error
			BasePage.print_js_errors
		end
	end

	def self.print_js_errors
		log = $driver.driver.manage.logs.get(:browser)
		errors = log.select{ |entry| entry.level.eql? 'SEVERE' }
		if errors.count > 0
			javascript_errors = errors.map(&:message).join("\n")
			puts "PageError:\n#{javascript_errors}"
		end
	end

	def self.print_js_warnings
		log = $driver.driver.manage.logs.get(:browser)
		errors = log.select{ |entry| entry.level.eql? 'WARNING' }
		if errors.count > 0
			javascript_errors = errors.map(&:message).join("\n\n")
			puts "\nPageError:\n#{javascript_errors}"
		end
	end

	def self.raise_js_errors
		log = $driver.driver.manage.logs.get(:browser)
		errors = log.select{ |entry| entry.level.eql? 'SEVERE' }
		if errors.count > 0
			javascript_errors = errors.map(&:message).join("\n")
			raise javascript_errors
		end
	end

	def self.collect_links
		links = $driver.links.collect
		results = links.reject { |r| r.include?("extra") }
		return results
	end

	def self.collect_links_href
		hrefs = $driver.links.collect(&:href)
		results = hrefs.reject { |r| r.include?("extra") }
		return results
	end

	def self.collect_links_text
		text = $driver.links.collect(&:text)
		results = text.reject { |r| r.include?("extra") }
		return results
	end

	def self.navigate_to_starting_page
		$driver.goto $base_url
	end

	def self.quit_webdriver
		$driver.quit
	end

	def self.setup
		BasePage.set_user
		BasePage.set_base_url
		BasePage.navigate_to_starting_page
	end

	def self.create_csv
		CSV.open("#{$screenshotfolder}/#{$csv_file}", "wb") do |csv|
			csv << ["Summary","Issue Type","Assignee","Status","Priority","Reporter","Creator","Due Date","Description","Custom field (Epic Link)","Outward Issue Link"]
		end
	end

	def self.get_csv_data
		adult_hash = Hash.new
		child_hash = Hash.new
		csv_data = CSV.read("./Book1.csv", "r", skip_blanks: true).reject { |row| row.all?(&:nil?) }
		# puts csv_data[0][0] 
		csv_data[1].zip(csv_data[2]).each do |header, data|
			adult_hash.store(header.parameterize.underscore.to_sym,data)
		end
		# puts "#{adult_hash}"
		# puts ""
		# puts "#{csv_data[3][0]}"
		# csv_data[4].zip(csv_data[5]).each do |header, data|
		# 	child_hash.store(header.parameterize.underscore.to_sym,data)
		# end
		return adult_hash
	end

	def self.update_csv(summary, description)
		CSV.open("#{$screenshotfolder}/#{$csv_file}", "a+") do |csv| 
			csv << ["#{summary}","Task","cwilliams","To Do","Normal","cwilliams","cwilliams","","#{description}","",""]
		end
	end


	def self.set_user
		case ENV['SITE'].to_sym
		when :'gk-elite'
			case ENV['ENVIRONMENT'].to_sym
			when :staging
				case ENV['USER_TYPE'].to_sym
				when :consumer then $username = 'pollinatetest@gmail.com' and $password = 'iamquality'
				when :dealer then $username = 'pollinatetest+gkdealer@gmail.com' and $password = 'IamQuality!'
				when :distributor then $username = 'pollinatetest+gkdistributor@gmail.com' and $password = 'IamQuality!'
				when :salesrep then $username = 'pollinatetest+gksalesrep@gmail.com' and $password = 'IamQuality!'
				when :teamlead then $username = 'pollinatetest+gkteamdealer@gmail.com' and $password = 'IamQuality!'
				end
			when :prod
				case ENV['USER_TYPE'].to_sym
				when :consumer then $username = 'pollinatetest+gkconsumer@gmail.com' and $password = 'IamQuality!'
				when :dealer then $username = 'pollinatetest+gkdealer@gmail.com' and $password = 'IamQuality!'
				when :distributor then $username = 'pollinatetest+gkdistributor@gmail.com' and $password = 'IamQuality!'
				when :salesrep then $username = 'pollinatetest+gksalesrep@gmail.com' and $password = 'IamQuality!'
				when :teamlead then $username = 'pollinatetest+gkteamdealer@gmail.com' and $password = 'IamQuality!'
				end
			end
		end
	end

	def self.set_base_url
		case ENV['SITE'].to_sym
		when :benchmade 
			case ENV['ENVIRONMENT']
			when :test then $base_url = 'http://madetoordertest.azureedge.net/benchmade' and $price_url = 'http://test.spectrumcustomizer.com/api/recipesets/pricing/' and $spec_url = 'http://test.spectrumcustomizer.com/benchmade/specification/preview/'
			when :staging then $base_url = 'http://madetoorderstaging.azureedge.net/benchmade' and $price_url = 'http://staging.spectrumcustomizer.com/api/recipesets/pricing/' and $spec_url = 'http://staging.spectrumcustomizer.com/benchmade/specification/preview/'
			when :prod then $base_url = 'http://madetoorder.azureedge.net/benchmade' and $price_url = 'http://api.spectrumcustomizer.com/api/recipesets/pricing/' and $spec_url = 'http://api.spectrumcustomizer.com/benchmade/specification/preview/'
			end
		when :camelbak then $base_url = 'http://madetoorder.azureedge.net/camelbak/frontend/'
		when :customizer
			case ENV['ENVIRONMENT'].to_sym
			when :test then $base_url = 'http://demo.madetoordercustomizer.com/gk-elite/test/frontend/index.html#/products/'
			when :staging then $base_url = 'http://demo.madetoordercustomizer.com/gk-elite/staging/frontend/index.html#/products/'
			when :prod then $base_url = 'http://demo.madetoordercustomizer.com/gk-elite/production/frontend/index.html#/products/'
			end
		when :'gk-elite'
			case ENV['ENVIRONMENT'].to_sym
			when :test then $base_url = 'https://dev-gkelite.pollinate.com'
			when :staging then $base_url = 'https://staging-gkelite.pollinate.com'
			when :prod then $base_url = 'https://www.gkelite.com'
			end
		when :'ua-icon'
			case ENV['ENVIRONMENT']
			when :staging then $base_url = 'https://staging.underarmour.com'
			when :prod then $base_url = 'https://www.underarmour.com/en-us/ua-icon-customized-shoes'
			end
		when :admin
			case ENV['ENVIRONMENT']
			when :dev then $base_url = 'https://dev.spectrumcustomizer.com/admin/' and $username = 'test_sa' and $password = 'SuperUser#1'
			when :staging then $base_url = 'http://demo.spectrumcustomizer.com/admin/' and $username = 'test_sa' and $password = 'SuperUser#1'
			when :prod then $base_url = 'http://demo.spectrumcustomizer.com/admin/' and $username = 'test_sa' and $password = 'SuperUser#1'
			end
		when :eto then $base_url = "https://forms.energytrust.org/esf"
		when :'under-armour'
			case ENV['ENVIRONMENT'].to_sym
			when :test then $base_url =  'http://demo.madetoordercustomizer.com/under-armour/test/uau/frontend/index.html#/materialIds/'
			when :staging then $base_url = 'http://demo.madetoordercustomizer.com/under-armour/staging/uau/frontend/index.html#/materialIds/'
			when :prod then $base_url = 'http://demo.madetoordercustomizer.com/under-armour/production/uau/frontend/index.html#/materialIds/'
			end
		when :preview then $base_url = 'http://preview.madetoordercustomizer.com/'
		when :'eddie-bauer'
			case ENV['ENVIRONMENT'].to_sym
			when :prod then $base_url = 'http://www.eddiebauer.com/product/'
			when :staging then $base_url = 'http://staging.eddiebauer.com/product/'
			end
		end
	end
end