class IconBasePage < BasePage

	include PageObject

	text_field(:username,		id: 		"username")
	text_field(:password,		id: 		"password")
	button(:close_popup,		class: 		["close"])


	def self.get_pattern_categories(scenefile)
		@patterns = Hash.new { |hash, key| hash[key] = [] }
		@response = JSON.parse(RestClient.get("http://test.spectrumcustomizer.com/api/assets/productset/#{scenefile}"){|response, request, result| response })
		@response["assetsTypes"].each do |at|
			if at["assetType"] == "pattern"
				at["assets"].each do |a|
					if a["categories"].empty? == false && a["tags"].empty? == false
						a["tags"].each do |t|
							if t["term"].include?("current")
								@patterns[a["descriptionHandle"]] << t["term"]
							end 
						end
					end
				end
			end
		end
		return @patterns
	end

	def self.get_print_categories(scenefile)
		@prints = Hash.new { |hash, key| hash[key] = [] }
		@response = JSON.parse(RestClient.get("http://test.spectrumcustomizer.com/api/assets/productset/#{scenefile}"){|response, request, result| response })
		@response["assetsTypes"].each do |at|
			if at["assetType"] == "stock"
				at["assets"].each do |a|
					if a["categories"].empty? == false && a["tags"].empty? == false
						a["tags"].each do |t|
							if t["term"].include?("current")
								@prints[a["descriptionHandle"]] << t["term"]
							end
						end
					end
				end
			end
		end
		return @prints
	end

	def icon_login
		if self.close_popup_element.exists? && self.close_popup_element.visible?
			self.close_popup
		end
		self.username = "john.fisher@pollinate.com"
		self.password = "IamQuality!"
		self.send_keys :return
	end

	


	def self.spec_response(recipe_id)
		case ENV['ENVIRONMENT'].to_sym
		when :test then @response = Nokogiri::XML.parse(RestClient.get("test.spectrumcustomizer.com/under-armour/icon/specification/#{recipe_id}/html"){|response, request, result| response })
		when :staging then @response = Nokogiri::XML.parse(RestClient.get("staging.spectrumcustomizer.com/under-armour/icon/specification/#{recipe_id}/html"){|response, request, result| response })
		when :prod then @response = Nokogiri::XML.parse(RestClient.get("api.spectrumcustomizer.com/under-armour/icon/specification/#{recipe_id}/html"){|response, request, result| response })
		end
		return @response
	end



	def self.packlist_response(recipe_id)
		case ENV['ENVIRONMENT'].to_sym
		when :test then @response = Nokogiri::XML.parse(RestClient.get("test.spectrumcustomizer.com/under-armour/icon/packlist/#{recipe_id}/html"){|response, request, result| response })
		when :staging then @response = Nokogiri::XML.parse(RestClient.get("staging.spectrumcustomizer.com/under-armour/icon/packlist/#{recipe_id}/html"){|response, request, result| response })
		when :prod then @response = Nokogiri::XML.parse(RestClient.get("api.spectrumcustomizer.com/under-armour/icon/packlist/#{recipe_id}/html"){|response, request, result| response })
		end
		return @response
	end



	def self.parse_icon_packlist(recipe_id)
		@packlist = Array.new
		@keys = Array.new
		@values = Array.new
		@hash = Hash.new{ |hsh,key| hsh[key] = [] }
		@response = packlist_response(recipe_id)
		@response.search('div[class="info"]').each do |i|
			body = i.inner_text.gsub('<br>',"")
			pair = body.scan(/\S.*/)
			@hash.store(pair[0],pair[1].strip)
		end
		return @hash
	end



	def self.parse_icon_spec(recipe_id)
		details = parse_icon_spec_details(recipe_id)
		images = parse_icon_spec_images(recipe_id)
		colors = parse_icon_spec_colors(recipe_id)
		return details,images,colors
	end



	def self.parse_icon_spec_details(recipe_id)
		@arr = Array.new
		@hash = Hash.new{ |hsh,key| hsh[key] = [] }
		@keys = Array.new
		@values = Array.new
		@images = Array.new
		@response = spec_response(recipe_id)
		row_recipe = @response.search('div[class="row recipe"]')
		rows = row_recipe.search('td').each do |node|
			@arr.push(node.inner_text.scan(/\S.*/))
		end
		array = @arr.flatten
		array.each do |string|
			if string.include?('SIZE TYPE:') == false
				if string.include?(":")
					@keys.push(string.gsub(/:/,'').downcase)
				else
					@values.push(string)
				end
			end
		end
		links = @response.search('a')
		@keys.zip(@values).each do |k,v|
			if v == "Download All Printable Assets"
				@hash.store(k,links[2]['href'])
			elsif v == "Download Printable Pack List"
				@hash.store(k,links[1]['href'])
			else
				@hash.store(k, v)
			end
		end
		return @hash
	end



	def self.parse_icon_spec_images(recipe_id)
		@images = Array.new
		@response = spec_response(recipe_id)
		images = @response.search('img')
		images.each do |img|
			if (img['width'] == "400") == false
				@images.push(img['src'])
			end
		end
		return @images
	end




	def self.parse_icon_spec_colors(recipe_id)
		@hash = Hash.new{ |hsh,key| hsh[key] = [] }
		@table = Array.new
		@keys = Array.new
		@values = Array.new
		@response = spec_response(recipe_id)
		@response.search('table[class="table"]').each do |e|
			element = e.text.scan(/\S*\w.*\S/)
			element.each do |s|
				s.gsub("-"," ")
				@table.push(s.strip)
			end
		end
		@table.each do |string|
			if string.include?(":")
				@keys.push(string.gsub(/:/,'').downcase)
			else
				@values.push(string)
			end
		end
		@keys.zip(@values).each do |k,v|
			@hash.store(k.gsub(/\W+/,' '), v)
		end
		return @hash
	end







	def self.get_product_set_handle(recipe_id)
		case ENV['ENVIRONMENT'].to_sym
		when :test then @response = JSON.parse(RestClient.get("test.spectrumcustomizer.com/api/recipesets/readable/#{recipe_id}"){|response, request, result| response })
		when :staging then @response = JSON.parse(RestClient.get("staging.spectrumcustomizer.com/api/recipesets/readable/#{recipe_id}"){|response, request, result| response })
		when :prod then @response = JSON.parse(RestClient.get("api.spectrumcustomizer.com/api/recipesets/readable/#{recipe_id}"){|response, request, result| response })
		end
		return @response['contents']['productSetHandle']
	end








	def self.parse_recipeset(recipe_id)
		@rset = Hash.new{ |hsh,key| hsh[key] = [] }
		case ENV['ENVIRONMENT'].to_sym
		when :test then @response = JSON.parse(RestClient.get("test.spectrumcustomizer.com/api/recipesets/readable/#{recipe_id}"){|response, request, result| response })
		when :staging then @response = JSON.parse(RestClient.get("staging.spectrumcustomizer.com/api/recipesets/readable/#{recipe_id}"){|response, request, result| response })
		when :prod then @response = JSON.parse(RestClient.get("api.spectrumcustomizer.com/api/recipesets/readable/#{recipe_id}"){|response, request, result| response })
		end
		@response['contents']['recipes'].each do |recipes|
			recipes['recipe']['recipeData'].each do |recipeData|
				recipeData['childFeatures'].each do |childFeatures|
					if (childFeatures.empty? == false || childFeatures.nil? == false)
						if (childFeatures['featureHandle'].nil? == false && childFeatures['selectionHandle'].nil? == false)
							if (childFeatures['featureHandle'].include?("-color") == true && childFeatures['featureHandle'].include?("roughness") == false && childFeatures['featureHandle'].include?("toggle") == false && childFeatures['featureHandle'].include?("-upper-color") == false)
								@rset.store(childFeatures['featureHandle'], childFeatures['selectionHandle'])
							end
						end
						if childFeatures['selectionGroup'].nil? == false
							childFeatures['selectionGroup']['selections'].each do |selections|
								if (selections.empty? == false || selections.nil? == false)
									if (selections['childFeatures'].nil? == false || selections['childFeatures'].empty? == false)
										selections['childFeatures'].each do |features|
											if (features.empty? == false || features.nil? == false)
												if (features['featureHandle'].nil? == false && features['selectionHandle'].nil? == false)
													if (features['featureHandle'].include?("-color") || features['featureHandle'].include?("roughness") == false && features['featureHandle'].include?("toggle") == false && features['featureHandle'].include?("-upper-color") == false)
														@rset.store(features['featureHandle'], features['selectionHandle'])
													end
												end
											end
										end
									end
								end
							end
						end
					end
				end
			end
		end
		return @rset
	end


	# HANDLES #


	$gallery_handles = [
		"uaf-gallery-spring-break-ignite-mens-2019",
		"uaf-gallery-spring-break-ignite-womens-2019",
		"uaf-gallery-staging-activemixedproducts",
		"uaf-gallery-curry6-mens-inspiration",
		"uaf-gallery-curry6-youth-inspiration",
		"uaf-gallery-yard-mens-inspiration",
		"uaf-gallery-kicksprint2-mens-inspiration",
		"uaf-gallery-creator-collabs",
		"uaf-gallery-artist-collab-dez-kreative-customz",
		"uaf-gallery-basketball-starting-5",
		"uaf-gallery-ignite-mens-inspiration-2018",
		"uaf-gallery-ignite-womens-inspiration-2018",
		"uaf-gallery-spotlight-mens-inspiration-2018",
		"uaf-gallery-hovrhavoc-mens-inspiration-2018",
		"uaf-gallery-hovrhavoc-womens-inspiration-2018",
		"uaf-gallery-hovrhavoclow-mens-inspiration-2018",
		"uaf-gallery-hovrslk-mens-inspiration-2018",
		"uaf-gallery-hovrslk-womens-inspiration-2018",
		"uaf-gallery-highlight-mens-inspiration-2018",
		"uaf-gallery-highlight",
		"uaf-gallery-highlight-fourth",
		"uaf-gallery-highlight-mens-inspiration",
		"uaf-gallery-highlight-sept17",
		"uaf-gallery-sack-pack-inspiration",
		"uaf-gallery-highlight-s318",
		"uaf-gallery-sackpack-inspirations",
		"uaf-gallery-wgyc",
		"uaf-gallery-country-pride",
		"uaf-gallery-curry5"
	]

	$icon_pid = [	
		# "pid3000416",
		# "pid3000414",
		# "pid3022401",
		# "pid3022609",
		# "pid3022400",
		# "pid3020925",
		# "pid3020926",
		# "pid3021634",
		# "pid3021635",
		# "pid3021636",
		# "pid3021637",
		# "pid3021729",
		# "pid3021728",
		# "pid3022240",
		# "pid3000042",
		# "pid3022239",
		"pid3000043",
		"pid3020924",
		"pid1326338",
		"pid3022238",
		"pid3022403",
		"pid3022404",
		"pid3022405",
		"pid3022406",
		"pid3022402",
		"pid3022417",
		"pid3022413",
		"pid3022411",
		"pid3022412"
	]

	$icon_handles = [	
		'uaf-prs-spotlightdevtest',
		'uaf-prs-spotlight-mens',
		'uaf-prs-highlight-mens',
		'uaf-prs-icon-sackpack',
		'uaf-prs-ignite-mens',
		'uaf-prs-ignite-womens',
		'uaf-prs-hovrhavoclow-mens',
		'uaf-prs-hovrhavoc-mens',
		'uaf-prs-hovrhavoc-womens',
		'uaf-prs-hovrslk-womens',
		'uaf-prs-kicksprint2-mens',
		'uaf-prs-curry6-mens',
		'uaf-prs-curry6-youth',
		'uaf-prs-yard-mens',
		'uaf-prs-hovrslk-mens',
		'uaf-prs-hovrphantom-mens',
		'uaf-prs-hovrphantom-womens',
		'uaf-prs-highlightmc2019-mens',
		'uaf-prs-curry6-iwd'
	]


	# COLOR LOGIC #


	$iwd = {
		:pattern => ["White", "Black", "Elemental", "Gray Flux", "Pitch Gray", "Mod Gray", "Jet Gray", "Khaki Base", "Taxi", "Steeltown Gold", "Team Orange", "Red", "Cardinal", "Maroon", "Tropic Pink", "Purple", "Carolina Blue", "St. Tropez", "Royal", "Academy", "Team Kelly Green", "Forest Green", "Texas Orange", "Cleveland Brown", "Silt Brown", "Vegas Gold", "Dust", "Aqua Foam", "Lima Bean", "Canyon Green", "Orange Glitch", "Coho", "Deep Orchid", "Flight Purple"],
		:solid => ["Curry 6 IWD Sockliner Artwork Template","White", "Black", "Elemental", "Gray Flux", "Pitch Gray", "Mod Gray", "Jet Gray", "Khaki Base", "Taxi", "Steeltown Gold", "Team Orange", "Red", "Cardinal", "Maroon", "Tropic Pink", "Purple", "Carolina Blue", "St. Tropez", "Royal", "Academy", "Team Kelly Green", "Forest Green", "Texas Orange", "Cleveland Brown", "Silt Brown", "Vegas Gold", "Dust", "Aqua Foam", "Lima Bean", "Canyon Green", "Orange Glitch", "Coho", "Deep Orchid", "Flight Purple"],
		:artwork_style => ["Full Takeover", "Tile"],
		:artwork => ["White", "Black", "Elemental", "Gray Flux", "Pitch Gray", "Mod Gray", "Jet Gray", "Khaki Base", "Taxi", "Steeltown Gold", "Team Orange", "Red", "Cardinal", "Maroon", "Tropic Pink", "Purple", "Carolina Blue", "St. Tropez", "Royal", "Academy", "Team Kelly Green", "Forest Green", "Texas Orange", "Cleveland Brown", "Silt Brown", "Vegas Gold", "Dust", "Aqua Foam", "Lima Bean", "Canyon Green", "Orange Glitch", "Coho", "Deep Orchid", "Flight Purple"],
		:knit => ["Flight Purple"],
		:heel_knit_dots => ["Black", "White", "Pitch Gray", "Academy", "High Vis Yellow", "Elemental", "Deep Orchid"],
		:heel_tape => ["Flight Purple"],
		:tongue_logo => ["Flight Purple"],
		:heel_counter => ["Deep Orchid"],
		:heel_counter_stitching => ["Deep Orchid"],
		:hftpu => ["Deep Orchid", "Black", "Royal", "White", "Taxi", "Red", "Academy", "Orange Glitch", "Aqua Foam", "High Vis Yellow", "Mod Gray"],
		:toe_hotmelt => ["Flight Purple"],
		:laces => ["Deep Orchid"],
		:cables => ["Flight Purple"],
		:midsole => ["Flight Purple"],
		:energy_web => ["Deep Orchid"],
		:sc_logo => ["White"],
		:external_shank_signature_icdat => ["Deep Orchid"],
		:outsole => ["Deep Orchid"],
		:left_sockliner_text => ["White", "Black", "Elemental", "Gray Flux", "Pitch Gray", "Mod Gray", "Jet Gray", "Khaki Base", "Taxi", "Steeltown Gold", "Team Orange", "Red", "Cardinal", "Maroon", "Tropic Pink", "Purple", "Carolina Blue", "St. Tropez", "Royal", "Academy", "Team Kelly Green", "Forest Green", "Texas Orange", "Cleveland Brown", "Silt Brown", "Vegas Gold", "Dust", "Aqua Foam", "Lima Bean", "Canyon Green", "Orange Glitch", "Coho", "Deep Orchid", "Flight Purple"],
		:left_tongue_text => ["Deep Orchid", "White"],
		:knit_forefoot => ["Deep Orchid"],
		:tongue_lining => ["Flight Purple"],
		:knit_toe => ["Deep Orchid"],
		:right_tongue_text => ["Deep Orchid", "White"],
		:right_sockliner_text => ["White", "Black", "Elemental", "Gray Flux", "Pitch Gray", "Mod Gray", "Jet Gray", "Khaki Base", "Taxi", "Steeltown Gold", "Team Orange", "Red", "Cardinal", "Maroon", "Tropic Pink", "Purple", "Carolina Blue", "St. Tropez", "Royal", "Academy", "Team Kelly Green", "Forest Green", "Texas Orange", "Cleveland Brown", "Silt Brown", "Vegas Gold", "Dust", "Aqua Foam", "Lima Bean", "Canyon Green", "Orange Glitch", "Coho", "Deep Orchid", "Flight Purple"]
	}

	$highlight = {
		:tongue_logo => ["White", "Steel", "Black", "Cardinal", "Red", "Tropic Pink", "Team Orange", "Steeltown Gold", "Team Kelly Green", "Forest Green", "Neptune", "Carolina Blue", "Team Royal", "Midnight Navy", "Purple"],
		:stitching => ["White", "Steel", "Team Royal", "Black", "Midnight Navy", "Taxi", "Carolina Blue", "Red", "Phoenix Fire", "Vapor Green", "Ice Blue - Translucent", "Aluminum", "Belt Blue", "Bitter", "Blaze Orange", "Capri", "Cardinal", "Cerise", "Charcoal", "Classic Green", "Cleveland Brown", "Maroon", "Elemental", "Dark Orange", "Dune", "Fluo Pink", "Forest Green", "Graphite", "Gravel", "High Vis Yellow", "Hyper Green", "Neo Pulse", "Purple", "Sandstorm", "Solder", "St. Tropez", "Steeltown Gold", "Stone", "Team Orange", "Texas Orange", "Tropic Pink", "Vegas Gold", "Velocity", "X Ray"],
		:wordmark => ["White", "Taxi", "Midnight Navy", "Steel", "Red", "Phoenix Fire", "Ice Blue - Translucent", "Vapor Green", "Aluminum", "Bitter", "Capri", "Cardinal", "Blaze Orange", "Belt Blue", "Cerise", "Charcoal", "Team Kelly Green", "Cleveland Brown", "Maroon", "Dark Orange", "Dune", "Fluo Pink", "Elemental", "Graphite", "Forest Green", "Gravel", "High Vis Yellow", "Hyper Green", "Neo Pulse", "Purple", "Sandstorm", "Solder", "St. Tropez", "Steeltown Gold", "Stone", "Team Orange", "Texas Orange", "Tropic Pink", "Vegas Gold", "Velocity", "X Ray", "Black", "Carolina Blue", "Team Royal"],
		:outsole => ["Chrome", "Transparent"],
		:lining => ["Black", "Aluminum", "Red", "Team Royal", "Midnight Navy", "White", "Steel"],
		:upper => ["Under Armour Highlight Upper Artwork Template","Black","Elemental","Jet Gray","Mod Gray","Pitch Gray","White","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Orange Glitch","Steel","Midnight Navy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink"],
		:tongue => ["White","Black","Steel"],
		:laces => ["White","Black","Steel","Red","Team Orange","Steeltown Gold","Forest Green","Team Royal","Midnight Navy"],
		:heel => ["White","Black","Steel","Cardinal","Red","Tropic Pink","Team Orange","Steeltown Gold","Team Kelly Green","Forest Green","Carolina Blue","Team Royal","Midnight Navy","Purple"],
		:plate => ["White","Black","Steel","Red","Team Orange","Steeltown Gold","Forest Green","Team Royal","Midnight Navy"],
		:skeleton => ["White","Black"],
		:logo => ["White","Black","Steel","Cardinal","Red","Tropic Pink","Team Orange","Steeltown Gold","Team Kelly Green","Forest Green","Carolina Blue","Team Royal","Midnight Navy","Purple"],
		:logo_outline => ["Charcoal","White","Black","Aluminum","Steel","Cardinal","Red","Tropic Pink","Team Orange","Steeltown Gold","Team Kelly Green","Forest Green","Carolina Blue","Team Royal","Midnight Navy","Purple"]
	}



	$sackpack = {
		:upper => ["Under Armour Highlight Upper Pattern Template","Royal","Black","Elemental","Jet Gray","Mod Gray","Pitch Gray","White","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Orange Glitch","Steel","Midnight Navy","Academy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Texas Orange","Tropic Pink","Vegas Gold"],
		:logo => ["Black","White","Steel","Red","Academy","Royal","Taxi","Team Orange","Tropic Pink","Metallic Silver","Metallic Gold"],
		:expandable => ["White","Black","Steel","Red","Tropic Pink","Team Orange","Taxi","Royal","Academy"],
		:lining => ["White","Black","Steel"],
		:zipper => ["White","Black","Steel"],
		:back => ["White","Black","Steel","Red","Tropic Pink","Team Orange","Taxi","Royal","Academy"],
		:straps => ["White","Black","Steel","Red","Academy"],
		:webbing => ["White","Black","Steel"],
		:label => ["Black", "White", "Steel"]
	}

	$mens_ignite = {
		:pattern => ["Desert Sky", "Jupiter Blue", "Black", "Elemental", "Jet Gray", "Mod Gray", "Pitch Gray", "White", "Gray Flux", "Khaki Base", "Silt Brown", "Aqua Foam", "Canyon Green", "Coho", "Dust", "Lima Bean", "Orange Glitch", "Steel", "Midnight Navy", "Cardinal", "Carolina Blue", "Cleveland Brown", "Dark Orange", "Forest Green", "Maroon", "Purple", "Red", "St. Tropez", "Steeltown Gold", "Taxi", "Team Kelly Green", "Team Orange", "Team Royal", "Texas Orange", "Tropic Pink", "Vegas Gold"],
		:solid => ["UA Ignite Footbed Artwork Template","Desert Sky", "Jupiter Blue", "Black", "Elemental", "Jet Gray", "Mod Gray", "Pitch Gray", "White", "Gray Flux", "Khaki Base", "Silt Brown", "Aqua Foam", "Canyon Green", "Coho", "Dust", "Lima Bean", "Orange Glitch", "Steel", "Midnight Navy", "Cardinal", "Carolina Blue", "Cleveland Brown", "Dark Orange", "Forest Green", "Maroon", "Purple", "Red", "St. Tropez", "Steeltown Gold", "Taxi", "Team Kelly Green", "Team Orange", "Team Royal", "Texas Orange", "Tropic Pink", "Vegas Gold"],
		:strap => ["White","Black","Steel","Cardinal","Red","Team Orange","Steeltown Gold","Forest Green","Team Royal","Midnight Navy"],
		:strap_logo => ["White","Black","Metallic Silver","Steel","Cardinal","Red","Team Orange","Steeltown Gold","Metallic Gold","Forest Green","Team Royal","Midnight Navy"],
		:lining => ["White","Black","Steel","Cardinal","Red","Team Orange","Steeltown Gold","Forest Green","Team Royal","Midnight Navy"],
		:outsole => ["White","Black"],
		:left_text => ["Desert Sky", "Jupiter Blue", "Black", "Elemental", "Jet Gray", "Mod Gray", "Pitch Gray", "Gray Flux", "Khaki Base", "Silt Brown", "Aqua Foam", "Canyon Green", "Coho", "Dust", "Lima Bean", "Orange Glitch", "Steel", "Midnight Navy", "Cardinal", "Carolina Blue", "Cleveland Brown", "Dark Orange", "Forest Green", "Maroon", "Purple", "Red", "St. Tropez", "Steeltown Gold", "Taxi", "Team Kelly Green", "Team Orange", "Team Royal", "Texas Orange", "Tropic Pink", "Vegas Gold"],
		:right_text => ["Desert Sky", "Jupiter Blue", "Black", "Elemental", "Jet Gray", "Mod Gray", "Pitch Gray", "Gray Flux", "Khaki Base", "Silt Brown", "Aqua Foam", "Canyon Green", "Coho", "Dust", "Lima Bean", "Orange Glitch", "Steel", "Midnight Navy", "Cardinal", "Carolina Blue", "Cleveland Brown", "Dark Orange", "Forest Green", "Maroon", "Purple", "Red", "St. Tropez", "Steeltown Gold", "Taxi", "Team Kelly Green", "Team Orange", "Team Royal", "Texas Orange", "Tropic Pink", "Vegas Gold"],
	}

	$womens_ignite = {
		:pattern => ["Desert Sky", "Jupiter Blue", "Black", "Elemental", "Jet Gray", "Mod Gray", "Pitch Gray", "White", "Gray Flux", "Khaki Base", "Silt Brown", "Aqua Foam", "Canyon Green", "Coho", "Dust", "Lima Bean", "Orange Glitch", "Steel", "Midnight Navy", "Cardinal", "Carolina Blue", "Cleveland Brown", "Dark Orange", "Forest Green", "Maroon", "Purple", "Red", "St. Tropez", "Steeltown Gold", "Taxi", "Team Kelly Green", "Team Orange", "Team Royal", "Texas Orange", "Tropic Pink", "Vegas Gold"],
		:solid => ["UA Ignite Footbed Artwork Template","Desert Sky", "Jupiter Blue", "Black", "Elemental", "Jet Gray", "Mod Gray", "Pitch Gray", "White", "Gray Flux", "Khaki Base", "Silt Brown", "Aqua Foam", "Canyon Green", "Coho", "Dust", "Lima Bean", "Orange Glitch", "Steel", "Midnight Navy", "Cardinal", "Carolina Blue", "Cleveland Brown", "Dark Orange", "Forest Green", "Maroon", "Purple", "Red", "St. Tropez", "Steeltown Gold", "Taxi", "Team Kelly Green", "Team Orange", "Team Royal", "Texas Orange", "Tropic Pink", "Vegas Gold"],
		:strap => ["Aluminum", "Black", "Carolina Blue", "Desert Sky", "Jupiter Blue", "Purple", "Tropic Pink", "White"],
		:strap_logo => ["Aluminum", "Black", "Carolina Blue", "Desert Sky", "Jupiter Blue", "Metallic Gold", "Metallic Silver", "Purple", "Tropic Pink", "White"],
		:lining =>  ["Aluminum", "Black", "Carolina Blue", "Desert Sky", "Jupiter Blue", "Purple", "Tropic Pink", "White"],
		:outsole => ["White","Black"],
		:left_text => ["Desert Sky", "Jupiter Blue", "Black", "Elemental", "Jet Gray", "Mod Gray", "Pitch Gray", "Gray Flux", "Khaki Base", "Silt Brown", "Aqua Foam", "Canyon Green", "Coho", "Dust", "Lima Bean", "Orange Glitch", "Steel", "Midnight Navy", "Cardinal", "Carolina Blue", "Cleveland Brown", "Dark Orange", "Forest Green", "Maroon", "Purple", "Red", "St. Tropez", "Steeltown Gold", "Taxi", "Team Kelly Green", "Team Orange", "Team Royal", "Texas Orange", "Tropic Pink", "Vegas Gold"],
		:right_text => ["Desert Sky", "Jupiter Blue", "Black", "Elemental", "Jet Gray", "Mod Gray", "Pitch Gray", "Gray Flux", "Khaki Base", "Silt Brown", "Aqua Foam", "Canyon Green", "Coho", "Dust", "Lima Bean", "Orange Glitch", "Steel", "Midnight Navy", "Cardinal", "Carolina Blue", "Cleveland Brown", "Dark Orange", "Forest Green", "Maroon", "Purple", "Red", "St. Tropez", "Steeltown Gold", "Taxi", "Team Kelly Green", "Team Orange", "Team Royal", "Texas Orange", "Tropic Pink", "Vegas Gold"],
	}



	$spotlight = {
		:pattern => ["Black","Elemental","Jet Gray","Mod Gray","Pitch Gray","White","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Orange Glitch","Steel","Midnight Navy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink"],
		:solid => ["Spotlight Upper Artwork Template","Black","Elemental","Jet Gray","Mod Gray","Pitch Gray","White","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Orange Glitch","Steel","Midnight Navy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink"],
		:knit => ["White","Black"],
		:laces => ["Black","Midnight Navy","Red","White","Forest Green","Team Orange","Steeltown Gold","Steel","Team Royal","Cardinal"],
		:eyelets => ["Black","Midnight Navy","Red","White","Forest Green","Team Orange","Steeltown Gold","Steel","Team Royal","Cardinal"],
		:toe_logo => ["Black","Midnight Navy","Red","White","Forest Green","Team Orange","Steeltown Gold","Steel","Team Royal","Cardinal"],
		:toe_logo_outline => ["Black","Midnight Navy","Red","White","Forest Green","Team Orange","Steeltown Gold","Steel","Team Royal","Charcoal","Aluminum","Cardinal"],
		:heel => ["Black","Midnight Navy","Red","White","Forest Green","Team Orange","Steeltown Gold","Steel","Team Royal","Cardinal"],
		:weld => ["Chrome"],
		:medial => ["Metallic Silver"],
		:heel_logo_outline => ["Black","Midnight Navy","Red","White","Forest Green","Team Orange","Steeltown Gold","Steel","Team Royal","Cardinal"],
		:plate => ["Gunmetal Chrome","Silver Chrome","Solar Chrome"],
		:trim => ["Black","Midnight Navy","Red","White","Forest Green","Team Orange","Steeltown Gold","Steel","Team Royal","Cardinal"],
		:medial_outline => ["Black","Midnight Navy","Red","White","Forest Green","Team Orange","Steeltown Gold","Steel","Team Royal","Charcoal"],
		:left_text => ["Black","Elemental","Jet Gray","Mod Gray","Pitch Gray","White","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Orange Glitch","Steel","Midnight Navy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink"],
		:right_text => ["Black","Elemental","Jet Gray","Mod Gray","Pitch Gray","White","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Orange Glitch","Steel","Midnight Navy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink"]
	}



	$hovr_havoc_mid = {
		:pattern => ["Aluminum","Black","Graphite","White","After Burn","Brick Red","Charcoal","Flushed Pink","Ghost Gray","Mink Gray","Pixel Purple","Techno Teal","Venetian Blue","Blue Jet","Constellation Purple","Magma Orange","Elemental","Jet Gray","Mod Gray","Pitch Gray","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Orange Glitch","Steel","Academy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink","Vegas Gold"],
		:solid => ["Upper Artwork","Aluminum","Black","Graphite","White","After Burn","Brick Red","Charcoal","Flushed Pink","Ghost Gray","Mink Gray","Pixel Purple","Techno Teal","Venetian Blue","Blue Jet","Constellation Purple","Magma Orange","Elemental","Jet Gray","Mod Gray","Pitch Gray","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Orange Glitch","Steel","Academy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink","Vegas Gold"],
		:tongue => ["White", "Black", "Steel", "Red", "Taxi", "Team Royal", "Academy", "High Vis Yellow"],
		:heel => ["White","Black", "Steel", "Gold", "Red", "Taxi", "Team Royal", "Academy","High Vis Yellow", "Magma Orange", "Tropic Pink"],
		:heel_logo => ["White","Black", "Steel", "Gold", "Red", "Taxi", "Team Royal", "Academy","High Vis Yellow", "Magma Orange", "Tropic Pink"],
		:lining => ["Black", "Steel","Red", "Taxi","Team Royal", "Academy","High Vis Yellow", "Magma Orange", "Gold", "Tropic Pink"],
		:lycra => ["Black", "White", "Team Royal", "Academy", "Taxi", "Red", "Steel", "Cardinal", "Carolina Blue", "Forest Green"],
		:bemis => ["Black", "White", "Team Royal", "Academy", "Taxi", "Red", "Steel", "Cardinal", "Carolina Blue", "Forest Green"],
		:midfoot => ["White", "Black", "Steel", "Red", "Taxi","Team Royal", "Academy","High Vis Yellow", "Magma Orange", "Tropic Pink"],
		:forefoot => ["Black", "White", "Team Royal", "Academy", "Taxi", "Red", "Steel","High Vis Yellow", "Magma Orange", "Tropic Pink"],
		:laces => ["White", "Black", "Steel", "Red", "Taxi","Team Royal", "Academy","High Vis Yellow", "Magma Orange", "Tropic Pink"],
		:lace_strap => ["White", "Black", "Steel","Red", "Taxi", "Team Royal", "Academy","High Vis Yellow", "Magma Orange", "Tropic Pink"],
		:strap_logo => ["White", "Steel", "Black"],
		:eyelets => ["Black", "White", "Team Royal", "Academy", "Taxi", "Red", "Steel", "High Vis Yellow", "Magma Orange", "Tropic Pink"],
		:webbing => ["Black", "White", "Team Royal", "Academy", "Taxi", "Red", "Steel", "High Vis Yellow", "Magma Orange", "Tropic Pink"],
		:midsole => ["White", "Black", "Steel"],
		:cushion => ["White", "Steel", "Black"],
		:mesh => ["Black", "Steel", "Red", "Taxi", "Team Royal", "Academy","High Vis Yellow", "Magma Orange", "Gold", "Tropic Pink"],
		:outsole => ["White", "Black", "Ice Blue - Translucent","White/Gray\u2014Camo"],
		:logo => ["Black", "White"]
	}

	$hovr_havoc = {
		:pattern => ["Aluminum","Black","Graphite","White","After Burn","Brick Red","Charcoal","Flushed Pink","Ghost Gray","Mink Gray","Pixel Purple","Techno Teal","Venetian Blue","Blue Jet","Constellation Purple","Elemental","Jet Gray","Mod Gray","Pitch Gray","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Orange Glitch","Steel","Academy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink","Vegas Gold","Magma Orange"],
		:solid => ["Upper Artwork","Aluminum","Black","Graphite","White","After Burn","Brick Red","Charcoal","Flushed Pink","Ghost Gray","Mink Gray","Pixel Purple","Techno Teal","Venetian Blue","Blue Jet","Constellation Purple","Elemental","Jet Gray","Mod Gray","Pitch Gray","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Orange Glitch","Steel","Academy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink","Vegas Gold","Magma Orange"],
		:tongue => ["White", "Black", "Steel", "Red", "Taxi", "Team Royal", "Academy", "High Vis Yellow"],
		:heel => ["White","Black", "Steel", "Gold", "Red", "Taxi", "Team Royal", "Academy","High Vis Yellow", "Magma Orange", "Tropic Pink"],
		:heel_logo => ["White","Black", "Team Royal", "Academy", "Taxi", "Red", "Steel", "Gold", "High Vis Yellow", "Magma Orange", "Tropic Pink"],
		:lining => ["Black", "Steel","Red", "Taxi","Team Royal", "Academy","High Vis Yellow", "Magma Orange", "Gold", "Tropic Pink"],
		:lycra => ["Black", "Team Royal", "Academy", "Taxi", "Red", "Steel","High Vis Yellow", "Magma Orange", "Gold", "Tropic Pink"],
		:midfoot => ["Black", "White", "Team Royal", "Academy", "Taxi", "Red", "Steel", "High Vis Yellow", "Magma Orange", "Tropic Pink"],
		:forefoot => ["Black", "White", "Team Royal", "Academy", "Taxi", "Red", "Steel", "High Vis Yellow", "Magma Orange", "Tropic Pink"],
		:laces => ["White", "Black", "Steel", "Red", "Taxi","Team Royal", "Academy","High Vis Yellow", "Magma Orange", "Tropic Pink"],
		:webbing => ["Black", "White", "Team Royal", "Academy", "Taxi", "Red", "Steel", "High Vis Yellow", "Magma Orange", "Tropic Pink"],
		:tongue_logo => ["White", "Black", "Steel"],
		:eyelets => ["White", "Steel", "Red", "Team Royal", "Black", "Taxi", "Academy","High Vis Yellow", "Magma Orange", "Tropic Pink"],
		:stitching => ["Steel", "Team Royal", "Black", "White", "Red", "Taxi","Academy","High Vis Yellow", "Magma Orange", "Tropic Pink"],
		:midsole => ["Black", "White", "Steel"],
		:mesh => ["Black", "Steel", "Red", "Taxi", "Team Royal", "Academy", "High Vis Yellow", "Magma Orange", "Gold", "Tropic Pink"],
		:outsole => ["Black", "White", "Ice Blue - Translucent","White/Gray\u2014Camo"],
		:logo => ["Black", "White"]
	}

	$hovr_slk = {
		:pattern => ["Aluminum","Black","Graphite","White","After Burn","Brick Red","Charcoal","Flushed Pink","Ghost Gray","Mink Gray","Pixel Purple","Techno Teal","Venetian Blue","Downtown Green","Elemental","Jet Gray","Mod Gray","Pitch Gray","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Orange Glitch","Steel","Academy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink","Vegas Gold"],
		:solid => ["Artwork","Aluminum","Black","Graphite","White","After Burn","Brick Red","Charcoal","Flushed Pink","Ghost Gray","Mink Gray","Pixel Purple","Techno Teal","Venetian Blue","Downtown Green","Elemental","Jet Gray","Mod Gray","Pitch Gray","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Orange Glitch","Steel","Academy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink"],
		:heel => ["White", "Black", "Ghost Gray", "Academy", "Brick Red", "After Burn", "Venetian Blue"],
		:heel_lining => ["White", "Black", "Ghost Gray", "Academy", "Downtown Green", "Brick Red", "After Burn", "Flushed Pink", "Venetian Blue"],
		:tongue => ["White", "Black", "Ghost Gray", "Academy", "Downtown Green", "Brick Red", "After Burn", "Flushed Pink", "Venetian Blue"],
		:lining_tongue => ["White", "Black", "Ghost Gray", "Academy", "Downtown Green", "Brick Red", "After Burn", "Flushed Pink", "Venetian Blue"],
		:branding => ["White", "Black", "Ghost Gray", "Academy", "Downtown Green", "Brick Red", "After Burn", "Flushed Pink", "Venetian Blue"],
		:binding => ["White", "Black", "Ghost Gray", "Academy", "Downtown Green", "Brick Red", "After Burn", "Flushed Pink", "Venetian Blue"],
		:laces => ["White", "Black", "Ghost Gray", "Academy", "Downtown Green", "Brick Red", "After Burn", "Flushed Pink", "Venetian Blue"],
		:webbing => ["White", "Black", "Ghost Gray", "Academy", "Downtown Green", "Brick Red", "After Burn", "Flushed Pink", "Venetian Blue"],
		:eyestay => ["White", "Black", "Ghost Gray", "Academy", "Downtown Green", "Brick Red", "After Burn", "Flushed Pink", "Venetian Blue"],
		:thread => ["White", "Ghost Gray", "After Burn", "Black", "Downtown Green", "Academy", "Brick Red", "Flushed Pink", "Venetian Blue"],
		:quarter => ["White", "Downtown Green", "After Burn", "Black", "Academy", "Brick Red", "Ghost Gray", "Venetian Blue", "Flushed Pink"],
		:tab => ["White", "Black", "Ghost Gray", "After Burn", "Academy", "Brick Red", "Flushed Pink", "Downtown Green", "Venetian Blue"],
		:midsole => ["White", "Black", "Ghost Gray"],
		:midsole_logo => ["Black", "Ghost Gray", "White"],
		:eva_midsole => ["White", "Ghost Gray", "Charcoal"],
		:wrap => ["Elemental", "After Burn", "Black"],
		:pattern_wrap => ["Aluminum","Black","Graphite","White","After Burn","Brick Red","Charcoal","Flushed Pink","Ghost Gray","Mink Gray","Pixel Purple","Techno Teal","Venetian Blue","Downtown Green","Elemental","Jet Gray","Mod Gray","Pitch Gray","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Orange Glitch","Steel","Academy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink"],
		:reinforcement => ["Black", "White"],
		:outsole => ["White", "Black", "Ghost Gray", "Gum Rubber"],
		:background => ["Black", "White", "Ghost Gray"]
	}

	$yard = {
		:quarter_panel_logo => ["Black", "Red", "White", "Forest Green", "Team Orange", "Steeltown Gold", "Steel", "Team Royal", "Cardinal", "Midnight Navy"], 
		:heel_logo => ["Black", "Red", "White", "Forest Green", "Team Orange", "Steeltown Gold", "Steel", "Team Royal", "Cardinal", "Midnight Navy"], 
		:tongue_mesh => ["Black", "Red", "White", "Forest Green", "Team Orange", "Steeltown Gold", "Steel", "Team Royal", "Cardinal", "Midnight Navy"], 
		:hue_feng_collar => ["White", "Steel", "Cardinal", "Team Royal", "Black", "Red", "Forest Green", "Team Orange", "Steeltown Gold", "Midnight Navy"], 
		:lace_webbing => ["White", "Steel", "Black", "Red", "Team Royal", "Steeltown Gold", "Team Orange", "Forest Green", "Cardinal", "Midnight Navy"], 
		:quarter_collar_lining => ["Black", "Red", "White", "Forest Green", "Team Orange", "Steeltown Gold", "Steel", "Team Royal", "Cardinal", "Midnight Navy"], 
		:tongue_logo => ["Black", "White", "Red", "Midnight Navy", "Team Royal"], 
		:cosmo_sandwich_mesh => ["Black", "Red", "White", "Forest Green", "Team Orange", "Steeltown Gold", "Steel", "Team Royal", "Cardinal", "Midnight Navy"], 
		:clarino_overlay => ["Team Royal", "Red", "White", "Cardinal", "Black", "Steel", "Forest Green", "Steeltown Gold", "Team Orange", "Midnight Navy"], 
		:laces => ["Black", "Red", "White", "Forest Green", "Team Orange", "Steeltown Gold", "Steel", "Team Royal", "Cardinal", "Midnight Navy"], 
		:midsole => ["Black", "White"], 
		:cleat_plate => ["Black", "White", "Red", "Midnight Navy"], 
		:outsole_logo => ["White", "Black", "Red", "Midnight Navy", "Team Royal"], 
		:outsole_shank => ["White", "Steel"], 
		:tongue_patch => ["Black", "Red", "White", "Forest Green", "Team Orange", "Steeltown Gold", "Steel", "Team Royal", "Cardinal", "Midnight Navy"], 
		:quarter_panel_overlay_patterns => ["Black","Elemental","Jet Gray","Mod Gray","Pitch Gray","White","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Steel","Midnight Navy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink"],
		:quarter_panel_overlay_solid => ["Yard Back Panel Artwork Template","Black","Elemental","Jet Gray","Mod Gray","Pitch Gray","White","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Steel","Midnight Navy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink"],
		:quarter_panel_overlay_artwork => ["Black","Elemental","Jet Gray","Mod Gray","Pitch Gray","White","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Steel","Midnight Navy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink"],
		:quarter_panel_overlay_artwork_style => ["Full Takeover", "Mirror", "Tile", "Tile Mirror", "Medial Color"],
		:left_tongue_text => ["Black","Red","White","Forest Green","Team Orange","Steeltown Gold","Steel","Team Royal","Cardinal","Midnight Navy"],
		:right_tongue_text => ["Black","Red","White","Forest Green","Team Orange","Steeltown Gold","Steel","Team Royal","Cardinal","Midnight Navy"]
	}



	$curry6 = {
		:sockliner_overlay_pattern => ["Royal","Black","Elemental","Jet Gray","Mod Gray","Pitch Gray","White","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Orange Glitch","Academy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Texas Orange","Tropic Pink","Vegas Gold"],
		:sockliner_overlay_solid => ["Curry 6 Sockliner Artwork Template","Royal","Black","Elemental","Jet Gray","Mod Gray","Pitch Gray","White","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Orange Glitch","Academy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Texas Orange","Tropic Pink","Vegas Gold"],
		:sockliner_overlay_artwork => ["Royal","Black","Elemental","Jet Gray","Mod Gray","Pitch Gray","White","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Orange Glitch","Academy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Texas Orange","Tropic Pink","Vegas Gold"],
		:knit => ["White", "Black", "Royal", "Red", "Academy", "Orange Glitch", "Mod Gray"],
		:heel_knit_dots => ["Elemental", "Black", "White","Academy", "High Vis Yellow", "Pitch Gray"],
		:heel_tape => ["Aqua Foam", "Guardian Green", "Mod Gray","Academy", "White", "Red", "Royal", "High Vis Yellow", "Orange Glitch"],
		:tongue_logo => ["Black", "Royal", "White", "Taxi", "Red", "Academy", "Orange Glitch", "Aqua Foam", "High Vis Yellow", "Mod Gray"],
		:heel_counter => ["Black", "Royal", "White", "Taxi", "Red", "Academy", "Orange Glitch", "Aqua Foam", "High Vis Yellow", "Mod Gray"],
		:heel_counter_stitching => ["Black", "Royal", "White", "Taxi", "Red", "Academy", "Orange Glitch", "Aqua Foam", "High Vis Yellow", "Mod Gray"],
		:hftpu => ["Black", "Royal", "White", "Taxi", "Red", "Academy", "Orange Glitch", "Aqua Foam", "High Vis Yellow", "Mod Gray"],
		:toe_hotmelt => ["White", "Black", "Taxi", "Red", "Academy", "Mod Gray", "High Vis Yellow", "Orange Glitch", "Aqua Foam", "Royal"],
		:outsole_toe_backpaint => ["Taxi", "Red", "White", "Black", "High Vis Yellow", "Academy", "Mod Gray", "Aqua Foam", "Orange Glitch"],
		:laces => ["Black", "Royal", "White", "Taxi", "Red", "Academy", "Orange Glitch", "Aqua Foam", "High Vis Yellow", "Mod Gray"],
		:lace_tip_logo => ["Mod Gray","White", "Black"],
		:cables => ["Black", "Royal", "White", "Taxi", "Red", "Academy", "Orange Glitch", "Aqua Foam", "High Vis Yellow", "Mod Gray"],
		:eva_foam => ["Black", "Royal", "White", "Taxi", "Red", "Academy", "Orange Glitch", "Aqua Foam", "High Vis Yellow", "Mod Gray"],
		:color_fade => ["Elemental","Black", "Royal", "White", "Taxi", "Red", "Academy", "Orange Glitch", "Aqua Foam", "High Vis Yellow", "Mod Gray"],
		:energy_web => ["Elemental","Black", "Royal", "Taxi", "Red", "Academy", "Orange Glitch", "Aqua Foam", "High Vis Yellow", "Mod Gray"],
		:external_shank => ["Clear", "Tinted Black", "Tinted Hi Vis Yellow", "Tinted Royal"],
		:tpu_plate => ["Clear", "Tinted Black", "Tinted Royal", "Tinted Hi Vis Yellow"],
		:sc_logo => ["Elemental","White", "Black", "Royal", "Taxi", "Red", "Academy", "Orange Glitch", "Aqua Foam", "High Vis Yellow", "Mod Gray"],
		:outsole => ["Black", "Royal", "Red","Mod Gray","Academy", "Aqua Foam", "Elemental", "Orange Glitch", "Taxi", "White"],
		:left_sockliner_text => ["Elemental","White", "Black", "Gray Flux", "Pitch Gray", "Mod Gray", "Jet Gray", "Khaki Base", "Taxi", "Steeltown Gold", "Team Orange", "Red", "Cardinal", "Maroon", "Tropic Pink", "Purple", "Carolina Blue", "St. Tropez", "Royal", "Academy", "Team Kelly Green", "Forest Green", "Texas Orange", "Cleveland Brown", "Silt Brown", "Vegas Gold", "Dust", "Aqua Foam", "Lima Bean", "Canyon Green", "Orange Glitch", "Coho"],
		:right_sockliner_text => ["Elemental","White", "Black", "Gray Flux", "Pitch Gray", "Mod Gray", "Jet Gray", "Khaki Base", "Taxi", "Steeltown Gold", "Team Orange", "Red", "Cardinal", "Maroon", "Tropic Pink", "Purple", "Carolina Blue", "St. Tropez", "Royal", "Academy", "Team Kelly Green", "Forest Green", "Texas Orange", "Cleveland Brown", "Silt Brown", "Vegas Gold", "Dust", "Aqua Foam", "Lima Bean", "Canyon Green", "Orange Glitch", "Coho"]
	}

	$kicksprint2 = {
		:upper_overlay_pattern => ["Black","Elemental","Jet Gray","Pitch Gray","White","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Steel","Midnight Navy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink","Vegas Gold"],
		:upper_overlay_solid => ["Kicksprint 2 Upper Artwork Templates","Black","Elemental","Jet Gray","Pitch Gray","White","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Steel","Midnight Navy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink","Vegas Gold"],
		:upper_overlay_artwork_style => ["Full Takeover", "Mirror", "Tile", "Tile Mirror"],
		:upper_overlay_artwork => ["Black","Elemental","Jet Gray","Pitch Gray","White","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Steel","Midnight Navy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink","Vegas Gold"],
		:toe_logo => ["Black", "Cardinal", "Forest Green", "Midnight Navy", "Red", "Steel", "Steeltown Gold", "Team Orange", "Team Royal", "White"],
		:tongue_logo => ["Black", "Cardinal", "Forest Green", "Midnight Navy", "Red", "Steel", "Steeltown Gold", "Team Orange", "Team Royal", "White"],
		:heel_logo => ["Black", "Cardinal", "Forest Green", "Midnight Navy", "Red", "Steel", "Steeltown Gold", "Team Orange", "Team Royal", "White"],
		:tongue => ["Black", "Midnight Navy", "Red", "Steel", "Steeltown Gold", "Team Royal", "White"],
		:tongue_lining => ["Black", "Midnight Navy", "Red", "Steel", "Steeltown Gold", "Team Royal", "White"],
		:bottom_tongue_tape => ["Black", "Midnight Navy", "Red", "Steel", "Steeltown Gold", "Team Royal", "White"],
		:collar_lining => ["Black", "Midnight Navy", "Red", "Steel", "Steeltown Gold", "Team Royal"],
		:internal_lining => ["Black", "Midnight Navy", "Red", "Steel", "Steeltown Gold", "Team Royal"],
		:heel_counter_lining => ["Black", "Midnight Navy", "Red", "Steel", "Steeltown Gold", "Team Royal"],
		:heel_counter => ["Black", "Cardinal", "Forest Green", "Midnight Navy", "Red", "Steel", "Steeltown Gold", "Team Orange", "Team Royal", "White"],
		:bemis_tape => ["Black", "Midnight Navy", "Red", "Steel", "Steeltown Gold", "Team Royal"],
		:laces => ["Black", "Cardinal", "Forest Green", "Midnight Navy", "Red", "Steel", "Steeltown Gold", "Team Orange", "Team Royal", "White"],
		:outsole_toe => ["Metallic Chrome", "Solar Chrome", "Metallic Gold Chrome"],
		:outsole_heel => ["Black", "White"],
		:left_vamp_text => ["Academy", "Aluminum", "Charcoal", "Graphite", "Steel","White", "Black", "Cardinal", "Maroon", "Purple", "Red", "Tropic Pink", "Dark Orange", "Team Orange", "Texas Orange", "Steeltown Gold", "Taxi", "Team Kelly Green", "Forest Green", "Carolina Blue", "St. Tropez", "Team Royal","Cleveland Brown", "Aqua Foam", "Canyon Green", "Coho", "Dust", "Lima Bean", "Orange Glitch", "Gray Flux", "Khaki Base", "Silt Brown"],
		:right_vamp_text => ["Academy", "Aluminum", "Charcoal", "Graphite", "Steel","White", "Black", "Cardinal", "Maroon", "Purple", "Red", "Tropic Pink", "Dark Orange", "Team Orange", "Texas Orange", "Steeltown Gold", "Taxi", "Team Kelly Green", "Forest Green", "Carolina Blue", "St. Tropez", "Team Royal","Cleveland Brown", "Aqua Foam", "Canyon Green", "Coho", "Dust", "Lima Bean", "Orange Glitch", "Gray Flux", "Khaki Base", "Silt Brown"]
	}

	$hovr_phantom = {
		:pattern => ["Dark Orange","White","Elemental","Mod Gray","Pitch Gray","Jet Gray","Black","Taxi","Steeltown Gold","Team Orange","Red","Cardinal","Maroon","Tropic Pink","Purple","Carolina Blue","St. Tropez","Team Royal","Academy","Team Kelly Green","Forest Green","Vegas Gold","Texas Orange","Cleveland Brown","Dust","Aqua Foam","Lima Bean","Canyon Green","Coho","Orange Glitch","Gray Flux","Khaki Base","Silt Brown"],
		:solid => ["Dark Orange","White","Elemental","Mod Gray","Pitch Gray","Jet Gray","Black","Taxi","Steeltown Gold","Team Orange","Red","Cardinal","Maroon","Tropic Pink","Purple","Carolina Blue","St. Tropez","Team Royal","Academy","Team Kelly Green","Forest Green","Vegas Gold","Texas Orange","Cleveland Brown","Dust","Aqua Foam","Lima Bean","Canyon Green","Coho","Orange Glitch","Gray Flux","Khaki Base","Silt Brown"],
		:artwork_style =>["Full Takeover", "Mirror", "Tile", "Tile Mirror"], 
		:artwork => ["Dark Orange","White","Elemental","Mod Gray","Pitch Gray","Jet Gray","Black","Taxi","Steeltown Gold","Team Orange","Red","Cardinal","Maroon","Tropic Pink","Purple","Carolina Blue","St. Tropez","Team Royal","Academy","Team Kelly Green","Forest Green","Vegas Gold","Texas Orange","Cleveland Brown","Dust","Aqua Foam","Lima Bean","Canyon Green","Coho","Orange Glitch","Gray Flux","Khaki Base","Silt Brown"],
		:collar =>["White", "Black", "Mod Gray", "Red"], 
		:lining =>["White", "Red", "Mod Gray", "Black"], 
		:stitching =>["Black","Academy","Red","White","Mod Gray","Orange Glitch","Coho","Dust","Aqua Foam","Canyon Green"],
		:heel =>["Black","Academy","Red","White","Mod Gray","Orange Glitch","Coho","Dust","Aqua Foam","Canyon Green"],
		:laces =>["Black","Academy","Red","White","Mod Gray","Orange Glitch","Coho","Dust","Aqua Foam","Canyon Green"],
		:webbing =>["Black","Academy","Red","White","Mod Gray","Orange Glitch","Coho","Dust","Aqua Foam","Canyon Green"],
		:tongue =>["Black","Academy","Red","White","Mod Gray","Orange Glitch","Coho","Dust","Aqua Foam","Canyon Green"],
		:midsole =>["White", "Black", "Mod Gray", "Red"], 
		:wrap =>["Mod Gray", "Pitch Gray", "Black"], 
		:foam =>["Red", "White", "Black"], 
		:outsole_logo_background =>["White", "Mod Gray", "Red", "Black"], 
		:outsole =>["White", "Black", "Mod Gray", "Red","Ice Blue - Translucent"], 
		:outsole_logo =>["Metallic Silver"],
		:tongue_tab =>["Black", "Academy", "Red", "White", "Mod Gray", "Orange Glitch", "Coho", "Dust", "Aqua Foam", "Canyon Green"]
	}

	$highlightmc = {
		:solid =>["Black","Elemental","Jet Gray","Pitch Gray","White","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Steel","Midnight Navy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink", "Highlight Side Panel Artwork Template"], 
		:tongue =>["Black", "Red", "White", "Forest Green", "Team Orange", "Steeltown Gold", "Team Royal", "Cardinal", "Steel", "Midnight Navy"], 
		:laces =>["Black", "Red", "White", "Forest Green", "Team Orange", "Steeltown Gold", "Team Royal", "Cardinal", "Steel", "Midnight Navy"], 
		:accent_line =>["Black", "Red", "White", "Forest Green", "Team Orange", "Steeltown Gold", "Team Royal", "Cardinal", "High Vis Yellow", "Tropic Pink", "Steel", "Midnight Navy"], 
		:logo =>["Black", "Red", "White", "Forest Green", "Team Orange", "Steeltown Gold", "Team Royal", "Cardinal", "Steel", "Midnight Navy", "Tropic Pink"], 
		:logo_outline =>["Black", "Red", "White", "Team Orange", "Steeltown Gold", "Team Royal", "Cardinal", "Elemental", "Tropic Pink", "Steel", "Midnight Navy", "Pitch Gray"], 
		:heel =>["Black", "Academy", "Red", "White", "Forest Green", "Team Orange", "Steeltown Gold", "Mod Gray", "Team Royal", "Cardinal", "Steel", "Midnight Navy"], 
		:heel_wordmark =>["Black", "Academy", "Red", "White", "Forest Green", "Team Orange", "Steeltown Gold", "Mod Gray", "Team Royal", "Cardinal", "Steel", "Midnight Navy"], 
		:cleat_plate =>["Black", "Red", "White", "Forest Green", "Team Orange", "Steeltown Gold", "Team Royal", "Cardinal", "High Vis Yellow", "Metallic Silver", "Metallic Gold", "Midnight Navy"], 
		:cleat_plate_tips =>["Black", "White"], 
		:cleat_plate_logo =>["White", "Black"], 
		:outsole =>["Black", "White"]
	}

	$icon_patterns = {
		:xmas_camo =>["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:digi_camo =>["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:tiger_camo =>["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:rig =>["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:breaker =>["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:noise_acc =>["uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current"], 
		:floral_camo =>["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:rgb =>[], 
		:fracture =>["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:jagger_multi =>["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:knockout =>["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:fleet =>["uaf-prs-highlightmc2019-mens-current","uaf-prs-kicksprint2-mens-current", "uaf-prs-yard-mens-current","uaf-prs-highlight-mens-current","uaf-prs-spotlight-mens-current"], 
		:alley_oop =>["uaf-prs-highlightmc2019-mens-current","uaf-prs-kicksprint2-mens-current", "uaf-prs-yard-mens-current","uaf-prs-highlight-mens-current","uaf-prs-spotlight-mens-current"], 
		:geo_stripe =>["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:geo_cache =>["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:future_skeleton =>["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:multisplatter =>["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:ribbon =>["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:abbey_two =>["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:abbey_one =>[], 
		:abbey_three =>[], 
		:block_gradient =>["uaf-prs-highlightmc2019-mens-current","uaf-prs-kicksprint2-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-highlight-mens-current", "uaf-prs-spotlight-mens-current"]
	}

	$icon_prints = {
		:carefree => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current","uaf-prs-spotlightdevtest-current"], 
		:cutout_taupe_gray => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:cutout_black_turquoise => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:cutout_black_orange => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:flux_multi_red_black => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:flux_multi_white_gray => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:flux_multi_white_turquoise => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:grit_black_gray => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:grit_turquoise => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:grit_white_gray => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:curry5_launch => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current"], 
		:scope_1 => ["uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current"], 
		:scope_2 => ["uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current"], 
		:scope_3 => ["uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current"], 
		:the_process_white_red => ["uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current"], 
		:the_process_white_blue => ["uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current"], 
		:the_process_black_yellow => ["uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current"], 
		:kenesha_sneed_escape => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:kenesha_sneed_garden => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:tommi_lim_cloudburst => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:shelby_and_sandy_clouds => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:shelby_and_sandy_skull_black => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:jen_mussari_marble => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:vivid_stroke_dust => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-kicksprint2-mens-current", "uaf-prs-yard-mens-current","uaf-prs-highlight-mens-current","uaf-prs-spotlight-mens-current"], 
		:vivid_stroke_tetra_gray => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-kicksprint2-mens-current", "uaf-prs-yard-mens-current","uaf-prs-highlight-mens-current","uaf-prs-spotlight-mens-current"], 
		:vivid_stroke_aqua_foam => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-kicksprint2-mens-current", "uaf-prs-yard-mens-current","uaf-prs-highlight-mens-current","uaf-prs-spotlight-mens-current"], 
		:vivid_stroke_black => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-kicksprint2-mens-current", "uaf-prs-yard-mens-current","uaf-prs-highlight-mens-current","uaf-prs-spotlight-mens-current"], 
		:p2nation_black_red => ["uaf-prs-spotlight-mens-current","uaf-prs-spotlightdevtest-current"], 
		:p2nation_white_red => ["uaf-prs-spotlight-mens-current","uaf-prs-spotlightdevtest-current"], 
		:plaid_1 => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-spotlight-mens-current"], 
		:plaid_2 => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-spotlight-mens-current"], 
		:plaid_3 => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-spotlight-mens-current"], 
		:plaid_4 => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-spotlight-mens-current"], 
		:supernova_c => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-spotlight-mens-current"], 
		:supernova_b => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-spotlight-mens-current"], 
		:supernova_a => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-spotlight-mens-current"], 
		:static_b => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-spotlight-mens-current"], 
		:static_a => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-spotlight-mens-current"], 
		:sprocket_a => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-spotlight-mens-current"], 
		:sprocket_d => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-spotlight-mens-current"], 
		:sprocket_b => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-spotlight-mens-current"], 
		:sprocket_c => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-spotlight-mens-current"], 
		:patriotic => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-spotlightdevtest-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current"], 
		:country_pride_flag_crop => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current","uaf-prs-spotlightdevtest-current"], 
		:country_pride_leaf_pattern => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current","uaf-prs-spotlightdevtest-current"], 
		:leaf_camo => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current","uaf-prs-spotlightdevtest-current"], 
		:galaxy => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current","uaf-prs-spotlightdevtest-current"], 
		:lightning => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current","uaf-prs-spotlightdevtest-current"], 
		:country_pride_borealis => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current","uaf-prs-spotlightdevtest-current"], 
		:flames => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current","uaf-prs-spotlightdevtest-current"], 
		:floral => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current", "uaf-prs-curry6-youth-current", "uaf-prs-highlight-mens-current", "uaf-prs-hovrhavoclow-mens-current", "uaf-prs-hovrhavoc-mens-current", "uaf-prs-hovrhavoc-womens-current", "uaf-prs-hovrphantom-womens-current", "uaf-prs-hovrslk-mens-current", "uaf-prs-hovrslk-womens-current", "uaf-prs-icon-sackpack-current", "uaf-prs-ignite-mens-current", "uaf-prs-ignite-womens-current", "uaf-prs-kicksprint2-mens-current", "uaf-prs-spotlight-mens-current", "uaf-prs-yard-mens-current", "uaf-prs-hovrphantom-mens-current","uaf-prs-spotlightdevtest-current"],
		:jungle_one => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current","uaf-prs-curry6-youth-current","uaf-prs-highlight-mens-current","uaf-prs-hovrhavoclow-mens-current","uaf-prs-hovrhavoc-mens-current","uaf-prs-hovrhavoc-womens-current","uaf-prs-hovrphantom-womens-current","uaf-prs-hovrslk-mens-current","uaf-prs-hovrslk-womens-current","uaf-prs-icon-sackpack-current","uaf-prs-ignite-mens-current","uaf-prs-ignite-womens-current","uaf-prs-kicksprint2-mens-current","uaf-prs-spotlight-mens-current","uaf-prs-yard-mens-current","uaf-prs-hovrphantom-mens-current"],
		:jungle_two => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current","uaf-prs-curry6-youth-current","uaf-prs-highlight-mens-current","uaf-prs-hovrhavoclow-mens-current","uaf-prs-hovrhavoc-mens-current","uaf-prs-hovrhavoc-womens-current","uaf-prs-hovrphantom-womens-current","uaf-prs-hovrslk-mens-current","uaf-prs-hovrslk-womens-current","uaf-prs-icon-sackpack-current","uaf-prs-ignite-mens-current","uaf-prs-ignite-womens-current","uaf-prs-kicksprint2-mens-current","uaf-prs-spotlight-mens-current","uaf-prs-yard-mens-current","uaf-prs-hovrphantom-mens-current"],
		:jungle_three => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current","uaf-prs-curry6-youth-current","uaf-prs-highlight-mens-current","uaf-prs-hovrhavoclow-mens-current","uaf-prs-hovrhavoc-mens-current","uaf-prs-hovrhavoc-womens-current","uaf-prs-hovrphantom-womens-current","uaf-prs-hovrslk-mens-current","uaf-prs-hovrslk-womens-current","uaf-prs-icon-sackpack-current","uaf-prs-ignite-mens-current","uaf-prs-ignite-womens-current","uaf-prs-kicksprint2-mens-current","uaf-prs-spotlight-mens-current","uaf-prs-yard-mens-current","uaf-prs-hovrphantom-mens-current"],
		:jungle_four => ["uaf-prs-highlightmc2019-mens-current","uaf-prs-curry6-mens-current","uaf-prs-curry6-iwd-current","uaf-prs-curry6-youth-current","uaf-prs-highlight-mens-current","uaf-prs-hovrhavoclow-mens-current","uaf-prs-hovrhavoc-mens-current","uaf-prs-hovrhavoc-womens-current","uaf-prs-hovrphantom-womens-current","uaf-prs-hovrslk-mens-current","uaf-prs-hovrslk-womens-current","uaf-prs-icon-sackpack-current","uaf-prs-ignite-mens-current","uaf-prs-ignite-womens-current","uaf-prs-kicksprint2-mens-current","uaf-prs-spotlight-mens-current","uaf-prs-yard-mens-current","uaf-prs-hovrphantom-mens-current"]
	}


################### DEPRECATED STYLES #######################
	

		# $railfit = {
	# 	:upper => ["Black/Elemental","Forest Green/Forest Green","Cardinal/Cardinal","Midnight Navy/Midnight Navy","Black/Black","White/White","Elemental/Elemental","White/Elemental"],
	# 	:collar => ["White","Black","Elemental","Midnight Navy","Gold","Cardinal","Forest Green"],
	# 	:tongue => ["White","Black","Elemental","Midnight Navy","Gold","Cardinal","Forest Green"],
	# 	:laces => ["White","Black","Elemental","Midnight Navy","Gold","Cardinal","Forest Green"],
	# 	:guide => ["White","Black","Elemental","Midnight Navy","Gold","Cardinal","Forest Green"],
	# 	:panel => ["White","Black","Aluminum","Steel","Graphite","Taxi","Steeltown Gold","Team Orange","Cardinal","Maroon","Tropic Pink","Purple","Carolina Blue","St. Tropez","Midnight Navy","Team Kelly Green","Forest Green","Texas Orange","Cleveland Brown","Dark Orange","Red","Team Royal","Railfit Side Panel Artwork Template", "Brick Red", "Charcoal", "Flushed Pink", "Ghost Gray", "Mink Gray", "Pixel Purple", "Techno Teal", "Venetian Blue"],
	# 	:rails => ["White","Black","Elemental","Midnight Navy","Gold","Cardinal","Forest Green"],
	# 	:midsole => ["White","Black"],
	# 	:heel => ["White","Black","Elemental","Midnight Navy","Gold","Cardinal","Forest Green"], 
	# 	:outsole => ["White","Black","Gum Rubber"],
	# 	:sockliner => ["White","Black"],
	# 	:left_text => ["Downtown Green","White", "Black", "Aluminum", "Steel", "Graphite", "Taxi", "Steeltown Gold", "Team Orange", "Red", "Cardinal", "Maroon", "Tropic Pink", "Purple", "Carolina Blue", "St. Tropez", "Team Royal", "Midnight Navy", "Team Kelly Green", "Forest Green", "Texas Orange", "Cleveland Brown", "Dark Orange",  "Desert Sky", "Tokyo Lemon", "Merit Purple", "Vermillion", "Jupiter Blue", "Tin", "Baja", "Elemental"],
	# 	:right_text => ["Downtown Green","White", "Black", "Aluminum", "Steel", "Graphite", "Taxi", "Steeltown Gold", "Team Orange", "Red", "Cardinal", "Maroon", "Tropic Pink", "Purple", "Carolina Blue", "St. Tropez", "Team Royal", "Midnight Navy", "Team Kelly Green", "Forest Green", "Texas Orange", "Cleveland Brown", "Dark Orange",  "Desert Sky", "Tokyo Lemon", "Merit Purple", "Vermillion", "Jupiter Blue", "Tin", "Baja", "Elemental"]
	# }

		# $charged = {
	# 	:forefoot => ["Phoenix Fire","White","Black","Aluminum","Steel","Graphite","Maroon","Cardinal","Red","Tropic Pink","Dark Orange","Team Orange","Texas Orange","Steeltown Gold","Taxi","Team Kelly Green","Forest Green","Artillery Green","Carolina Blue","Blue Shift","St. Tropez","Team Royal","Midnight Navy","Purple","Cleveland Brown","Under Armour Charged 24/7 Forefoot Pattern Template","Brick Red", "Charcoal", "Flushed Pink", "Ghost Gray", "Mink Gray", "Pixel Purple", "Techno Teal", "Venetian Blue"],
	# 	:outsole => ["White","Black","Gum Rubber"],
	# 	:logo => ["White","Black"],
	# 	:base => ["White"],
	# 	:laces => ["White","Black","Aluminum","Red","Phoenix Fire","Blue Shift","Artillery Green","Midnight Navy"],
	# 	:speckling => ["No Treatment","Black","Aluminum","Red","Phoenix Fire","Blue Shift","Artillery Green","Midnight Navy"],
	# 	:heel => ["White","Black","Aluminum","Red","Artillery Green","Midnight Navy"],
	# 	:strap => ["White","Black","Aluminum","Red","Artillery Green","Midnight Navy"]
	# }

	# $curry_mid = {
	# 	:solid => ["Vegas Gold","White","Black","Aluminum","Steel","Graphite","Cardinal","Red","Tropic Pink","Team Orange","Texas Orange","Steeltown Gold","Taxi","Team Kelly Green","Forest Green","Neptune","Carolina Blue","St. Tropez","Team Royal","Midnight Navy","Purple","Upper - Artwork","Brick Red", "Charcoal", "Flushed Pink", "Ghost Gray", "Mink Gray", "Pixel Purple", "Techno Teal", "Venetian Blue"],
	# 	:pattern => ["White","Black","Aluminum","Steel","Graphite","Maroon","Cardinal","Red","Tropic Pink","Dark Orange","Team Orange","Phoenix Fire","Texas Orange","Steeltown Gold","Taxi","Team Kelly Green","Forest Green","Neptune","Carolina Blue","St. Tropez","Team Royal","Midnight Navy","Purple","Cleveland Brown","Brick Red", "Charcoal", "Flushed Pink", "Ghost Gray", "Mink Gray", "Pixel Purple", "Techno Teal", "Venetian Blue"],
	# 	:outsole => ["White","Black","Steel","Gum Rubber","Ice Blue - Translucent"],
	# 	:laces => ["White","Black","Steel","Cardinal","Red","Phoenix Fire","Team Orange","Taxi","Neptune","Carolina Blue","Team Royal","Midnight Navy","Purple"],
	# 	:toecap => ["White","Black","Steel","Cardinal","Red","Taxi","Carolina Blue","Team Royal","Midnight Navy"],
	# 	:shank => ["White","Black","Metallic Gold","Metallic Silver","Steel","Red","Phoenix Fire","Taxi","Neptune","Team Royal","Midnight Navy"],
	# 	:lining => ["Black","Gold","Aluminum","Steel","Cardinal","Red","Phoenix Fire","Taxi","Neptune","Carolina Blue","Team Royal","Midnight Navy"],
	# 	:upper_logo => ["White","Black","Metallic Gold","Metallic Silver","Steel","Cardinal","Red","Phoenix Fire","Team Orange","Taxi","Neptune","Carolina Blue","Team Royal","Midnight Navy","Purple"],
	# 	:heel => ["White","Black","Metallic Gold","Metallic Silver","Steel","Red","Phoenix Fire","Taxi","Neptune","Team Royal","Midnight Navy"],
	# 	:tongue => ["White","Black","Steel","Cardinal","Red","Taxi","Forest Green","Carolina Blue","Team Royal","Midnight Navy"],
	# 	:tongue_logo => ["White","Black","Metallic Gold","Metallic Silver","Steel","Cardinal","Red","Phoenix Fire","Team Orange","Taxi","Neptune","Carolina Blue","Team Royal","Midnight Navy","Purple"],
	# 	:topline => ["White","Black","Metallic Gold","Metallic Silver","Steel","Red","Phoenix Fire","Taxi","Neptune","Team Royal","Midnight Navy"],
	# 	:midsole => ["White","Black","Metallic Gold","Metallic Silver","Steel","Red","Phoenix Fire","Taxi","Neptune","Team Royal","Midnight Navy"],
	# 	:right_logo => ["Black", "Midnight Navy", "Red", "Metallic Silver", "Taxi", "Team Royal", "White", "Metallic Gold"],
	# 	:left_logo => ["Black", "Midnight Navy", "Red", "Metallic Silver", "Taxi", "Team Royal", "White", "Metallic Gold"]
	# }

	# $drive_low = {
	# 	:upper => ["White", "Black", "Aluminum", "Steel", "Red", "Taxi", "Forest Green", "Team Royal", "Midnight Navy"],
	# 	:pattern => ["White","Black","Elemental","Aluminum","Steel","Tin","Graphite","Maroon","Cardinal","Red","Tropic Pink","Vermillion","Dark Orange","Team Orange","Texas Orange","Steeltown Gold","Taxi","Tokyo Lemon","Team Kelly Green","Downtown Green","Forest Green","Desert Sky","Carolina Blue","St. Tropez","Powderkeg Blue","Jupiter Blue","Team Royal","Midnight Navy","Purple","Merit Purple","Cleveland Brown","Baja","Stone","Brick Red", "Charcoal", "Flushed Pink", "Ghost Gray", "Mink Gray", "Pixel Purple", "Techno Teal", "Venetian Blue"],
	# 	:solid => ["Midnight Navy","Team Royal","White","Black","Aluminum","Steel","Taxi","Red","Forest Green","UA Drive4 Upper Artwork Template"],
	# 	:tongue => ["White","Black","Aluminum","Steel","Red","Taxi","Forest Green","Team Royal","Midnight Navy"],
	# 	:trim => ["Black","Aluminum","Red","Team Royal","Midnight Navy"],
	# 	:eyelets => ["White","Black","Steel","Red","Tropic Pink","Taxi","High Vis Yellow","Forest Green","Team Royal","Midnight Navy"],
	# 	:laces => ["White","Black","Steel","Red","Tropic Pink","Taxi","High Vis Yellow","Forest Green","Team Royal","Midnight Navy"],
	# 	:colorblock => ["No Treatment","White","Black","Metallic Silver","Metallic Gold","Steel","Red","Tropic Pink","Taxi","High Vis Yellow","Forest Green","Team Royal","Midnight Navy"],
	# 	:logo => ["White","Black","Metallic Silver","Metallic Gold","Steel","Red","Tropic Pink","Taxi","High Vis Yellow","Forest Green","Team Royal","Midnight Navy"],
	# 	:outsole => ["White","Black","Ice Blue - Translucent","Gum"],
	# 	:midsole => ["White","Black","Stone"],
	# 	:heel => ["White","Black","Metallic Silver","Metallic Gold","Steel","Red","Taxi","Forest Green","Powderkeg Blue","Team Royal","Midnight Navy"],
	# 	:midfoot => ["White","Black","Metallic Silver","Metallic Gold","Steel","Red","Tropic Pink","Taxi","High Vis Yellow","Forest Green","Powderkeg Blue","Team Royal","Midnight Navy"],
	# 	:left_text => ["Downtown Green", "Desert Sky", "Tokyo Lemon", "Tin", "Merit Purple", "Vermillion", "Jupiter Blue", "Baja", "White", "Black", "Elemental", "Aluminum", "Steel", "Graphite", "Charcoal", "Stealth Gray", "Maroon", "Cardinal", "Red", "Tropic Pink", "True Pink", "Dark Orange", "Team Orange", "Steeltown Gold", "Taxi", "Quirky Lime", "Team Kelly Green", "Forest Green", "Artillery Green", "Fresco Green", "Marlin Blue", "Neptune", "Blue Infinity", "Carolina Blue", "Blue Shift", "St. Tropez", "Powderkeg Blue", "Team Royal", "Midnight Navy", "Purple", "Indulge", "Cleveland Brown", "Texas Orange", "Beige Canvas", "Vegas Gold", "Dune", "Stone", "Ivory"],
	# 	:right_text => ["Downtown Green", "Desert Sky", "Tokyo Lemon", "Tin", "Merit Purple", "Vermillion", "Jupiter Blue", "Baja", "White", "Black", "Elemental", "Aluminum", "Steel", "Graphite", "Charcoal", "Stealth Gray", "Maroon", "Cardinal", "Red", "Tropic Pink", "True Pink", "Dark Orange", "Team Orange", "Steeltown Gold", "Taxi", "Quirky Lime", "Team Kelly Green", "Forest Green", "Artillery Green", "Fresco Green", "Marlin Blue", "Neptune", "Blue Infinity", "Carolina Blue", "Blue Shift", "St. Tropez", "Powderkeg Blue", "Team Royal", "Midnight Navy", "Purple", "Indulge", "Cleveland Brown", "Texas Orange", "Beige Canvas", "Vegas Gold", "Dune", "Stone", "Ivory"]
	# }

	# $drive_four = {
	# 	:overlay => ["Red","White","Black","Aluminum","Steel","Graphite","Maroon","Cardinal","Tropic Pink","Dark Orange","Team Orange","Texas Orange","Steeltown Gold","Team Kelly Green","Carolina Blue","St. Tropez","Powderkeg Blue","Team Royal","Midnight Navy","Purple","Cleveland Brown","Forest Green","Stone","Taxi", "Under Armour Highlight Upper Artwork Template","Brick Red", "Charcoal", "Flushed Pink", "Ghost Gray", "Mink Gray", "Pixel Purple", "Techno Teal", "Venetian Blue"],
	# 	:tongue => ["Team Royal","Midnight Navy","Red"],
	# 	:heel => ["White","Black","Metallic Silver","Metallic Gold","Steel","Red","Taxi","Forest Green","Powderkeg Blue","Team Royal","Midnight Navy"],
	# 	:eyelets => ["White","Black","Aluminum","Steel","Red","Taxi","Forest Green","Powderkeg Blue","Team Royal","Midnight Navy"],
	# 	:midfoot => ["White","Black","Metallic Silver","Metallic Gold","Steel","Red","Tropic Pink","High Vis Yellow","Taxi","Forest Green","Powderkeg Blue","Team Royal","Midnight Navy"],
	# 	:colorblock => ["No Treatment","White","Black","Metallic Silver","Metallic Gold","Steel","Red","Tropic Pink","High Vis Yellow","Taxi","Forest Green","Team Royal","Midnight Navy"],
	# 	:trim => ["White","Black","Aluminum","Steel","Red","Taxi","Forest Green","Powderkeg Blue","Team Royal","Midnight Navy"],
	# 	:laces => ["White","Black","Steel","Red","Tropic Pink","Taxi","High Vis Yellow","Forest Green","Team Royal","Midnight Navy"],
	# 	:logo => ["White","Black","Metallic Silver","Metallic Gold","Steel","Red","Tropic Pink","High Vis Yellow","Taxi","Forest Green","Powderkeg Blue","Team Royal","Midnight Navy"],
	# 	:outsole => ["White","Black","Ice Blue - Translucent","Gum"],
	# 	:midsole => ["White","Black","Stone"],
	# 	:left_text => ["Downtown Green", "Desert Sky", "Tokyo Lemon", "Tin", "Merit Purple", "Vermillion", "Jupiter Blue", "Baja", "White", "Black", "Elemental", "Aluminum", "Steel", "Graphite", "Charcoal", "Stealth Gray", "Maroon", "Cardinal", "Red", "Tropic Pink", "True Pink", "Dark Orange", "Team Orange", "Steeltown Gold", "Taxi", "Quirky Lime", "Team Kelly Green", "Forest Green", "Artillery Green", "Fresco Green", "Marlin Blue", "Neptune", "Blue Infinity", "Carolina Blue", "Blue Shift", "St. Tropez", "Powderkeg Blue", "Team Royal", "Midnight Navy", "Purple", "Indulge", "Cleveland Brown", "Texas Orange", "Beige Canvas", "Vegas Gold", "Dune", "Stone", "Ivory"],
	# 	:right_text => ["Downtown Green", "Desert Sky", "Tokyo Lemon", "Tin", "Merit Purple", "Vermillion", "Jupiter Blue", "Baja", "White", "Black", "Elemental", "Aluminum", "Steel", "Graphite", "Charcoal", "Stealth Gray", "Maroon", "Cardinal", "Red", "Tropic Pink", "True Pink", "Dark Orange", "Team Orange", "Steeltown Gold", "Taxi", "Quirky Lime", "Team Kelly Green", "Forest Green", "Artillery Green", "Fresco Green", "Marlin Blue", "Neptune", "Blue Infinity", "Carolina Blue", "Blue Shift", "St. Tropez", "Powderkeg Blue", "Team Royal", "Midnight Navy", "Purple", "Indulge", "Cleveland Brown", "Texas Orange", "Beige Canvas", "Vegas Gold", "Dune", "Stone", "Ivory"]
	# }

	# $curry_low = {
	# 	:solid => ["White","Black","Aluminum","Steel","Graphite","Cardinal","Red","Tropic Pink","Team Orange","Texas Orange","Steeltown Gold","Taxi","Team Kelly Green","Forest Green","Neptune","Carolina Blue","St. Tropez","Team Royal","Midnight Navy","Purple","Upper - Artwork","Brick Red", "Charcoal", "Flushed Pink", "Ghost Gray", "Mink Gray", "Pixel Purple", "Techno Teal", "Venetian Blue","Vegas Gold"],
	# 	:pattern => ["White","Black","Aluminum","Steel","Graphite","Maroon","Cardinal","Red","Tropic Pink","Dark Orange","Team Orange","Phoenix Fire","Texas Orange","Steeltown Gold","Taxi","Team Kelly Green","Forest Green","Neptune","Carolina Blue","St. Tropez","Team Royal","Midnight Navy","Purple","Cleveland Brown","Brick Red", "Charcoal", "Flushed Pink", "Ghost Gray", "Mink Gray", "Pixel Purple", "Techno Teal", "Venetian Blue"],
	# 	:outsole => ["White","Black","Gum Rubber", "Ice Blue - Translucent", "Steel"],
	# 	:outsole_logo => ["Black", "Metallic Gold", "Metallic Silver", "Midnight Navy", "Red", "Taxi", "Team Royal", "White"],
	# 	:toecap => ["White","Black","Steel","Cardinal","Red","Taxi","Carolina Blue","Team Royal","Midnight Navy"],
	# 	:tongue => ["Black", "Cardinal", "Carolina Blue", "Forest Green", "Midnight Navy", "Red", "Steel", "Taxi", "Team Royal", "White"],
	# 	:tongue_logo => ["Cardinal", "Carolina Blue", "Neptune", "Phoenix Fire", "Purple", "Steel", "Team Orange","Black", "Metallic Gold", "Metallic Silver", "Midnight Navy", "Red", "Taxi", "Team Royal", "White"],
	# 	:loop => ["Black", "Metallic Gold", "Metallic Silver", "Midnight Navy", "Red", "Taxi", "Team Royal", "White"],
	# 	:icon_logo => ["Black", "Metallic Gold", "Metallic Silver", "Midnight Navy", "Red", "Taxi", "Team Royal", "White"],
	# 	:lining => ["Black","Gold","Aluminum","Steel","Cardinal","Red","Phoenix Fire","Taxi","Neptune","Carolina Blue","Team Royal","Midnight Navy"],
	# 	:laces => ["White","Black","Steel","Red","Team Orange","Taxi","Team Royal","Midnight Navy","Purple"],
	# 	:topline => ["White","Black","Metallic Silver","Metallic Gold","Steel","Red","Phoenix Fire","Taxi","Neptune","Team Royal","Midnight Navy"],
	# 	:midsole => ["White","Black","Metallic Silver","Metallic Gold","Steel","Red","Phoenix Fire","Taxi","Neptune","Team Royal","Midnight Navy"],
	# 	:shank => ["White","Black","Metallic Silver","Metallic Gold","Steel","Red","Phoenix Fire","Taxi","Neptune","Team Royal","Midnight Navy"],
	# 	:heel => ["White","Black","Metallic Silver","Metallic Gold","Steel","Red","Phoenix Fire","Taxi","Neptune","Team Royal","Midnight Navy"]
	# }

		# $curry_five = {
	# 	:pattern => ["Black","Elemental","Jet Gray","Mod Gray","Pitch Gray","White","Gray Flux","Khaki Base","Silt Brown","Aqua Foam","Canyon Green","Coho","Dust","Lima Bean","Steel","Midnight Navy","Cardinal","Carolina Blue","Cleveland Brown","Dark Orange","Forest Green","Maroon","Purple","Red","St. Tropez","Steeltown Gold","Taxi","Team Kelly Green","Team Orange","Team Royal","Texas Orange","Tropic Pink","Vegas Gold"],
	# 	:solid => ["Academy","White", "Black", "Aluminum", "After Burn", "Vegas Gold", "Steel", "Graphite", "Taxi", "Steeltown Gold", "Team Orange", "Red", "Cardinal", "Maroon", "Tropic Pink", "Purple", "Carolina Blue", "St. Tropez", "Team Royal", "Team Kelly Green", "Forest Green", "Texas Orange", "Cleveland Brown", "Dark Orange",  "Venetian Blue", "Brick Red", "Flushed Pink", "Ghost Gray", "Mink Gray", "Pixel Purple", "Techno Teal", "Charcoal", "Upper - Overlay Artwork", "Purple Rave"],
	# 	:upper => ["White","Black","Team Royal","Taxi","Red"],
	# 	:laces => ["White","Black","Red","Taxi","Team Royal","Academy","Ghost Gray"],
	# 	:tongue => ["Academy","White","Black","Metallic Gold","Metallic Silver","Red","Taxi","Team Royal"],
	# 	:midsole => ["White","Black","Metallic Silver","Team Royal"],
	# 	:outsole => ["White","Black","Ghost Gray", "Team Royal", "Taxi"],
	# 	:logo => ["White","Black","Ice Blue - Translucent","Team Royal"],
	# 	:side => ["White","Black","Metallic Silver","Metallic Gold","Red","Taxi","Team Royal","Academy"],
	# 	:bottom => ["White","Black","Metallic Silver","Metallic Gold","Red","Taxi","Team Royal","Academy"],
	# 	:signature => ["White","Black","Metallic Silver"],
	# 	:tape => ["Black", "Red", "Taxi", "Team Royal", "White"],
	# 	:eyestay => ["Black", "Red", "Taxi", "Team Royal", "White"],
	# 	:lining => ["Black", "Red", "Taxi", "Team Royal", "White"]
	# }

		# $curry5le = {
	# 	:upper_knit => ["Elemental", "Baja", "Guardian Green", "Black", "White"],
	# 	:heel_tape => ["White", "Black", "Elemental", "Baja", "Guardian Green"],
	# 	:eyestay => ["White", "Black", "Guardian Green", "Elemental", "Baja"],
	# 	:counter_lining => ["White", "Black", "Elemental", "Guardian Green", "Baja"],
	# 	:laces => ["Elemental", "Baja", "Guardian Green", "Black"],
	# 	:tongue_label => ["White", "Pitch Gray", "Guardian Green", "Black", "Metallic Silver", "Metallic Gold", "Stone"],
	# 	:tongue_logo => ["Elemental", "Black", "City Khaki", "Trail Green", "Metallic Silver", "White", "Metallic Gold"],
	# 	:midsole => ["Baja", "Black", "White", "Trail Green", "Metallic Silver"],
	# 	:side_tpu => ["White", "Black", "Baja", "Trail Green", "Metallic Silver"],
	# 	:bottom_tpu => ["Static"],
	# 	:outsole => ["Palm Green", "Stone", "Elemental", "White", "Black"],
	# 	:outsole_logo => ["White", "Elemental", "Palm Green", "Stone"],
	# 	:side_tpu_sc_signature => ["White", "Metallic Silver", "Palm Green"],
	# 	:heel_stitching => ["White", "Black", "Elemental", "Guardian Green", "Baja"],
	# 	:lace_loops => ["Guardian Green", "Baja", "Elemental", "Black","White"],
	# 	:outsole_inner_map => ["noise"],
	# 	:outsole_transparency => ["Green Outsole Transparency", "Brown Outsole Transparency", "Black Outsole Transparency", "White Outsole Transparancy", "White Outsole Transparency (no colorway)", "Black Outsole Transparency (no colorway)"],
	# 	:upper_mesh => ["Trail Green", "Baja", "White", "Black"],
	# 	:center_stripe => ["Trail Green", "Baja", "Elemental", "Black"],
	# 	:upper_mesh_pattern => ["Trail Green/Guardian Green/White", "Baja/Khaki Base/White", "White/Overcast Gray/Elemental", "Black/Pitch Gray/White"]	
	# }

		# $clutchfit = {
	# 	:upper => ["White","Black","Aluminum","Steel","Graphite","Maroon","Cardinal","Red","Tropic Pink","Dark Orange","Team Orange","Texas Orange","Steeltown Gold","Taxi","Team Kelly Green","Forest Green","Carolina Blue","St. Tropez","Team Royal","Midnight Navy","Purple","Cleveland Brown","Upper Artwork Template", "Brick Red", "Charcoal", "Flushed Pink", "Ghost Gray", "Mink Gray", "Pixel Purple", "Techno Teal", "Venetian Blue"],
	# 	:logo => ["White","Black","Steel","Cardinal","Red","Tropic Pink","Team Orange","Taxi","Team Kelly Green","Forest Green","Carolina Blue","Team Royal","Midnight Navy","Purple"],
	# 	:logo_outline => ["White", "Black", "Aluminum", "Steel", "Charcoal", "Cardinal", "Red", "Tropic Pink", "Team Orange", "Taxi", "Team Kelly Green", "Forest Green", "Carolina Blue", "Team Royal", "Midnight Navy", "Purple"],
	# 	:lining => ["White","Black"],
	# 	:tongue => ["White","Black","Red","Team Orange","Taxi","Team Royal"],
	# 	:laces => ["White","Black","Red","Team Orange","Taxi","Team Royal"],
	# 	:plate => ["White","Black","Steel"]
	# }


end