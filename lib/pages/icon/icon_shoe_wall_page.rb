require "./lib/pages/icon/icon_base_page"
require "./lib/pages/icon/icon_customizer_page"

class IconShoeWall < IconCustomizer

	divs(:grid_options, class: ["gs-grid-lite-tile-container"])
	div(:loader_overlay, class: ["ua-wall__loader__overlay"])
	button(:get_started, class: ["ua-wall__loader__overlay__button"])
	div(:out_of_stock, class: ["spectrum-overlay overlay--validation"])
	div(:oos_color, class: ["overlay__validation-error-text-value"])
	div(:ui_menu, class: ["ui--uppercontrols__splitcontrol ui--uppercontrols__splitcontrol--multimedia"])
	element(:notice, css: "body > main > section")
	div(:out_of_stock_style,	class: ["not-available-message product-message"])

	def collect_shoewall_links
		@urls = Array.new
		hrefs = $driver.links.collect(&:href)
		hrefs.each do |link|
			if link.include?("iconStartType=inspirationStart&iconStartFrom=gallery&size=0")
				@urls.push(link)
			end
		end
		@urls.reject! { |r| r.include?("BVRRContainer") }
		return @urls
	end

	def verify_front_end_galleries(handle)
		$driver.goto("https://www.underarmour.com/en-us/ua-icon/custom-gear-grid-gallery/#{handle}")
		if self.notice?
			puts "This Gallery '#{handle}' Has Been Deprecated"
		end
		collect_shoewall_links.each do |link|
			$driver.goto("#{link}")
			sleep 6
			id = link.scan(/\w+[A-Z0-9]/)
			if self.out_of_stock_style?
				puts "#{id[3]} | #{self.out_of_stock_style}"
			elsif self.ui_menu? == false
				# BasePage.print_js_errors
				puts "#{id[3]} | Failed to Load Asset"
				$driver.screenshot.save "#{$screenshotfolder}/#{id[3]}.png"
			end
			if self.out_of_stock?
				puts "#{id[3]} | Recipe Contains Out Of Stock Options"
				$driver.screenshot.save "#{$screenshotfolder}/#{id[3]}.png"
			end
		end
	end

	def verify_back_end_galleries(handle)
		@pair = Hash.new { |hash, key| hash[key] = [] }
		gallery = JSON.parse(RestClient.get("http://api.spectrumcustomizer.com/api/external/under-armour/icon/gallery/#{handle}"){|response, request, result| response })
		gallery['recipeSets'].each do |rs|
			@pair[handle] << rs['readableRecipeSetId']
			# @pair.store(rs['productNumber'],rs['readableRecipeSetId'])
		end
		puts @pair
		# @pair.each do |pid,id|
		# 	$driver.goto("https://www.underarmour.com/en-us/pid#{pid}?recipeSetId=#{id}")
		# 	sleep 6
		# 	if self.out_of_stock_style?
		# 		puts "#{id} | #{self.out_of_stock_style}"
		# 	elsif self.ui_menu? == false
		# 		# BasePage.print_js_errors
		# 		puts "#{id} | Failed to Load Asset"
		# 		$driver.screenshot.save "#{$screenshotfolder}/#{id}.png"
		# 	end
		# 	if self.out_of_stock?
		# 		puts "#{id} | Recipe Contains Out Of Stock Options"
		# 		$driver.screenshot.save "#{$screenshotfolder}/#{id}.png"
		# 	end
		# end
	end
end