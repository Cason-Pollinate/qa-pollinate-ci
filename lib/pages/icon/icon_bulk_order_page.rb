require './lib/pages/icon/icon_customizer_page'
class IconBulkReport < IconCustomizer

	text_area		:recipe_input, 				name: 			"RawRecipesInput"
	button			:validate_button, 			value: 			"Validate"
	text_field		:email, 					id: 			"EmailAddress"
	text_field		:first_name,				id: 			"Address_FirstName"
	text_field		:last_name,					id: 			"Address_LastName"
	text_field		:address,					id: 			"Address_Address1"
	text_field		:city,						id: 			"Address_City"
	text_field		:state,						id: 			"Address_State"
	text_field		:postal_code,				id: 			"Address_PostalCode"
	text_field		:country,					id: 			"Address_Country"
	button			:submit,				 	value: 			"Submit Order"
	p				:thank_you,				 	css: 			"body > div > p"

	def generate_bulk_product_report(recipesetids)
		case ENV['ENVIRONMENT'].to_sym
		when :test then $driver.goto("https://test.spectrumcustomizer.com/under-armour/icon/bulkorder")
		when :staging then $driver.goto("https://staging.spectrumcustomizer.com/under-armour/icon/bulkorder")
		when :prod then $driver.goto("https://api.spectrumcustomizer.com/under-armour/icon/bulkorder")
		end
		recipesetids_string = recipesetids.join("\n")
		self.recipe_input = recipesetids_string
		self.validate_button
		self.wait_until { self.email? }
		self.email = "cason.williams@pollinate.com"
		self.first_name = "Test"
		self.last_name = "Test"
		self.address = "123 Test Ave."
		self.city = "Portland"
		self.state = "Oregon"
		self.postal_code = "97205"
		self.country = "United States"
		self.submit
	end
end