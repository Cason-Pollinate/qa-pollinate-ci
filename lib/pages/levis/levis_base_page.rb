class LevisBasePage < BasePage

	button 		:front_chest, 		text: 	"Front Left Chest"
	button 		:back_panel, 		text: 	"Back Panel"
	button 		:waistband, 		text: 	"Back Waistband"
	button 		:bean_font,			title: 	"Bean"
	button 		:block_font,		title: 	"Block"
	button 		:script_font,		title: 	"Script"										 
	element 	:accordion,			css: 	"body > div > div > div.sidebar > div:nth-child(1) > section > div.menu-categories > section:nth-child(1)"
	element 	:placement, 		css: 	"body > div > div > div.sidebar > div:nth-child(1) > section > div.menu-categories > section:nth-child(2)"
	element 	:embroidery, 		css: 	"body > div > div > div.sidebar > div:nth-child(1) > section > div.menu-categories > section:nth-child(3)"
	text_field 	:line_one,			css: 	"body > div > div > div.sidebar > div:nth-child(1) > section > div.menu-categories > section:nth-child(3) > div > div > div:nth-child(1) > div > input[type='text']"
	div 		:line_one_count,	css: 	"body > div > div > div.sidebar > div:nth-child(1) > section > div.menu-categories > section:nth-child(3) > div > div > div:nth-child(1) > div > div"
	text_field 	:line_two,			css: 	"body > div > div > div.sidebar > div:nth-child(1) > section > div.menu-categories > section:nth-child(3) > div > div > div:nth-child(2) > div > input[type='text']"
	div 		:line_two_count,	css: 	"body > div > div > div.sidebar > div:nth-child(1) > section > div.menu-categories > section:nth-child(3) > div > div > div:nth-child(2) > div > div"
	text_field 	:line_three,		css: 	"body > div > div > div.sidebar > div:nth-child(1) > section > div.menu-categories > section:nth-child(3) > div > div > div:nth-child(3) > div > input[type='text']"
	div 		:line_three_count, 	css: 	"body > div > div > div.sidebar > div:nth-child(1) > section > div.menu-categories > section:nth-child(3) > div > div > div:nth-child(3) > div > div"
	text_field	:tshirt_text,		css:	"body > div > div > div.sidebar > div:nth-child(1) > section > div.menu-categories > section:nth-child(2) > div > div > div.text-feature.menu-feature > div > input[type='text']"
	div			:tshirt_text_count, css: 	"body > div > div > div.sidebar > div:nth-child(1) > section > div.menu-categories > section:nth-child(2) > div > div > div.text-feature.menu-feature > div > div"
	div 		:test_url, 			class: 	["text-feature menu-feature"]

	$levis_jackets = {
		"723340136" => "lev-pro-jacketm-trucker",
		"299450026" => "lev-pro-jacketw-trucker"
	}

	$levis_shirts = {
		"577880000" => "lev-pro-tshirtm-custom",
		"173690389" => "lev-pro-tshirtw-custom"
	}

	def diff_urls
		self.test_url_element.attribute_value('data-image-url')
	end


	def front_center_tshirt
		self.placement_element.click
		sleep 1
		self.tshirt_text = "XXXXXXXXXXXXXXXXXXXXX"
		sleep 1
	end

	def front_left_tshirt
		self.accordion_element.click
		sleep 1
		self.front_chest
		self.placement_element.click
		sleep 1
		self.tshirt_text = "XXXXXXXXXXXXXXXXXXXXX"
		sleep 1
	end


	def front_chest_area(font,handle)
		self.embroidery_element.click
		sleep 1
		case font
		when "Bean"
			self.bean_font
			self.line_one = "XXXXXXXXXXXXXXXXXXXXX"
			sleep 1
			text_line_one = self.line_one
			
			self.wait_until {self.diff_urls == "https://test.spectrumcustomizer.com/api/external/levi/pulse/render?productHandle=#{handle}&location=UL&font=#{font}&color=Classic Red&textLines=#{text_line_one}"}
			
		when "Block"
			self.block_font
			self.line_one = "XXXXXXXXXXXXXXXXXXXXX"
			sleep 1
			text_line_one = self.line_one
			
			self.wait_until {self.diff_urls == "https://test.spectrumcustomizer.com/api/external/levi/pulse/render?productHandle=#{handle}&location=UL&font=#{font}&color=Classic Red&textLines=#{text_line_one}"}
			
		when "Script"
			self.script_font
			self.line_one = "XXXXXXXXXXXXXXXXXXXXX"
			sleep 1
			text_line_one = self.line_one
			
			self.wait_until {self.diff_urls == "https://test.spectrumcustomizer.com/api/external/levi/pulse/render?productHandle=#{handle}&location=UL&font=#{font}&color=Classic Red&textLines=#{text_line_one}"}
			
		end
	end

	def back_panel_area(font,handle)
		self.embroidery_element.click
		sleep 1
		case font
		when "Bean"
			self.bean_font
			self.line_one = "XXXXXXXXXXXXXXXXXXXXX"
			sleep 1
			text_line_one = self.line_one
			self.line_two = "XXXXXXXXXXXXXXXXXXXXX"
			sleep 1
			text_line_two = self.line_two
			self.line_three = "XXXXXXXXXXXXXXXXXXXXX"
			sleep 1
			text_line_three = self.line_three
			self.wait_until {self.diff_urls == "https://test.spectrumcustomizer.com/api/external/levi/pulse/render?productHandle=#{handle}&location=CB&font=#{font}&color=Classic Red&textLines=#{text_line_one}&textLines=#{text_line_two}&textLines=#{text_line_three}"}
		when "Script"
			self.script_font
			self.line_one = "XXXXXXXXXXXXXXXXXXXXX"
			sleep 1
			text_line_one = self.line_one
			self.line_two = "XXXXXXXXXXXXXXXXXXXXX"
			sleep 1
			text_line_two = self.line_two
			self.line_three = "XXXXXXXXXXXXXXXXXXXXX"
			sleep 1
			text_line_three = self.line_three
			self.wait_until {self.diff_urls == "https://test.spectrumcustomizer.com/api/external/levi/pulse/render?productHandle=#{handle}&location=CB&font=#{font}&color=Classic Red&textLines=#{text_line_one}&textLines=#{text_line_two}&textLines=#{text_line_three}"}
		end
	end

	def waist_band_area(font,handle)
		self.embroidery_element.click
		sleep 1
		case font
		when "Bean"
			self.bean_font
			self.line_one = "XXXXXXXXXXXXXXXXXXXXX"
			sleep 1
			text_line_one = self.line_one
			self.wait_until {self.diff_urls == "https://test.spectrumcustomizer.com/api/external/levi/pulse/render?productHandle=#{handle}&location=BH&font=#{font}&color=Classic Red&textLines=#{text_line_one}"}
			
		when "Block"
			self.block_font
			self.line_one = "XXXXXXXXXXXXXXXXXXXXX"
			sleep 1
			text_line_one = self.line_one
			self.wait_until {self.diff_urls == "https://test.spectrumcustomizer.com/api/external/levi/pulse/render?productHandle=#{handle}&location=BH&font=#{font}&color=Classic Red&textLines=#{text_line_one}"}

		when "Script"
			self.script_font
			self.line_one = "XXXXXXXXXXXXXXXXXXXXX"
			sleep 1
			text_line_one = self.line_one
			self.wait_until {self.diff_urls == "https://test.spectrumcustomizer.com/api/external/levi/pulse/render?productHandle=#{handle}&location=BH&font=#{font}&color=Classic Red&textLines=#{text_line_one}"}
			
		end
	end
end