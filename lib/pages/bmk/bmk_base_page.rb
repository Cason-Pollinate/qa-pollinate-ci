class BMKBasePage < BasePage
	include PageObject

	divs(:catagory_groups, class: "desktopmenu-section__categorygroup__category")
	divs(:feature_groups, class: "desktopmenu-section__featuregroup__feature")
	divs(:steel_option, class: "c-fea-blade-steel__selection__image")
	divs(:color_option, class: "c-fea-color__swatch")
	buttons(:material_option, class: "c-fea-material-texture__selection__image")
	buttons(:clip_option, class: "c-fea-clip__swatch__toggle")
	div(:saved_recipe_id, class: "")
	span(:button_status, text: "Buying...")
	ul(:shape_option, class: "c-fea-blade-shape__selections")
	div(:ui_price, class: ["c-customizer__panel__product-largescreencontrols__content__price"])
	button(:buy, class: ["c-buy-share__cta--buy"])
	button(:add_to_cart, text: "Add to cart")
	div(:vue, id: "benchmade-customizer-vue")
	span(:mini_price, css: "#cart-sidebar > li > div > table > tbody > tr:nth-child(1) > td > span")
	span(:unit_price, css: "#shopping-cart-table > tbody > tr.first.odd > td.product-cart-price > span > span")
	link(:checkout, class: ["button checkout-button"])
	div(:mini_cart, class: ["minicart-wrapper"])
	links(:delete, text: "Delete")
	checkbox(:checkbox, id: "termsAccepted")
	p(:popup, class: ["ltk-disclaimer"])
	div(:prod_popup, id: "ltkmodal-overlay")
	button(:stock_image, css: "#benchmade-customizer-vue > div > div.c-customizer__panel.c-customizer__panel--menu > div.c-customizer__panel__menu-section.c-customizer__panel__menu-section--desktop > div > div.desktopmenu-section__palettegroup > div > div.c-fea-image__image-type-selector > div:nth-child(2) > button")
	select_list(:image_category_dropdown, css: "#benchmade-customizer-vue > div > div.c-modal-container.c-modal-container--active > div > section > section > form > div > div.c-stock-artwork-modal__controls__column.c-stock-artwork-modal__controls__column--filter > select")
	button(:first_image, css: "#benchmade-customizer-vue > div > div.c-modal-container.c-modal-container--active > div > section > section > ul > li:nth-child(1) > button")
	button(:pattern_image, css: "#benchmade-customizer-vue > div > div.c-customizer__panel.c-customizer__panel--menu > div.c-customizer__panel__menu-section.c-customizer__panel__menu-section--desktop > div > div.desktopmenu-section__palettegroup > div > div.c-fea-pattern__image-type-selector > div > button")
	button(:first_pattern, css: "#benchmade-customizer-vue > div > div.c-modal-container.c-modal-container--active > div > section > section > ul > li:nth-child(1) > button")
	textarea(:blade_text, css: "#benchmade-customizer-vue > div > div.c-modal-container.c-modal-container--active > div > section > section > textarea")
	button(:done, css: "#benchmade-customizer-vue > div > div.c-modal-container.c-modal-container--active > div > footer > div:nth-child(2) > button")
	button(:edit_text, class: ["c-fea-text__edit"])
	dd(:product_info, css: "#shopping-cart-table > tbody > tr > td.product-cart-info > dl > dd > dl > dd")

	span(:shape, css: "#benchmade-customizer-vue > div > div.c-customizer__panel.c-customizer__panel--menu > div.c-customizer__panel__menu-section.c-customizer__panel__menu-section--desktop > div > div.desktopmenu-section__featuregroup > div.desktopmenu-section__featuregroup__feature.desktopmenu-section__featuregroup__feature--active > button > span")
	span(:steel, css: "#benchmade-customizer-vue > div > div.c-customizer__panel.c-customizer__panel--menu > div.c-customizer__panel__menu-section.c-customizer__panel__menu-section--desktop > div > div.desktopmenu-section__featuregroup > div:nth-child(2) > button > span")
	span(:finish, css: "#benchmade-customizer-vue > div > div.c-customizer__panel.c-customizer__panel--menu > div.c-customizer__panel__menu-section.c-customizer__panel__menu-section--desktop > div > div.desktopmenu-section__featuregroup > div:nth-child(3) > button > span")


	button(:one_five_four_cm, css: "#bmk-sel-steel-154cm > div.c-fea-blade-steel__selection__image > button")
	button(:steel_tab, css: "#benchmade-customizer-vue > div > div.c-customizer__panel.c-customizer__panel--menu > div.c-customizer__panel__menu-section.c-customizer__panel__menu-section--desktop > div > div.desktopmenu-section__featuregroup > div:nth-child(2) > button")
	button(:color_tab, css: "#benchmade-customizer-vue > div > div.c-customizer__panel.c-customizer__panel--menu > div.c-customizer__panel__menu-section.c-customizer__panel__menu-section--desktop > div > div.desktopmenu-section__featuregroup > div:nth-child(3) > button")
	button(:shape_tab, css: "#benchmade-customizer-vue > div > div.c-customizer__panel.c-customizer__panel--menu > div.c-customizer__panel__menu-section.c-customizer__panel__menu-section--desktop > div > div.desktopmenu-section__featuregroup > div:nth-child(1) > button")

	$recipes = []
	$knives = ["bmk-pro-crookedriver-full","bmk-pro-crookedriver-mini","bmk-pro-grip-full","bmk-pro-barrage-full","bmk-pro-grip-mini","bmk-pro-barrage-mini"]
	$bmk_skus = ['550-S30V','551-S30V','553-S30V','555-S30V','556-S30V','557-S30V','580-S30V','583-S30V','585-S30V','15080-S30V']


	def self.bmk_saved_spec(id)
		case ENV['ENVIRONMENT'].to_sym
		when :prod then Nokogiri::XML.parse(RestClient.get("http://api.spectrumcustomizer.com/benchmade/specification/saved/#{id}"){|response, request, result| response })
		when :staging then Nokogiri::XML.parse(RestClient.get("http://staging.spectrumcustomizer.com/benchmade/specification/saved/#{id}"){|response, request, result| response })
		when :test then Nokogiri::XML.parse(RestClient.get("http://test.spectrumcustomizer.com/benchmade/specification/saved/#{id}"){|response, request, result| response })
		end
	end

	def self.bmk_price(id)
		case ENV['ENVIRONMENT'].to_sym
		when :test then JSON.parse(RestClient.get("http://test.spectrumcustomizer.com/benchmade/specification/saved/#{id}"){|response, request, result| response })
		when :staging then JSON.parse(RestClient.get("http://staging.spectrumcustomizer.com/api/recipesets/pricing/#{id}"){|response, request, result| response })
		when :prod then JSON.parse(RestClient.get("http://api.spectrumcustomizer.com/api/recipesets/pricing/#{id}"){|response, request, result| response })
		end
	end

	def self.bmk_sku_spec_check(readable_id)
		case ENV['ENVIRONMENT'].to_sym
		when :test then response = JSON.parse(RestClient.get("http://test.spectrumcustomizer.com/api/external/benchmade/specification/#{readable_id}/json"){|response, request, result| response })
		when :staging then response = JSON.parse(RestClient.get("http://staging.spectrumcustomizer.com/api/external/benchmade/specification/#{readable_id}/json"){|response, request, result| response })
		when :prod then response = JSON.parse(RestClient.get("http://api.spectrumcustomizer.com/api/external/benchmade/specification/#{readable_id}/json"){|response, request, result| response })
		end
		return response["KnifeDetails"]["Model"]
	end

	def self.bmk_sku_endpoint_check(sku)
		case ENV['ENVIRONMENT'].to_sym
		when :test then response = JSON.parse(RestClient.get("https://test.spectrumcustomizer.com/api/producthandle/#{sku}"){|response, request, result| response })
		when :staging then response = JSON.parse(RestClient.get("https://staging.spectrumcustomizer.com/api/producthandle/#{sku}"){|response, request, result| response })
		when :prod then response = JSON.parse(RestClient.get("https://api.spectrumcustomizer.com/api/producthandle/#{sku}"){|response, request, result| response })
		end
		return response
	end

	def check_cart_price
		asset = $knives.sample
		puts asset
		case ENV['ENVIRONMENT'].to_sym
		when :test then $driver.goto("http://madetoordertest.blob.core.windows.net/benchmade/frontend/index.html#/product/#{asset}")
		when :staging
			case asset
			when "bmk-pro-crookedriver-full" then
				$driver.goto("https://staging.benchmade.com/customizer/crooked-river-family.html?customize=1#/product/bmk-pro-crookedriver-full")
			when "bmk-pro-grip-full" then
				$driver.goto("https://staging.benchmade.com/customizer/griptilian-family.html?customize=1#/product/bmk-pro-grip-full")
			when "bmk-pro-grip-mini" then
				$driver.goto("https://staging.benchmade.com/customizer/mini-griptilian-family.html?customize=1#/product/bmk-pro-grip-mini")
			when "bmk-pro-barrage-full" then
				$driver.goto("https://staging.benchmade.com/customizer/barrage-family.html?customize=1#/product/bmk-pro-barrage-full")
			when "bmk-pro-barrage-mini" then
				$driver.goto("https://staging.benchmade.com/customizer/mini-barrage-family.html?customize=1#/product/bmk-pro-barrage-mini")
			when "bmk-pro-crookedriver-mini" then
				$driver.goto("https://staging.benchmade.com/customizer/mini-crooked-river-family.html?customize=1#/product/bmk-pro-crookedriver-mini")
			end
		when :prod then 
			case asset
			when "bmk-pro-crookedriver-full" then
				$driver.goto("https://www.benchmade.com/customizer/crooked-river-family.html?customize=1#/product/bmk-pro-crookedriver-full")
			when "bmk-pro-grip-full" then
				$driver.goto("https://www.benchmade.com/customizer/griptilian-family.html?customize=1#/product/bmk-pro-grip-full")
			when "bmk-pro-grip-mini" then
				$driver.goto("https://www.benchmade.com/customizer/mini-griptilian-family.html?customize=1#/product/bmk-pro-grip-mini")
			when "bmk-pro-barrage-full" then
				$driver.goto("https://www.benchmade.com/customizer/barrage-family.html?customize=1#/product/bmk-pro-barrage-full")
			when "bmk-pro-barrage-mini" then
				$driver.goto("https://www.benchmade.com/customizer/mini-barrage-family.html?customize=1#/product/bmk-pro-barrage-mini")
			when "bmk-pro-crookedriver-mini" then
				$driver.goto("https://www.benchmade.com/customizer/mini-crooked-river-family.html?customize=1#/product/bmk-pro-crookedriver-mini")
			end
		end

		self.wait_until(60) { self.ui_price_element.present? }
		sleep 2

		if self.popup? || self.prod_popup?
			$driver.refresh
			self.wait_until(60) { self.ui_price_element.present? }
			sleep 2
		end
		
		self.catagory_groups_elements.each do |cg|			
			if cg.button.attribute('title').include?('Lasermark') == false			
				if cg.attribute('class').include?('active') == false				
					cg.button.click			
				end				
				self.feature_groups_elements.each do |fg|
					if fg.attribute('class').include?('active') == false						
						fg.button.click			
					end										
					case
					when fg.button.text.include?("SHAPE")
						shapes = self.shape_option_element.lis
						shape = shapes[rand(shapes.length)]
						shape.button.click
					when fg.button.text.include?("STEEL")
						steel = self.steel_option_elements
						steel.sample.button.click		
					when fg.button.text.include?("MATERIAL")
						material = self.material_option_elements
						material.sample.click
					when fg.button.text.include?("CLIP")
						clip = self.clip_option_elements
						clip.sample.click
					when fg.button.text.include?("STEEL") == false && fg.button.text.include?("MATERIAL") == false && fg.button.text.include?("CLIP") == false
						color = self.color_option_elements
						color.sample.button.click
					end
				end
			else
				if cg.attribute('class').include?('active') == false				
					cg.button.click			
				end				
				self.feature_groups_elements.each do |fg|
					if fg.attribute('class').include?('active') == false						
						fg.button.click			
					end									
					case
					when fg.button.text.include?("IMAGE")
						self.stock_image
						self.image_category_dropdown
						category = self.image_category_dropdown_options.sample
						if category.nil?
							category = self.image_category_dropdown_options.sample
						end
						self.image_category_dropdown=category
						self.first_image
					when fg.button.text.include?("PATTERN")
						self.pattern_image
						self.first_pattern
					when fg.button.text.include?("TEXT")
						self.edit_text
						self.blade_text = "TEST"
						self.done
					end
				end
			end
		end
		self.buy
		self.check_checkbox
		self.add_to_cart
		knife_price = self.ui_price
		sleep 2
		self.wait_until { self.mini_cart_element.present? }
		mini_cart_price = self.mini_price
		self.checkout_element.click
		self.wait_until { self.url.include?('cart') }
		sleep 2
		cart_price = self.unit_price
		puts self.product_info
		delete_elements.length.times do
			delete_elements[0].click
		end
		return knife_price, mini_cart_price, cart_price
	end

	def random_knife
		asset = ["bmk-pro-grip-full","bmk-pro-grip-mini"].sample
		case ENV['ENVIRONMENT'].to_sym
		when :test then
			$driver.goto("http://madetoordertest.blob.core.windows.net/benchmade/frontend/index.html#/product/#{asset}")
		when :staging then 
			$driver.goto("http://madetoorderstaging.azureedge.net/benchmade/frontend/index.html#/product/#{asset}")
		when :prod then 
			$driver.goto("http://madetoorder.azureedge.net/benchmade/frontend/index.html#/product/#{asset}")
		end
		self.wait_while(timeout: 60, message: "Page Failed to Load after 60 seconds") { self.ui_price == "" }
		sleep 2
		self.catagory_groups_elements.each do |cg|			
			if cg.button.attribute('title').include?('Lasermark') == false			
				if cg.attribute('class').include?('active') == false				
					cg.button.click			
				end				
				self.feature_groups_elements.each do |fg|
					if fg.attribute('class').include?('active') == false						
						fg.button.click			
					end										
					case
					when fg.button.text.include?("SHAPE")
						shapes = self.shape_option_element.lis
						shape = shapes[rand(shapes.length)]
						shape.button.click
					when fg.button.text.include?("STEEL")
						steel = self.steel_option_elements
						steel.sample.button.click		
					when fg.button.text.include?("MATERIAL")
						material = self.material_option_elements
						material.sample.click
					when fg.button.text.include?("CLIP")
						clip = self.clip_option_elements
						clip.sample.click
					when fg.button.text.include?("STEEL") == false && fg.button.text.include?("MATERIAL") == false && fg.button.text.include?("CLIP") == false
						color = self.color_option_elements
						color.sample.button.click
					end
				end
			end
		end
		sleep 3
		knife_price = self.ui_price
		self.buy
		sleep 1
		self.check_checkbox
		self.add_to_cart
		sleep 1

		self.wait_until { $driver.alert.exists? }
		if $driver.alert.exists?
			alert_string = $driver.alert.text.scan(/\{.*?\}.+/)
			# puts alert_string[0]
			response = JSON.parse(alert_string[0])
			$driver.alert.close
		end
		# puts "#{knife_id} | #{knife_price}"
		sku = response['data']['sku']
		knife_id = response['data']['custom_knife_id']
		remote_price = response['data']['remote_price']
		remote_sku = response['data']['remote_sku']
		model = response['data']['options']['custom_knife']['Model']
		sleep 1

		return knife_id, knife_price, remote_price, sku, remote_sku, model
	end
end

