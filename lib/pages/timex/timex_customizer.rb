class TimexCustomizer < BasePage
	include PageObject

	# => Buttons
	button :case_tab, text: "Case"
	button :dial_tab, text: "Dial"
	button :strap_tab, text: "Strap"
	button :engraving_tab, text: "Engrave"
	button :engrave_initials, text: "Initials"
	button :engrave_message, text: "Message"
	button :engrave_none, text: "None"
	button :open_engrave_modal, class: ["ft-font__edit-text"]
	button :done, text: "Done"
	button :share, text: "share"
	button :save, text: "save for later"
	button :reset, text: "reset"
	button :copy_share_link, text: "Copy"
	button :ok, text: "OK"
	button :send_email, text: "Send"
	button :confirm_reset, text: "Reset"

	button :default_case_color, css: "#main-wrapper > main > section.feature-controls > nav > button.is-selected"


	# => Lists/Swatches
	ul :design_swatches, class: ["selection-list ft-component__selection-list"]
	ul :color_swatches, class: ["selection-list ft-color__selection-list"]
	ul :text_options, class: ["ft-text-type__selection-list"]
	select_list :font_dropdown, class: ["ft-font__selection-dropdown"]
	text_field :text, id: "textFeatureString"
	div :loading_bar, class: ["hero__miniloader"]
	div :graphic, 	class: ["graphic"]
	div :share_modal, class: ["modal-content"]
	div :save_modal, class: ["c-modal modal--save trap-active"]
	img :share_image, css: "#main-wrapper > main > div.modal-container > div > div > section > div > div.share__image-container > img.share__image.share__image-outline"
	text_field :email, css: "#main-wrapper > main > div.modal-container > div > div > section > div > div.save__send-url > input[type='email']"
	checkbox :email_consent, id: "email-consent"


	def iterate_tab
		self.design_swatches_element.lis.each do |li|
			li.button.click
			self.color_swatches_element.lis.each do |item|
				item.button.click
			end
		end
	end

	def text_tab
		self.engrave_initials
		self.open_engrave_modal
		self.text = "123"
		self.done
		self.font_dropdown_options.each do |opt|
			self.font_dropdown = opt
		end
		self.engrave_none
		self.engrave_message
		self.open_engrave_modal
		self.text = "1234567890"
		self.done
		self.font_dropdown_options.each do |opt|
			self.font_dropdown = opt
		end
		sleep 2
	end
end