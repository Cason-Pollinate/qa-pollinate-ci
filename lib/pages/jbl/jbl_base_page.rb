class JBLBasePage < BasePage

button			:step_one,				class: 			["jbl-custom__headphone-feature-menu-steps-1"]
button			:step_one,				class: 			["jbl-custom__headphone-feature-menu-steps-2"]
button			:left_ear, 				text: 			"Left Ear"
button			:right_ear, 			text: 			"Right Ear"
button			:design,				text: 			"Design"
button			:color, 				text: 			"Color"
button			:pattern, 				text: 			"Pattern"
button			:print, 				text: 			"Print"
button			:step_one_text,			text: 			"Text"
button			:logo, 					text: 			"Logo"
checkbox		:straight_text, 		id: 			"textStraight"
checkbox 		:curved_text, 			id: 			"textCurved"
text_field		:step_one_text_field, 	css: 			"#app > div > div.jbl-custom__headphone-feature-menu > div.jbl-custom__feature-menu > div.jbl-custom__feature-menu-items > div.jbl-custom__feature-menu-items--sub-items > div:nth-child(3) > div.jbl-custom__text-feature > input[type='text']"
select_list 	:font_dropdown, 		class: 			["ft-font__selection-dropdown"]
divs 			:colors, 				class: 			["jbl-custom__swatch-list-color-swatch"]
divs			:pattern_buttons,		class: 			["jbl-custom__pattern-feature-thumbnail-list"]
divs 			:print_buttons,			class: 			["jbl-custom__print-feature-thumbnail-list"]
div 			:loading, 				id: 			"cssload-loader"
div 			:menu, 					class: 			["jbl-custom__speaker-feature-menu"]



	$jbl_sku = ['JBLFLIP4CUSTOM',
				"JBLFLIP4WHTCUSTOM",
				"JBLFLIP4BLUCUSTOM",
				"JBLFLIP4TELCUSTOM",
				"JBLFLIP4REDCUSTOM",
				"JBLE55BTWHTCUSTOM",
				"JBLE55BTBLKCUSTOM",
				"JBLE55BTBLUCUSTOM",
				"JBLE55BTTELCUSTOM",
				"JBLE45BTWHTCUSTOM",
				"JBLE45BTBLKCUSTOM",
				"JBLE45BTTELCUSTOM",
				"JBLE45BTREDCUSTOM",
				"JBLLINK10WHTCUSTOM",
				"JBLLINK10BLKCUSTOM",
				"JBLCLIP3WHTCUSTOM",
				"JBLCLIP3BLUCUSTOM",
				"JBLCLIP3PNKCUSTOM",
				"JBLCLIP3TELCUSTOM",
				"JBLGO2GRYCUSTOM",
				"JBLGO2BLKCUSTOM",
				"JBLGO2BLUCUSTOM",
				"JBLGO2REDCUSTOM",
				'JBLE55BTCUSTOM']

	def get_readable_id
		string = self.ber_route_element.attribute("style")
		recipe = string.scan(/\w+[A-Z0-9]\w+/)
		return recipe[0]
	end


	def navigate_to_asset(sku)
		case ENV['ENVIRONMENT'].to_sym
		when :test then $driver.goto("http://madetoordertest.blob.core.windows.net/jbl/frontend/index.html?sku=#{sku}")
		when :staging then $driver.goto("http://cdn-staging.spectrumcustomizer.com/jbl/frontend/index.html?sku=#{sku}")
		when :prod then $driver.goto("http://cdn.spectrumcustomizer.com/jbl/frontend/index.html?sku=#{sku}")
		end
	end
end