class SpectrumBasePage < BasePage

	div(:page_content, 		id: 		"page")
	link(:request_demo_button,		class: 		["main-navigation__request-demo"])
	text_field(:name_field,		id: 		"field_ygsjz")
	text_field(:company_field, 	id: 		"field_3ytzx")
	text_field(:email_field, 		id: 		"field_s2coz")
	text_field(:phone_field, 		id: 		"field_4ppiu")
	text_area(:message_field, 	id: 		"field_kkwon")
	checkbox(:promo_checkbox, 		id: 		"field_zv340-0")
	button(:submit_button, 		class: 		["frm_final_submit"])

	div(:request_modal, 		id: 		"request-demo")
	p(:submitted_request_modal, text: 		"Your responses were successfully submitted. Thank you!")

	def fill_request_form
		self.wait_while { self.request_modal_element.visible? }
		self.wait_until { self.request_demo_button? }
		sleep 1
		self.request_demo_button_element.click
		if self.submitted_request_modal?
			$driver.refresh
		else
			self.name_field = "POLLINATE QA TEST"
			self.company_field = "POLLINATE QA"
			self.email_field = "pollinatetest@gmail.com"
			self.phone_field = "123-456-7890"
			self.message_field = "THIS IS A TEST OF THE REQUEST SUBMISSION FORM PERFORMED BY POLLINATE INTERNAL QA TEAM."
			self.check_promo_checkbox
			self.submit_button_element.click
		end
	end

end