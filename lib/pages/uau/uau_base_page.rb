require './lib/pages/base_page'
require 'page-object'

class UAUBasePage < BasePage
	include PageObject

	element(:page_load, tag_name: "modal", class: ["main-modal-container ng-scope ng-isolate-scope"])
	element(:loaded, tag_name: "uau-loader", class: "loaded")
	div(:product_image, class: ["product__image"])
	button(:save, class: "save")
	text_field(:name_design, name: "name-design")
	button(:submit, text: "SUBMIT")
	button(:select_product, text: "Select")
	button(:color_modal_done, text: "Done")
	button(:color_modal_skip, text: "Skip")
	image(:color_modal_close, class: ["close ng-scope"])
	h1(:save_complete, class: "ng-binding")
	div(:progress_bar, class: "progress-bar")

	$ua_material_id = [
		"1292573-999",
		"1304687-999",
		"1304689-999",
		"1304690-999",
 		"1278799-001,1278793-001",
 		"1241720-001,1244429-001",
 		"1299057-001,1292440-001",
 		"1278867-001,1280478-001",
 		"1299055-001,1280478-001",
 		"1292598-001,1292604-001",
 		"1249208-001,1249210-001",
 		"1249206-001,1278396-001",
 		"1249206-001,1278400-001",
 		"1249666-001,1249667-001",
 		"1262008-001,1280478-001",
 		"1249204-001,1249205-001",
 		"1249202-001,1249203-001",
 		"1262040-040,1292604-040"
	]

	def goto_asset(id)
		self.goto("http://demo.madetoordercustomizer.com/under-armour/#{ENV['ENVIRONMENT']}/uau/frontend/index.html#/materialIds/#{id}")
		puts "Start Time: #{Time.new.strftime("%H:%M:%S")}"
		self.wait_until(60) { self.loaded? }
		self.wait_while { self.page_load_element.exists? }
		self.wait_until { self.save_element.visible? }
		puts "End Time:   #{Time.new.strftime("%H:%M:%S")}"
		# puts "Summary | Response Time: #{self.performance.summary[:response_time]/1000.0}"
		# puts "Summary | Time To Last Byte: #{self.performance.summary[:time_to_last_byte]/1000.0}"
		# puts "Memory | Total JS Heap: #{self.performance.memory[:total_js_heap_size]}"
		# puts "Memory | JS Heap Limit: #{self.performance.memory[:js_heap_size_limit]}"
		# puts "Memory | JS Heap Used: #{self.performance.memory[:used_js_heap_size]}"
		# puts "Timing | Connection End: #{self.performance.timing[:connect_end]/1000.0}"
		# puts "Timing | Response End: #{self.performance.timing[:response_end]/1000.0}"
		# puts "Timing | DOM Loading: #{self.performance.timing[:dom_loading]/1000.0}"
		# puts "Timing | DOM Content Loaded: #{self.performance.timing[:dom_content_loaded_event_end]/1000.0}"
	end

	def uau_product(id)
		$driver.goto ("#{$base_url}#{id}")
		$driver.goto ("#{$base_url}#{id}")
	end

	def get_page_errors
		begin self.wait_until(10) { self.product_image? }
			if self.product_image?
				self.product_image_element.click
			end
		rescue Watir::Wait::TimeoutError, Watir::Exception::UnknownObjectException, Timeout::Error
			false
		end
		self.wait_until(60) { self.loaded? }
		self.wait_while { self.loader_element.visible? }
		self.wait_while { self.page_load_element.exists? }
		begin self.wait_until(10) { self.color_modal_skip? }
			if self.color_modal_skip?
				self.color_modal_skip_element.click
			end
		rescue Watir::Wait::TimeoutError, Watir::Exception::UnknownObjectException, Timeout::Error
			false
		end
		self.wait_until { self.save_element.visible? }
		self.save
		self.name_design = "test"
		self.submit
		sleep 5
		BasePage.print_js_errors
	end
end