class AssetAPI < BasePage


	def self.set_product_data_url_env(product)
		case ENV['ENVIRONMENT'].to_sym
		when :test then @specs = JSON.parse(RestClient.get("http://test.spectrumcustomizer.com/api/products/#{product}"){|response, request, result| response })
		when :staging then @specs = JSON.parse(RestClient.get("http://staging.spectrumcustomizer.com/api/products/#{product}"){|response, request, result| response })
		when :prod then @specs = JSON.parse(RestClient.get("http://api.spectrumcustomizer.com/api/products/#{product}"){|response, request, result| response })
		end
		return @specs
	end

	def self.set_manifest_url_env(product,manifest)
		case ENV['ENVIRONMENT'].to_sym
		when :test then @specs = JSON.parse(RestClient.get("http://madetoordertest.blob.core.windows.net/webgl/client/#{ENV['SITE']}/#{product}/" + manifest){|response, request, result| response })
		when :staging then @specs = JSON.parse(RestClient.get("http://madetoorderstaging.blob.core.windows.net/webgl/client/#{ENV['SITE']}/#{product}/" + manifest){|response, request, result| response })
		when :prod then @specs = JSON.parse(RestClient.get("http://madetoorder.blob.core.windows.net/webgl/client/#{ENV['SITE']}/#{product}/" + manifest){|response, request, result| response })
		end
		return @specs
	end

	def self.get_scenefile_handles
		@handles = Array.new
		$driver.goto('http://preview.madetoordercustomizer.com/')
		sf = BasePage.collect_links_text
		sf.each do |string|
			if ((string.include?('prs')) && ((string.include?('demo') == false) || (string.include?('test') == false)))
				@handles.push(string)
			end
		end
		return @handles.uniq
	end

	def self.scene_connection_with_fillname(scene)
		scene_productgroups_keys(scene).each do |group|
			list = (@specs["productGroups"]["#{group}"]["productOptions"].keys)
			list.each do |key|
				if @specs["productGroups"]["#{group}"]["productOptions"]["#{key}"]["connections"] != nil
					@specs["productGroups"]["#{group}"]["productOptions"]["#{key}"]["connections"].each do |val|
						if val[0].scan(/(fill-name)/) != [] || val[0].scan(/(fill-type)/) != []
							puts "#{scene} | #{key} | #{val[0]}"
						end
					end
				end
			end
		end
	end

	def self.get_product_handles
		@links = Array.new
		@handles = Array.new
		$scene_files.each do |scene|
			options = AssetAPI.scene_productoptions_keys(scene)
			@handles.push(options.flatten)
			@handles.uniq
			@handles.reject { |r| r.include?("extra") && r.include?("mannequin") && r.include?("scene.json") && r.include?("dummy") }
		end
		@handles.flatten
	end

	def self.set_scenefile_url(scene)
		if $leatherman_scene_files.include? "#{scene}"
			ENV['SITE'] = 'leatherman'
		end

		if $levis_scene_files.include? "#{scene}"
			ENV['SITE'] = 'levis'
		end

		if $tmx_scene_files.include? "#{scene}"
			ENV['SITE'] = 'timex'
		end

		if $giro_scene_files.include? "#{scene}"
			ENV['SITE'] = 'giro'
		end

		if $gk_scene_files.include? "#{scene}"
			ENV['SITE'] = 'gk-elite'
		end

		if $uau_scene_files.include? "#{scene}"
			ENV['SITE'] = 'under-armour'
		end

		if $uaf_scene_files.include? "#{scene}"
			ENV['SITE'] = 'ua-icon'
		end

		if $cb_scene_files.include? "#{scene}"
			ENV['SITE'] = 'camelbak'
		end

		if $eb_scene_files.include? "#{scene}"
			ENV['SITE'] = 'eddie-bauer'
		end

		if $bm_scene_files.include? "#{scene}"
			ENV['SITE'] = 'benchmade'
		end

		if $bauer_scene_files.include? "#{scene}"
			ENV['SITE'] = 'bauer'
		end

		if $jbl_scene_files.include? "#{scene}"
			ENV['SITE'] = 'jbl'
		end

		if $wilson_scene_files.include? "#{scene}"
			ENV['SITE'] = 'wilson'
		end

		case ENV['ENVIRONMENT'].to_sym
		when :test then url = "http://madetoordertest.blob.core.windows.net/webgl/client/#{ENV['SITE']}/scenelib/#{ENV['SITE']}/#{scene}/scene.json"
		when :staging then url = "http://madetoorderstaging.blob.core.windows.net/webgl/client/#{ENV['SITE']}/scenelib/#{ENV['SITE']}/#{scene}/scene.json"
		when :prod then url = "http://madetoorder.blob.core.windows.net/webgl/client/#{ENV['SITE']}/scenelib/#{ENV['SITE']}/#{scene}/scene.json"
		end
		return url
	end


	# =>  gets SceneFile product groups to interpolate into path (eg. Tops, Bottoms, Socks, etc.)
	def self.scene_productgroups_keys(scene)
		url = set_scenefile_url(scene)
		@specs = JSON.parse(RestClient.get(url){|response, request, result| response })
		@arr = Array.new
		list = @specs["productGroups"].keys
		list.each do |key|
			if (key.include?("mannequin") == false && key.include?("extra") == false && key.include?("scene.json") == false && key.include?("dummy") == false && key.nil? == false && key.empty? == false && key.include?('test') == false)
				@arr.push(key)
			end
		end
		@arr
	end


	# =>  grab all Product Handles from SceneFile (eg. gk-fea-6018-smoke)
	def self.scene_productoptions_keys(scene)
		@options = Array.new
		scene_productgroups_keys(scene).each do |group|
			list = @specs["productGroups"]["#{group}"]["productOptions"].keys
			list.each do |key|
				if (key.include?("mannequin") || key.include?("extra") || key.include?("scene.json") || key.include?("dummy") || key.nil? || key.empty? || key.include?('test'))
				else
					@options.push(key)
				end
			end
		end
		@options
	end

	# =>  grabs all scenefile keys in the 'connections' hash
	def self.scene_connection_keys(scene)
		@connections = Array.new
		scene_productgroups_keys(scene).each do |group|
			list = (@specs["productGroups"]["#{group}"]["productOptions"].keys)
			list.each do |key|
				if (key.include?("mannequin") || key.include?("extra") || key.include?("scene.json") || key.include?("dummy") || key.nil? || key.empty? || key.include?('test') || key.include?('roughness'))
				else
					@connections.push(@specs["productGroups"]["#{group}"]["productOptions"]["#{key}"]["connections"].keys)
				end
			end
		end
		@connections
	end

	# =>  grabs all scenefile values in the 'connections' hash
	def self.scene_connection_values(scene)
		@values = Array.new
		scene_productgroups_keys(scene).each do |group|
			list = (@specs["productGroups"]["#{group}"]["productOptions"].keys)
			list.each do |key|
				if (key.include?("mannequin") || key.include?("extra") || key.include?("scene.json") || key.include?("dummy") || key.nil? || key.empty? || key.include?('test') || key.include?('roughness'))
				else
					@values.push(@specs["productGroups"]["#{group}"]["productOptions"]["#{key}"]["connections"].values)
				end
			end
		end
		@values
	end

	# => grabs all scenefile key:value pairs in the 'connections' hash
	def self.scene_connections(scene)
		@pairs = Array.new
		scene_productgroups_keys(scene).each do |group|
			list = (@specs["productGroups"]["#{group}"]["productOptions"].keys)
			list.each do |key|
				if (key.include?("mannequin") || key.include?("extra") || key.include?("scene.json") || key.include?("dummy") || key.nil? || key.empty? || key.include?('test') || key.include?('roughness'))
				else
					@pairs.push(@specs["productGroups"]["#{group}"]["productOptions"]["#{key}"]["connections"])
				end
			end
		end
		@pairs
	end

	# => grabs scenefile manifest url suffix to append to manifest base URL
	def self.scene_manifest_url(scene)
		@manifest = Array.new
		scene_productgroups_keys(scene).each do |group|
			list = (@specs["productGroups"]["#{group}"]["productOptions"].keys)
			list.each do |key|
				if (key.include?("mannequin") || key.include?("extra") || key.include?("scene.json") || key.include?("dummy") || key.nil? || key.empty? || key.include?('test') || key.include?('roughness'))
				else
					@manifest.push(@specs["productGroups"]["#{group}"]["productOptions"]["#{key}"]["manifest"])
				end
			end
		end
		@manifest.uniq.first
	end

	def self.scene_mobile_manifest_url(scene)
		@manifest = Array.new
		scene_productgroups_keys(scene).each do |group|
			list = (@specs["productGroups"]["#{group}"]["productOptions"].keys)
			list.each do |key|
				if (key.include?("mannequin") || key.include?("extra") || key.include?("scene.json") || key.include?("dummy") || key.nil? || key.empty? || key.include?('test') || key.include?('roughness'))
				else
					@manifest.push(@specs["productGroups"]["#{group}"]["productOptions"]["#{key}"]["mobileManifest"])
				end
			end
		end
		@manifest.uniq.first
	end

	# =>  grabs all featureHandle values from the Product Data
	# def self.product_handle_values(product)
	# 	case ENV['ENVIRONMENT'].to_sym
	# 	when :test then @specs = JSON.parse(RestClient.get("http://test.spectrumcustomizer.com/api/products/#{product}"){|response, request, result| response })
	# 	when :staging then @specs = JSON.parse(RestClient.get("http://staging.spectrumcustomizer.com/api/products/#{product}"){|response, request, result| response })
	# 	when :prod then @specs = JSON.parse(RestClient.get("http://api.spectrumcustomizer.com/api/products/#{product}"){|response, request, result| response })
	# 	end
	# 	file = File.open("./lib/helpers/recursive_feature_traversal.js", "r")
	# 	features = file.read
	# 	if @specs["contents"]["rootFeature"].nil? || @specs["contents"]["rootFeature"].empty? || @specs["contents"]["rootFeature"] == []
	# 		puts "#{product} | Missing Product Handle Values"
	# 	else
	# 		result = $driver.execute_script(features, @specs["contents"]["rootFeature"])
	# 		result.reject { |r| r.empty? || r.nil? || r == "" }
	# 	end
	# end





	def self.product_handle_values(product)
		array = Array.new
		@specs = set_product_data_url_env(product)
		if @specs["contents"] != nil
			@specs["contents"]["rootFeature"]["childFeatures"].each do |cf|

				if cf['handle'] != nil && cf['handle'] != ''
					array.push(cf['handle'])
				end

				if cf['selectionGroup']['selections'] != nil && cf['selectionGroup'] != nil
					cf['selectionGroup']['selections'].each do |cfs|
						if cfs['features'] != nil
							cfs['features'].each do |cfsf|
								if cfsf['handle'] != nil && cfsf['handle'] != ''
									array.push(cfsf['handle'])
								end
							end
						end
					end
				else
					puts "#{product} | Cannot Access Product Handle Values"
				end

				if cf["featureSelectionGroups"] != nil
					cf["featureSelectionGroups"].each do |fsg|
						if fsg["thenSelectionGroup"]["selections"] != nil
							fsg["thenSelectionGroup"]["selections"].each do |fsgs|
								if fsgs["features"] != nil
									fsgs["features"].each do |fea|
										if fea['handle'] != nil && fea['handle'] != ''
											array.push(fea['handle'])
										end
									end
								end
							end
						end
					end
				end

				cf["childFeatures"].each do |cfcf|
					if cfcf['handle'] != nil && cfcf['handle'] != ''
						array.push(cfcf['handle'])
					end
					cfcf["childFeatures"].each do |cfcfcf|
						if cfcfcf['handle'] != nil && cfcfcf['handle'] != ''
							array.push(cfcfcf['handle'])
						end
					end
				end
			end
		else
			puts "spectrumcustomizer.com/api/products/#{product} | Product Data Does Not Exist For This Asset"
		end
		return array.uniq.reject { |r| r == nil || r == '' || r.include?('extra') }
	end

	def self.validate_home_away(product)
		@specs = set_product_data_url_env(product)
		if @specs != nil && @specs['contents'] != nil && @specs['contents']['rootFeature'] != nil
			@specs["contents"]["rootFeature"]["childFeatures"].each do |cf|
				if cf["index"] == 0 && cf['handle'].include?('color')
					puts "#{product} | #{cf['handle']} 'contents.rootFeature.childFeatures.index' value set to '0'"
				end
			end
		else
			puts "spectrumcustomizer.com/api/products/#{product} | Product Data Does Not Exist For This Asset"
		end
	end


	# =>  grabs all the keys from the manifest parameters array
	def self.manifest_parameter_keys(product, manifest)
		if product.nil?
		elsif manifest.nil?
		else
			@specs = set_manifest_url_env(product,manifest)
			result = @specs["parameters"].keys
		end
	end

	# =>  grabs all the keys from the manifest parameters array
	def self.manifest_parameter_values(product, manifest)
		if product.nil?
		elsif manifest.nil?
		else
			@specs = set_manifest_url_env(product,manifest)
			result = @specs["parameters"].values
		end
	end

	def self.manifest_model(product, manifest)
		if product.nil?
			puts "Missing Product Data"
		elsif manifest.nil?
			puts "Missing Manifest"
		else
			@specs = set_manifest_url_env(product,manifest)
			@models = Array.new
			regionmaps = @specs["shaders"].keys
			regionmaps.each do |regionMap|
				model_values = @specs["shaders"]["#{regionMap}"]["models"].values
				model_values.each do |model|
					@models.push(model)
				end
			end
			@models
		end
	end

	def self.manifest_shader_source(product, manifest)
		if product.nil?
			puts "Missing Product Data"
		elsif manifest.nil?
			puts "Missing Manifest"
		else
			@specs = set_manifest_url_env(product,manifest)
			@shaders = Array.new
			regionmaps = @specs["shaders"].keys
			regionmaps.each do |regionMap|
				@shaders.push(@specs["shaders"]["#{regionMap}"]["source"])
			end
			@shaders
		end
	end

	# def self.product_data_selection_values(product)
	# 	@values = Array.new
	# 	@data = JSON.parse(RestClient.get("http://test.spectrumcustomizer.com/api/products/#{product}"){|response, request, result| response })
	# 	@manifest = JSON.parse(RestClient.get("http://madetoordertest.blob.core.windows.net/webgl/client/gk-elite/#{product}/config/product.manifest"){|response, request, result| response })
	# 	# if (@manifest['shaders']['instances']['models'].keys) == nil
	# 	# 	puts "#{product} >>> Check Manifest Structure"
	# 	# else
	# 		@data['contents']['rootFeature']['childFeatures'].each do |cf|
	# 			cf['childFeatures'].each do |ccf|
	# 				ccf['selectionGroup']['selections'].each do |sgs|
	# 					@values.push(sgs['value'])
	# 				end
	# 				ccf['childFeatures'].each do |cccf|
	# 					cccf['selectionGroup']['selections'].each do |sgsels|
	# 						@values.push(sgsels['value'])
	# 					end
	# 				end
	# 				ccf['featureSelectionGroups'].each do |fsg|
	# 					fsg['thenSelectionGroup']['selections'].each do |tsgs|
	# 						@values.push(tsgs['value'])
	# 					end
	# 				end
	# 			end
	# 		end
	# 		@values.reject! { |r| r.include?("#") || r.empty? || r.nil? || r == "1" || r == 'inktek' || r == "tricot" || r == "forward" || r == "reverse" || r == "2" || r == "3" || r == "blue" || r == "blues" || r == "reds" || r == "black white" || r == "greens" || r == "orange" || r == "patriotic" || r == "pink" || r == "purple" || r.include?('-') || r.include?('nylpf') || r.include?('mesh') || r.include?('hologram') || r.include?('velvet') || r.include?('embroidery') || r.include?('crystals') || r.include?('drytech') || r.include?('holotek') || r.include?('subfuse') || r.include?('sublimated') || r.include?('polytek')}
	# 		@models.reject! { |r| r.include?('jewel') || r.include?('sequin') || r.include?('spangle')  }
	# 		return [@models.sort,@values.sort]
	# 	# end
	# end

	$jbl_scene_files = ['jbl-prs-speakers-headphones']

	$bauer_scene_files = ['bau-prs-hockey-skates',
		'bau-prs-player-sticks','bau-prs-goalieequipment-vapor2xpro','bau-prs-goalieequipment-supreme2spro','bau-prs-goalie-sticks']

		$wilson_scene_files = ['wil-prs-football']

		$leatherman_scene_files = ['ltg-prs-tools']

		$tmx_scene_files = ['tmx-prs-wrist-watches']

		$giro_scene_files = ['giro-prs-helmets']

		$levis_scene_files = ['lev-prs-custom-tshirts','lev-prs-trucker-jackets']

		$bm_scene_files = ['bmk-prs-knives']

		$approved_shaders = ['regionmap','artbox','standard','pshadow','translucent']

		$jbl_scene_files = ['jbl-prs-speakers-headphones']

		$gk_scene_files = [
			'gk-prs-bottoms',
		# 'gk-prs-cheer',
		'gk-prs-warmups',
		'gk-prs-gym',
		# 'ua-prs-cheer',
		'ua-prs-gym',
		'ua-prs-warmups'
	]

	$uau_scene_files = [
		'uau-prs-volleyball',
		'uau-prs-track',
		'uau-prs-soccer',
		'uau-prs-football',
		'uau-prs-baseball',
		'uau-prs-basketball',
		'uau-prs-lacrosse',
		'uau-prs-softball',
		'uau-prs-training',
		'uau-prs-wrestling',
		'uau-prs-sideline'
	]

	$uaf_scene_files = [	
		"uaf-prs-charged247-mens",
		"uaf-prs-charged247-womens",
		# "uaf-prs-clutchfit-mens",
		# "uaf-prs-clutchfit-womens",
		"uaf-prs-curry1-mens",
		"uaf-prs-curry1-youth",
		"uaf-prs-curry1low-mens",
		"uaf-prs-curry25-mens",
		"uaf-prs-curry5-kids",
		"uaf-prs-curry5-mens",
		"uaf-prs-curry6-mens",
		# "uaf-prs-dev-test",
		# "uaf-prs-drive4-mens",
		# "uaf-prs-drive4-womens",
		"uaf-prs-drive4low-mens",
		"uaf-prs-highlight-mens",
		"uaf-prs-hovrhavoc-mens",
		"uaf-prs-hovrhavoc-womens",
		"uaf-prs-hovrhavoclow-mens",
		"uaf-prs-hovrslk-mens",
		"uaf-prs-hovrslk-womens",
		"uaf-prs-icon-sackpack",
		"uaf-prs-ignite-mens",
		"uaf-prs-ignite-womens",
		# "uaf-prs-railfit-mens",
		# "uaf-prs-railfit-womens",
		"uaf-prs-spotlight-mens",
		"uaf-prs-yard-mens",
		# "uaf-prs-curry5le-mens"
		"uaf-prs-curry6-mens",
		"uaf-prs-kicksprint2-mens",
		"uaf-prs-phantom-mens",
		"uaf-prs-phantom-womens"
	]



	$cb_scene_files = [
		'cb-prs-chute-set',
		'cb-prs-chute-vacuum-set',
		'cb-prs-eddy-insulated-set',
		'cb-prs-eddy-kids-set',
		'cb-prs-eddy-set',
		'cb-prs-eddy-vacuum-set',
		'cb-prs-forge-set',
		'cb-prs-kickbak-set',
		'cb-prs-podium-set'
	]

	$eb_scene_files = [
		'eb-pro-mens-microtherm-hood',
		'eb-pro-mens-microtherm-nohood',
		'eb-pro-mens-sandstone-hood',
		'eb-pro-mens-sandstone-nohood',
		'eb-pro-womens-microtherm-hood',
		'eb-pro-womens-microtherm-nohood',
		'eb-pro-womens-sandstone-hood',
		'eb-pro-womens-sandstone-nohood'
	]

	$scene_files = [
		'bmk-prs-knives',
		'gk-prs-bottoms',
		# 'gk-prs-cheer',
		'gk-prs-gym',
		'gk-prs-warmups',
		# 'ua-prs-cheer',
		'ua-prs-gym',
		'ua-prs-warmups',
		'uau-prs-hockey',
		'uau-prs-lacrosse',
		'uau-prs-volleyball',
		'uau-prs-track',
		'uau-prs-soccer',
		'uau-prs-softball',
		'uau-prs-training',
		'uau-prs-wrestling',
		'uau-prs-baseball',
		'uau-prs-football',
		'uau-prs-basketball',
		# 'uau-prs-football-brawler',
		'uau-prs-sideline',
		'uaf-prs-railfit-mens',
		'uaf-prs-spotlight-mens',
		'uaf-prs-curry1-mens',
		'uaf-prs-clutchfit-mens',
		'uaf-prs-charged247-mens',
		'uaf-prs-highlight-mens',
		'uaf-prs-drive4-mens',
		'uaf-prs-drive4low-mens',
		'uaf-prs-curry1low-mens',
		'uaf-prs-icon-sackpack',
		'uaf-prs-ignite-mens',
		'uaf-prs-ignite-womens',
		'uaf-prs-railfit-womens',
		'uaf-prs-curry1-youth',
		'uaf-prs-clutchfit-womens',
		'uaf-prs-charged247-womens',
		'uaf-prs-drive4-womens',
		'uaf-prs-curry5-kids',
		'uaf-prs-curry5-mens',
		'uaf-prs-hovrhavoclow-mens',
		'uaf-prs-hovrhavoc-mens',
		'uaf-prs-hovrhavoc-womens',
		'uaf-prs-hovrslk-womens',
		'uaf-prs-hovrslk-mens',
		'cb-pro-chute1-bottle',
		'cb-pro-chute20-rubberized',
		'cb-pro-chute20-stainless-vacuum-bottle',
		'cb-pro-chute20-vacuum-bottle',
		'cb-pro-chute40-stainless-vacuum-bottle',
		'cb-pro-chute40-vacuum-bottle',
		'cb-pro-chute75-bottle',
		'cb-pro-eddy1-bottle',
		'cb-pro-eddy4-kids-bottle',
		'cb-pro-eddy6-bottle',
		'cb-pro-eddy6-insulated-bottle',
		'cb-pro-eddy20-rubberized',
		'cb-pro-eddy20-vacuum-bottle',
		'cb-pro-eddy75-bottle',
		'cb-pro-forge12-bottle',
		'cb-pro-forge16-bottle',
		'cb-pro-kickbak20-bottle',
		'cb-pro-kickbak20-stainless-bottle',
		'cb-pro-kickbak30-bottle',
		'cb-pro-kickbak30-stainless-bottle',
		'cb-pro-podium-bigchill-bottle',
		'cb-pro-podium-chill-bottle',
		'cb-pro-podium21-bottle',
		'cb-pro-podium24-bottle',
		'eb-pro-mens-microtherm-hood',
		'eb-pro-mens-microtherm-nohood',
		'eb-pro-mens-sandstone-hood',
		'eb-pro-mens-sandstone-nohood',
		'eb-pro-womens-microtherm-hood',
		'eb-pro-womens-microtherm-nohood',
		'eb-pro-womens-sandstone-hood',
		'eb-pro-womens-sandstone-nohood']
	end