require "./lib/pages/asset_api_page"

describe "| Client: LEATHERMAN |" do

	before(:all) do
		@v = Array.new
		@k = Array.new
	end

	after(:all) do
		if @v.nil?
			puts ""
			puts ">>  0 Discrepancies found in Product Data Connections!!"
		else
			puts ""
			puts ">>  #{@v.uniq.length} Assets with SceneFile['connections'].values not found in Product Data Connections"
		end
		if @k.nil?
			puts ""
			puts ">>  0 Discrepancies found in Manifest Data!!"
			puts ""
		else
			puts ""
			puts ">>  #{@k.uniq.length} Assets with SceneFile['connections'].keys not found in Manifest"
			puts ""
		end
	end

	$leatherman_scene_files.each do |scene|
		it "LEATHERMAN | #{ENV['ENVIRONMENT'].upcase} | #{scene} | Connection Test Complete" do
			aggregate_failures "Verify #{scene} Product 'connections' exist in associated productData and Manifest" do
				manifest = AssetAPI.scene_manifest_url(scene)
				mobile_manifest = AssetAPI.scene_mobile_manifest_url(scene)
				AssetAPI.scene_productoptions_keys(scene).zip(AssetAPI.scene_connections(scene)).each do |product, pairs|
					puts ""
					puts " - #{ENV['SITE']} | #{scene} | #{product}"
					if (pairs.nil? || pairs.empty? || pairs == [] || pairs == {})
						puts "#{product} does not have scenefile connections set up."						
					else
						product_handles = AssetAPI.product_handle_values(product)
						manifest_keys = AssetAPI.manifest_parameter_keys(product,manifest)
						mobile_manifest_keys = AssetAPI.manifest_parameter_keys(product,mobile_manifest)
						manifest_values = AssetAPI.manifest_parameter_values(product,manifest)
						mobile_manifest_values = AssetAPI.manifest_parameter_values(product,mobile_manifest)
						pairs.each do |sceneKey, sceneValue|
							if sceneValue.kind_of?(Array) ||  sceneValue.include?(':') || sceneKey.include?(':') || sceneKey.include?('raster') || sceneValue.include?('raster')
							else
								aggregate_failures "#{ENV['SITE']} | #{scene} | #{product}" do
									if sceneValue.is_a? Array
									else
										if product_handles.include?(sceneValue) == false
											puts "'#{sceneValue}' Not found in Product Data"
											@v.push("#{ENV['SITE']} | #{scene} | #{product}")
											expect(product_handles).to include(sceneValue)
										end
									end
									if mobile_manifest_values.nil?
									else
										if mobile_manifest_keys.include?(sceneKey) == false
											puts "'#{sceneKey}' Not found in mobileManifest Parameters"
											@k.push("#{ENV['SITE']} | #{scene} | #{product}")
											expect(mobile_manifest_keys).to include(sceneKey)
										end
									end
									if manifest_keys.include?(sceneKey) == false
										puts "'#{sceneKey}' Not found in Product Manifest Parameters"
										@k.push("#{ENV['SITE']} | #{scene} | #{product}")
										expect(manifest_keys).to include(sceneKey)
									end
								end
							end
						end
					end
				end
			end
		end
	end
end