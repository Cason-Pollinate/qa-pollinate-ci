require "./lib/pages/gkelite/gk_shopify_base_page"

describe "#{ENV['SITE'].upcase} | #{ENV['ENVIRONMENT'].upcase}" do

	before(:each) do
		@page = GKShopifyBasePage.new
	end

	after(:each) do 

	end

	context "Recipe Validation Test" do
		
		$recipe_ids.each do |recipe_id|
		
			it "Verify Recipe | #{recipe_id}" do
				@page.goto("www.google.com")
				aggregate_failures "#{recipe_id}" do
					case ENV['ENVIRONMENT'].to_sym
					when :test then @page.goto("https://dev-gkelite.pollinate.com/pages/gk-gym-custom#/saved/#{recipe_id}/design")
					when :staging then @page.goto("https://staging-gkelite.pollinate.com/pages/gk-gym-custom#/saved/#{recipe_id}/design")
					when :prod then @page.goto("https://www.gkelite.com/pages/gk-gym-custom#/saved/#{recipe_id}/design")
					end
					sleep 1
					@page.wait_until { @page.loader? || @page.sorry? }
					if @page.sorry?
						puts "Could Not Load #{recipe_id}"
						@page.goto("www.google.com")
					else
						sleep 3
						BasePage.print_js_errors
					end
				end
		
			end
		
		end

	end

end