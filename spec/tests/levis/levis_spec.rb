require "./lib/pages/levis/levis_base_page"

describe "Levis" do

	before(:all) do 
		@page = LevisBasePage.new
	end

	context ": Jackets" do 

		$levis_jackets.each do |sku,handle|
			it ":: Testing SKU: #{sku}" do 
				@page.goto("http://madetoordertest.blob.core.windows.net/levi/frontend/index.html?sku=#{sku}")
				@page.wait_until {@page.placement?}
			end

			it "Jackets | Front | Bean" do
				@page.placement_element.click
				sleep 5
				@page.front_chest_area("Bean",handle)
				expect(@page.line_one_count).to eq("#{@page.line_one.length}/#{@page.line_one.length}")
			end

			it "Jackets | Front | Block" do
				@page.front_chest_area("Block",handle)
				expect(@page.line_one_count).to eq("#{@page.line_one.length}/#{@page.line_one.length}")
			end

			it "Jackets | Front | Script" do
				@page.front_chest_area("Script",handle)
				expect(@page.line_one_count).to eq("#{@page.line_one.length}/#{@page.line_one.length}")
			end

			it "Jackets | Back | Bean" do
				@page.placement_element.click
				@page.back_panel_element.click
				sleep 5
				@page.back_panel_area("Bean",handle)
				expect(@page.line_one_count).to eq("#{@page.line_one.length}/#{@page.line_one.length}")
				expect(@page.line_two_count).to eq("#{@page.line_two.length}/#{@page.line_two.length}")
				expect(@page.line_three_count).to eq("#{@page.line_three.length}/#{@page.line_three.length}")
			end

			it "Jackets | Back | Script" do
				@page.back_panel_area("Script",handle)
				expect(@page.line_one_count).to eq("#{@page.line_one.length}/#{@page.line_one.length}")
				expect(@page.line_two_count).to eq("#{@page.line_two.length}/#{@page.line_two.length}")
				expect(@page.line_three_count).to eq("#{@page.line_three.length}/#{@page.line_three.length}")
			end

			it "Jackets | Waistband | Bean" do
				@page.placement_element.click
				@page.waistband_element.click
				sleep 5
				@page.waist_band_area("Bean",handle)
				expect(@page.line_one_count).to eq("#{@page.line_one.length}/#{@page.line_one.length}")
			end

			it "Jackets | Waistband | Block" do
				@page.waist_band_area("Block",handle)
				expect(@page.line_one_count).to eq("#{@page.line_one.length}/#{@page.line_one.length}")
			end

			it "Jackets | Waistband | Script" do
				@page.waist_band_area("Script",handle)
				expect(@page.line_one_count).to eq("#{@page.line_one.length}/#{@page.line_one.length}")
			end
		end
	end

	context ": Shirts" do

		$levis_shirts.each do |sku,handle|
			it ":: Testing SKU: #{sku}" do 
				@page.goto("http://madetoordertest.blob.core.windows.net/levi/frontend/index.html?sku=#{sku}")
				@page.wait_until {@page.placement?}
			end

			it "T-Shirt | Front Center" do
				@page.front_center_tshirt
				expect(@page.tshirt_text_count).to eq("#{@page.tshirt_text.length}/#{@page.tshirt_text.length}")
			end

			it "T-Shirt | Front Left Chest" do
				@page.front_left_tshirt
				expect(@page.tshirt_text_count).to eq("#{@page.tshirt_text.length}/#{@page.tshirt_text.length}")
			end
		end
	end
end






