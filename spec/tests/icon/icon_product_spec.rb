require "./lib/pages/icon/icon_customizer_page"
require "./lib/pages/icon/icon_bulk_order_page"

describe "ICON | #{ENV['ENVIRONMENT'].upcase} | #{ENV['BROWSER'].upcase} | Icon Smoke Test" do 

	before(:all) do
		@page = IconBulkReport.new
		@recipes = Array.new
	end



	# $icon_handles.each do |style|
	10.times do
		it "iCON |" do
			@recipes.push(@page.make_recipes('uaf-prs-hovrphantom-mens'))
		end
	end

	10.times do
		it "iCON |" do
			@recipes.push(@page.make_recipes('uaf-prs-hovrphantom-womens'))
		end
	end

	it "Generating Product Report" do
		puts @recipes
		@page.generate_bulk_product_report(@recipes)
		expect(@page.thank_you?).to eq(true)
	end
end