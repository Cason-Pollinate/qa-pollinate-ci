require "./lib/pages/icon/icon_base_page"
require "./lib/pages/icon/icon_customizer_page"

describe "ICON | Color Palette Test" do 


	$icon_handles.each do |scenefile|

		it "#{scenefile} | Patterns" do	
			aggregate_failures "#{scenefile}" do
				@patterns = IconBasePage.get_pattern_categories(scenefile)
				expect($icon_patterns[:xmas_camo]).to match_array(@patterns["uaf-asset-pattern-xmas-camo"])
				expect($icon_patterns[:digi_camo]).to match_array(@patterns["uaf-asset-pattern-digi-camo"])
				expect($icon_patterns[:tiger_camo]).to match_array(@patterns["uaf-asset-pattern-tiger-camo"])
				expect($icon_patterns[:rig]).to match_array(@patterns["uaf-asset-pattern-rig"])
				expect($icon_patterns[:breaker]).to match_array(@patterns["uaf-asset-pattern-breaker"])
				# expect($icon_patterns[:noise_acc]).to match_array(@patterns["uaf-asset-pattern-noise-acc"])
				expect($icon_patterns[:floral_camo]).to match_array(@patterns["uaf-asset-pattern-floral-camo"])
				# expect($icon_patterns[:rgb]).to match_array(@patterns["uaf-asset-pattern-3d-rgb"])
				expect($icon_patterns[:fracture]).to match_array(@patterns["uaf-asset-pattern-fracture"])
				expect($icon_patterns[:jagger_multi]).to match_array(@patterns["uaf-asset-pattern-jagger-multi"])
				expect($icon_patterns[:knockout]).to match_array(@patterns["uaf-asset-pattern-knockout"])
				expect($icon_patterns[:fleet]).to match_array(@patterns["uaf-asset-pattern-fleet"])
				expect($icon_patterns[:alley_oop]).to match_array(@patterns["uaf-asset-pattern-alley-oop"])
				expect($icon_patterns[:geo_stripe]).to match_array(@patterns["uaf-asset-pattern-geo-stripe"])
				expect($icon_patterns[:geo_cache]).to match_array(@patterns["uaf-asset-pattern-geo-cache"])
				expect($icon_patterns[:future_skeleton]).to match_array(@patterns["uaf-asset-pattern-future-skeleton"])
				expect($icon_patterns[:multisplatter]).to match_array(@patterns["uaf-asset-pattern-multisplatter"])
				expect($icon_patterns[:ribbon]).to match_array(@patterns["uaf-asset-pattern-ribbon"])
				expect($icon_patterns[:abbey_two]).to match_array(@patterns["uaf-asset-pattern-abbey-two"])
				# expect($icon_patterns[:abbey_one]).to match_array(@patterns["uaf-asset-pattern-abbey-one"])
				# expect($icon_patterns[:abbey_three]).to match_array(@patterns["uaf-asset-pattern-abbey-three"])
				expect($icon_patterns[:block_gradient]).to match_array(@patterns["uaf-asset-pattern-block-gradient"])
			end
		end

		it "#{scenefile} | Prints" do
			@prints = IconBasePage.get_print_categories(scenefile)
			aggregate_failures "#{scenefile}" do
				expect($icon_prints[:carefree]).to match_array(@prints["uaf-asset-photo-carefree"])
				expect($icon_prints[:cutout_taupe_gray]).to match_array(@prints["uaf-asset-photo-cutout-taupe-gray"])
				expect($icon_prints[:cutout_black_turquoise]).to match_array(@prints["uaf-asset-photo-cutout-black-turquoise"])
				expect($icon_prints[:cutout_black_orange]).to match_array(@prints["uaf-asset-photo-cutout-black-orange"])
				expect($icon_prints[:flux_multi_red_black]).to match_array(@prints["uaf-asset-photo-flux-multi-red-black"])
				expect($icon_prints[:flux_multi_white_gray]).to match_array(@prints["uaf-asset-photo-flux-multi-white-gray"])
				expect($icon_prints[:flux_multi_white_turquoise]).to match_array(@prints["uaf-asset-photo-flux-multi-white-turquoise"])
				expect($icon_prints[:grit_black_gray]).to match_array(@prints["uaf-asset-photo-grit-black-gray"])
				expect($icon_prints[:grit_turquoise]).to match_array(@prints["uaf-asset-photo-grit-turquoise"])
				expect($icon_prints[:grit_white_gray]).to match_array(@prints["uaf-asset-photo-grit-white-gray"])
				# expect($icon_prints[:curry5_launch]).to match_array(@prints["uaf-asset-photo-curry5-launch"])
				# expect($icon_prints[:scope_1]).to match_array(@prints["uaf-asset-photo-scope-1"])
				# expect($icon_prints[:scope_2]).to match_array(@prints["uaf-asset-photo-scope-2"])
				# expect($icon_prints[:scope_3]).to match_array(@prints["uaf-asset-photo-scope-3"])
				expect($icon_prints[:the_process_white_red]).to match_array(@prints["uaf-asset-photo-the-process-white-red"])
				expect($icon_prints[:the_process_white_blue]).to match_array(@prints["uaf-asset-photo-the-process-white-blue"])
				expect($icon_prints[:the_process_black_yellow]).to match_array(@prints["uaf-asset-photo-the-process-black-yellow"])
				expect($icon_prints[:kenesha_sneed_escape]).to match_array(@prints["uaf-asset-photo-kenesha-sneed-escape"])
				expect($icon_prints[:kenesha_sneed_garden]).to match_array(@prints["uaf-asset-photo-kenesha-sneed-garden"])
				expect($icon_prints[:tommi_lim_cloudburst]).to match_array(@prints["uaf-asset-photo-tommi-lim-cloudburst"])
				expect($icon_prints[:shelby_and_sandy_clouds]).to match_array(@prints["uaf-asset-photo-shelby-and-sandy-clouds"])
				expect($icon_prints[:shelby_and_sandy_skull_black]).to match_array(@prints["uaf-asset-photo-shelby-and-sandy-skull-black"])
				expect($icon_prints[:jen_mussari_marble]).to match_array(@prints["uaf-asset-photo-jen-mussari-marble"])
				expect($icon_prints[:vivid_stroke_dust]).to match_array(@prints["uaf-asset-photo-vivid-stroke-dust"])
				expect($icon_prints[:vivid_stroke_tetra_gray]).to match_array(@prints["uaf-asset-photo-vivid-stroke-tetra-gray"])
				expect($icon_prints[:vivid_stroke_aqua_foam]).to match_array(@prints["uaf-asset-photo-vivid-stroke-aqua-foam"])
				expect($icon_prints[:vivid_stroke_black]).to match_array(@prints["uaf-asset-photo-vivid-stroke-black"])
				expect($icon_prints[:p2nation_black_red]).to match_array(@prints["uaf-asset-photo-p2nation-black-red"])
				expect($icon_prints[:p2nation_white_red]).to match_array(@prints["uaf-asset-photo-p2nation-white-red"])
				# expect($icon_prints[:plaid_1]).to match_array(@prints["uaf-asset-photo-menswear-plaid-1"])
				# expect($icon_prints[:plaid_2]).to match_array(@prints["uaf-asset-photo-menswear-plaid-2"])
				# expect($icon_prints[:plaid_3]).to match_array(@prints["uaf-asset-photo-menswear-plaid-3"])
				# expect($icon_prints[:plaid_4]).to match_array(@prints["uaf-asset-photo-menswear-plaid-4"])
				# expect($icon_prints[:supernova_c]).to match_array(@prints["uaf-asset-photo-supernova-c"])
				# expect($icon_prints[:supernova_b]).to match_array(@prints["uaf-asset-photo-supernova-b"])
				# expect($icon_prints[:supernova_a]).to match_array(@prints["uaf-asset-photo-supernova-a"])
				# expect($icon_prints[:static_b]).to match_array(@prints["uaf-asset-photo-static-b"])
				# expect($icon_prints[:static_a]).to match_array(@prints["uaf-asset-photo-static-a"])
				# expect($icon_prints[:sprocket_a]).to match_array(@prints["uaf-asset-photo-sprocket-a"])
				# expect($icon_prints[:sprocket_d]).to match_array(@prints["uaf-asset-photo-sprocket-d"])
				# expect($icon_prints[:sprocket_b]).to match_array(@prints["uaf-asset-photo-sprocket-b"])
				# expect($icon_prints[:sprocket_c]).to match_array(@prints["uaf-asset-photo-sprocket-c"])
				expect($icon_prints[:patriotic]).to match_array(@prints["uaf-asset-photo-patriotic"])
				expect($icon_prints[:country_pride_flag_crop]).to match_array(@prints["uaf-asset-photo-country-pride-flag-crop"])
				expect($icon_prints[:country_pride_leaf_pattern]).to match_array(@prints["uaf-asset-photo-country-pride-leaf-pattern"])
				expect($icon_prints[:leaf_camo]).to match_array(@prints["uaf-asset-photo-leaf-camo"])
				expect($icon_prints[:galaxy]).to match_array(@prints["uaf-asset-photo-galaxy"])
				expect($icon_prints[:lightning]).to match_array(@prints["uaf-asset-photo-lightning"])
				expect($icon_prints[:country_pride_borealis]).to match_array(@prints["uaf-asset-photo-country-pride-borealis"])
				expect($icon_prints[:flames]).to match_array(@prints["uaf-asset-photo-flames"])
				expect($icon_prints[:floral]).to match_array(@prints["uaf-asset-photo-floral"])
				expect($icon_prints[:jungle_one]).to match_array(@prints["uaf-asset-photo-jungle"])
				expect($icon_prints[:jungle_two]).to match_array(@prints["uaf-asset-photo-jungle-tonal-white-choho-barn"])
				expect($icon_prints[:jungle_three]).to match_array(@prints["uaf-asset-photo-jungle-tonal-white-lima-bean-palmgreen"])
				expect($icon_prints[:jungle_four]).to match_array(@prints["uaf-asset-photo-jungle-tonal-white-bust-batik"])
				# expect($icon_prints[:blur_one]).to match_array(@prints['uaf-asset-photo-blurred-edge-purple-teal'])
				# expect($icon_prints[:blur_two]).to match_array(@prints['uaf-asset-photo-blurred-edge-red-black'])
				# expect($icon_prints[:blur_three]).to match_array(@prints['uaf-asset-photo-blurred-edge-yellow-black'])
			end
		end
	end

	

end