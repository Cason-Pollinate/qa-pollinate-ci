require "./lib/pages/icon/icon_bulk_order_page"
require "./lib/pages/icon/icon_customizer_page"
require "./lib/pages/asset_api_page"


describe "ICON | #{ENV['ENVIRONMENT'].upcase} | #{ENV['BROWSER'].upcase} | Bulk Order Report" do 

	before(:all) do
		@page = IconBulkReport.new
		@recipe_set_ids = Array.new
	end

	$uaf_scene_files.each do |asset|
		10.times do
			it "Create RecipeSetID's" do
				@recipe_set_ids.push(@page.make_recipes(asset))
			end
		end
		it "Generate Bulk Product Report" do
			puts @recipe_set_ids
			@page.generate_bulk_product_report(@recipe_set_ids)
			expect(@page.thank_you?).to eq(true)
		end
		@recipe_set_ids = Array.new
	end

end
