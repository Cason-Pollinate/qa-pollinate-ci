require "./lib/pages/icon/icon_base_page"
require "./lib/pages/icon/icon_shoe_wall_page"


describe "ICON | PROD | #{ENV['BROWSER'].upcase} | Shoe Wall Spec" do 

	before(:all) do
		@page = IconShoeWall.new
		@page.goto("https://www.underarmour.com/en-us/login")
		@page.icon_login
	end


	$gallery_handles.each do |handle|
		it "ICON | #{handle.upcase} | Verify Front End Gallery Default Recipes" do
			aggregate_failures "#{handle}" do
				@page.verify_front_end_galleries(handle)
			end
		end

		it "#{handle}" do
			aggregate_failures "#{handle}" do
				@page.verify_back_end_galleries(handle)
			end
		end
	end
end
