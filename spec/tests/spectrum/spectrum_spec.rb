require "./lib/pages/spectrum/spectrum_base_page"

describe "Client: Spectrum" do

	before(:all) do
		@page = SpectrumBasePage.new
		@links = Array.new
		@page.goto("http://spectrumcustom.staging.wpengine.com/")
		BasePage.performance_check
		@links = $driver.links.collect(&:href)
		@links.reject! {|r| (r.include?("http") == false)}
	end

	context "Link Test" do
		it "Submit Form w/ Incomplete Data" do
			@page.request_demo_button_element.click
			@page.check_promo_checkbox
			@page.submit_button_element.click
			sleep 2
		end 

		it "Submit Form w/ Incorrect Data" do
			@page.wait_while { @page.request_modal_element.visible? }
			@page.wait_until { @page.request_demo_button? }
			@page.request_demo_button_element.click
			@page.name_field = "!@$%^&*()_-=+[]|{}#"
			@page.company_field = "!@^&*"
			@page.email_field = "pollinatetest"
			@page.phone_field = "asodifnla"
			@page.submit_button_element.click
			sleep 2
		end

		it "Submit Correct Form" do
			@page.fill_request_form
		end
		
		it "Navigate to each Page Link" do
			aggregate_failures "" do
				@links.each do |url|
					aggregate_failures "" do
						@page.goto(url)
						BasePage.performance_check
						BasePage.raise_js_errors
					end
				end
			end
		end

		it "Validate Page Accessibility" do
			aggregate_failures "" do
				@links.each do |url|
					@page.goto(url)
					expect(@page).to be_accessible.according_to(:wcag2a)
				end
			end
		end
	end
end