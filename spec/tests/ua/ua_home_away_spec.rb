require "./lib/pages/asset_api_page"


describe "| Client: Under Armour |" do

	$uau_scene_files.each do |scene|
		it "Under-Armour | #{ENV['ENVIRONMENT'].upcase} | #{scene} | Test Complete" do
			AssetAPI.scene_productoptions_keys(scene).each do |product|
				aggregate_failures "#{product}" do
					puts ""
					puts " - #{ENV['SITE']} | #{scene} | #{product}"
					AssetAPI.validate_home_away(product)
				end
			end
		end
	end
end