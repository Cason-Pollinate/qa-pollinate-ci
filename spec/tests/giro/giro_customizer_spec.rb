require "./lib/pages/giro/giro_base_page"

describe ": Giro"  do
	
	before(:all) do
		@page = GiroBasePage.new
		case ENV['ENVIRONMENT'].to_sym
		when :test then @page.goto("http://madetoordertest.blob.core.windows.net/giro/frontend/index.html#/productset/giro-prs-helmets")
		when :staging then @page.goto("http://madetoorderstaging.blob.core.windows.net/giro/frontend/index.html#/productset/giro-prs-helmets")
		when :prod then @page.goto("http://madetoorder.blob.core.windows.net/giro/frontend/index.html#/productset/giro-prs-helmets")
		end
		@page.we_are_all_here
	end

	context ": Customizer" do 

		it ": Side" do
			@page.iterate_side
		end
		it ": Middle" do
			@page.iterate_middle
		end
		it ": Top" do
			@page.iterate_top
		end
		it ": Bottom" do
			@page.iterate_bottom
		end
		it ": Logo" do
			@page.iterate_logo
		end
		it ": Fit" do
			@page.iterate_fit
		end
		it ": Webbing" do
			@page.iterate_webbing
		end
		it ": Et Al" do
			@page.iterate_et_al
		end

		# it "Spec/Packlist" do
		# 	id = @page.get_recipe_id
		# 	puts @page.spec_response(id[0])
		# 	# @page.packlist_response(id[0])
		# end

		it ": PageErrors" do
			BasePage.print_js_errors
		end

	end
end