require "./lib/pages/timex/timex_customizer"

describe "Timex Customizer" do

	before(:all) do
		@page = TimexCustomizer.new
	end

	context "UI Test" do
		@assets = ["tmx-pro-guess-originals","tmx-pro-wilshire-38mm"]
		@assets.each do |asset|
			it "#{asset}" do
				case ENV['ENVIRONMENT'].to_sym
				when :test then @page.goto("https://madetoordertest.blob.core.windows.net/timex/frontend/index.html#/product/#{asset}")
				when :staging then @page.goto("https://madetoorderstaging.blob.core.windows.net/timex/frontend/index.html#/product/#{asset}")
				when :prod then @page.goto("https://madetoorder.blob.core.windows.net/timex/frontend/index.html#/product/#{asset}")
				end
				@page.wait_until {@page.loading_bar?}
				@page.wait_while {@page.loading_bar?}
			end

			it "Iterate Dial Tab" do
				@page.iterate_tab
			end

			it "Iterate Strap Tab" do
				@page.strap_tab_element.click
				@page.iterate_tab
			end

			it "Iterate Case Tab" do
				@page.case_tab_element.click
				@page.iterate_tab
			end

			it "Iterate Engraving Tab" do
				@page.engraving_tab_element.click
				@page.text_tab
			end

			it "Share Modal Present?" do
				@page.share
				@page.wait_until { @page.share_modal? }
			end

			it "Share Modal Image" do
				@page.wait_while {@page.graphic_element.visible?}
				expect(@page.share_image?).to eq true 
			end

			it "Share Modal Copy Link" do
				copy_link = @page.copy_share_link
				# puts copy_link
				# Clipboard.paste
				# expect(copy_link).to == Clipboard
			end

			it "Save Modal Present?" do
				@page.ok
				@page.save
				@page.wait_until { @page.save_modal? }
				@page.wait_while { @page.graphic_element.visible? }
				expect(@page.save_modal_element.visible?).to eq true 
			end

			it "Save Modal Send Button Disabled?" do
				expect(@page.send_email_element.disabled?).to eq true
			end

			it "Save Modal Send Button Enabled?" do
				@page.check_email_consent
				expect(@page.send_email_element.disabled?).to eq false
			end

			it "Send Save Email" do
				@page.email = "cason.williams@pollinate.com"
				@page.send_email
				@page.ok
			end

			it "Reset Confirmation Popup" do
				@page.strap_tab
				@page.reset
				@page.wait_until { @page.confirm_reset? }
				expect(@page.confirm_reset_element.visible?).to eq true
			end

			it "Reset 3D Model" do
				@page.confirm_reset
				expect(@page.default_case_color?).to eq true
			end

			it "Report Page Errors" do
				BasePage.print_js_warnings
				@page.goto("www.google.com")
			end
		end
	end
end