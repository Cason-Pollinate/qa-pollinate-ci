describe "Timex Performance Test" do 

	before (:each) do
		$driver.goto("www.google.com")
	end

	$ua_material_id.each do |id|
		it "#{id}" do
			$driver.goto("https://demo.spectrumcustomizer.com/gk-elite/#{ENV['ENVIRONMENT']}/frontend/index.html#/products/#{id}/design")
			BasePage.performance_check
		end
	end
end