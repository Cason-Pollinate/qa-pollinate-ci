require "./lib/pages/bmk/bmk_base_page"

describe "Client: Benchmade" do

	before(:all) do
		@page = BMKBasePage.new
	end

	context " | Default Recipe Test | " do

		$knives.each do |asset|
			it "Validate #{asset} Default Recipe" do
				@page.goto("www.google.com")
				aggregate_failures "#{asset}" do
					
					case ENV['ENVIRONMENT'].to_sym
					when :test then
						$driver.goto("http://madetoordertest.blob.core.windows.net/#{ENV['SITE']}/frontend/index.html#/product/#{asset}")
					when :staging then 
						$driver.goto("http://madetoorderstaging.azureedge.net/#{ENV['SITE']}/frontend/index.html#/product/#{asset}")
					when :prod then 
						$driver.goto("http://madetoorder.azureedge.net/#{ENV['SITE']}/frontend/index.html#/product/#{asset}")
					end

					case asset
					when 'bmk-pro-crookedriver-full'
						aggregate_failures "#{asset}" do
							expect(@page.shape).to eq(': S30V')
							expect(@page.steel).to eq(': SATIN')
						end

					when 'bmk-pro-crookedriver-mini'
						aggregate_failures "#{asset}" do
							expect(@page.shape).to eq(': S30V')
							expect(@page.steel).to eq(': SATIN')
						end

					when 'bmk-pro-barrage-full'
						aggregate_failures "#{asset}" do
							expect(@page.shape).to eq(': DROP-POINT')
							expect(@page.steel).to eq(': S30V')
							expect(@page.finish).to eq(': SATIN')
						end

					when 'bmk-pro-barrage-mini'
						aggregate_failures "#{asset}" do
							expect(@page.shape).to eq(': DROP-POINT')
							expect(@page.steel).to eq(': S30V')
							expect(@page.finish).to eq(': SATIN')
						end

					when 'bmk-pro-grip-mini'
						aggregate_failures "#{asset}" do
							expect(@page.shape).to eq(': DROP-POINT')
							expect(@page.steel).to eq(': S30V')
							expect(@page.finish).to eq(': SATIN')
							@page.shape_option_element.lis.each do |l|
								blade_shape = l.button.title
								l.button.click
								puts blade_shape
								if blade_shape.include?('Drop-point') || blade_shape.include?('Tanto')
									@page.steel_tab
									@page.shape_tab
								end
							end
						end

					when 'bmk-pro-grip-full'
						aggregate_failures "#{asset}" do
							expect(@page.shape).to eq(': DROP-POINT')
							expect(@page.steel).to eq(': S30V')
							expect(@page.finish).to eq(': SATIN')
							@page.shape_option_element.lis.each do |l|
								blade_shape = l.button.title
								l.button.click
								if blade_shape.include?('Drop-point') || blade_shape.include?('Tanto')
									@page.steel_tab
									@page.shape_tab
								end
							end
						end
					end
				end
			end
		end
	end

	# context "" do
	# 	it "" do
	# 		puts BasePage.localize_text_message_handles('bmk-prs-knives')
	# 	end
	# end
end