require "./lib/pages/bmk/bmk_base_page"

describe "Client: Benchmade" do

	before(:all) do
		@page = BMKBasePage.new
		@recipes = Array.new
	end

	$bmk_skus.each do |sku|
		it "Testing Endpoint for SKU: #{sku}" do
			response = BMKBasePage.bmk_sku_endpoint_check(sku)
			puts response
		end
	end

	it "Testing Spec SKU" do
		20.times do
			knife = @page.random_knife
			@recipes.push(knife[0])
			puts "Recipe ID: #{knife[0]}"
			puts "SKU: #{knife[3]}"
			puts "Remote SKU: #{knife[4]}"
			puts "Model #: #{knife[5]}"
			$driver.goto("www.google.com")
		end
		@recipes.each do |id|
			puts id
			puts "SPEC Model: #{BMKBasePage.bmk_sku_spec_check(id)}"
		end
	end
end