describe "Accessibility :: " do

    before(:all) do
        @page = BasePage.new
        @page.goto "http://www.benchmade.com/"
    end
        
    it "Page Accessibility WCAG2 Guideline Verification" do
        expect(@page).to be_accessible.according_to(:wcag2a)
    end         
end