require "./lib/pages/jbl/jbl_base_page"

describe "JBL.#{ENV['ENVIRONMENT']}.#{ENV['BROWSER']}" do

	before (:all) do

		@page = JBLBasePage.new

	end

	context "context.LoadAsset" do

		$jbl_sku.each do |sku|

			it "#{sku}" do

				@page.navigate_to_asset(sku)
				@page.wait_while { @page.loading? }
				@page.wait_until { @page.menu? }
				sleep 2
				BasePage.print_js_errors

			end

		end

	end
	
end